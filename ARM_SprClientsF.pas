unit ARM_SprClientsF;

interface

uses
  Windows,
  Messages,
  SysUtils,
  Variants,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  StdCtrls,
  Buttons,
  ExtCtrls,
  Grids,
  DBGrids,
  ARM_DataMod,
  DB,
  IBX.IBCustomDataSet,
  IBX.IBQuery,
  IBX.IBDatabase,
  ARM_Unit,
  ARM_AddClientF,
  IBX.IBTable,
  Vcl.DBCtrls,
  Vcl.Imaging.JPEG,
  System.DateUtils;

type
  TARM_SprClientsForm = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    GroupBox1: TGroupBox;
    Edit1: TEdit;
    GroupBox2: TGroupBox;
    DBGrid1: TDBGrid;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn5: TBitBtn;
    DataSource1: TDataSource;
    IBTransaction1: TIBTransaction;
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    DBText1: TDBText;
    DBText2: TDBText;
    DBText3: TDBText;
    DBText4: TDBText;
    DBText5: TDBText;
    DBText6: TDBText;
    DBText7: TDBText;
    DBText8: TDBText;
    Label10: TLabel;
    Label11: TLabel;
    DBText9: TDBText;
    DBText10: TDBText;
    IBTableMV: TIBTable;
    Label12: TLabel;
    Label13: TLabel;
    DBText11: TDBText;
    procedure DBGrid1DblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGrid1TitleClick(Column: TColumn);
    procedure IBTableMVAfterScroll(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
 //   StSQL, StQuery, StSearch, StOrder: string;
    procedure PrintRecordsCount;    //������� � ������ ���������� ����������� �������
    function GetRecordsCount(DS: TIBTable): integer; //���������� ���������� ����������� ������� � ��������
  public
    { Public declarations }
  end;

var
  ARM_SprClientsForm: TARM_SprClientsForm;

implementation

{$R *.dfm}

//-----------------------------------------------
//������ "�������"                                 +
procedure TARM_SprClientsForm.BitBtn5Click(Sender: TObject);
begin
 Close;
end;

//-----------------------------------------------
//������ "��������"
procedure TARM_SprClientsForm.BitBtn1Click(Sender: TObject);
begin
 ARM_AddClientForm := TARM_AddClientForm.Create(Self);
 ARM_AddClientForm.FormSt := fsAdd;
 ARM_AddClientForm.ShowModal;
 if ARM_AddClientForm.Res then
  begin
   IBTableMV.Refresh;
   IBTableMV.Locate('ID', ARM_AddClientForm.Rec_Id, []);
   PrintRecordsCount;
  end;
 ARM_AddClientForm.Destroy;
end;

//-----------------------------------------------
//������ "��������"
procedure TARM_SprClientsForm.BitBtn2Click(Sender: TObject);
var
 StreamValue: TMemoryStream;
 Jpg: TJPEGImage;
begin
 ARM_AddClientForm:=TARM_AddClientForm.Create(Self);
 with ARM_AddClientForm do
  begin
   FormSt := fsEdit;
   LabeledEdit2.Text :=IBTableMV.FieldByName('FIRST_NAME').AsString;
   LabeledEdit3.Text :=IBTableMV.FieldByName('MIDDLE_NAME').AsString;
   LabeledEdit1.Text :=IBTableMV.FieldByName('LAST_NAME').AsString;
   LabeledEdit7.Text :=IBTableMV.FieldByName('TNUM').AsString;
   LabeledEdit6.Text :=IBTableMV.FieldByName('CARD_CODE').AsString;
   Edit1.Text := IBTableMV.FieldByName('LIM').AsString;
   Edit2.Text := IBTableMV.FieldByName('DESCRIPTION_').AsString;
   if IBTableMV.FieldByName('MNTH').AsInteger = MonthOf(Today) then
    Label5.Caption := '������������� � ������� ������: ' + Format('%f', [IBTableMV.FieldByName('SUMM_MNTH').AsCurrency])
   else
    Label5.Caption := '������������� � ������� ������: 0,00';
   if IBTableMV.FieldByName('VALIDTHRU').IsNull then
    MaskEdit3.Text := '  .  .    '
   else
    MaskEdit3.Text := IBTableMV.FieldByName('VALIDTHRU').AsString;
   ComboBox2.Text :=IBTableMV.FieldByName('POS').AsString;
   ComboBox1.Text :=IBTableMV.FieldByName('DEPARTMENT').AsString;
   if IBTableMV.FieldByName('PHOTO').IsNull then
    begin
     ARM_AddClientForm.Image1.Visible := False;
     ARM_AddClientForm.Label3.Visible := True;
    end
   else
    begin
     StreamValue := TMemoryStream.Create;
     try
      TBlobField(IBTableMV.FieldByName('PHOTO')).SaveToStream(StreamValue);
      StreamValue.Position := 0;
      Jpg := TJPEGImage.Create;
      try
       Jpg.LoadFromStream(StreamValue);
       ARM_AddClientForm.Image1.Picture.Assign(Jpg)
      finally
       Jpg.Free;
      end;
     finally
      StreamValue.Free;
     end;
     ARM_AddClientForm.Image1.Visible := True;
     ARM_AddClientForm.Label1.Visible := False;
    end;
   Rec_ID := IBTableMV.FieldByName('ID').AsInteger;
   ShowModal;
  end;
 if ARM_AddClientForm.Res then
  IBTableMV.Refresh;
 PrintRecordsCount;
 ARM_AddClientForm.Destroy;
end;

//-----------------------------------------------
//������ "�������"                                  +
procedure TARM_SprClientsForm.BitBtn3Click(Sender: TObject);
var
 BM: TBookmark;
begin
 if Application.MessageBox(PChar('���� �� ������ "Ok", ��������� ������ ����� ������� �� ����.'),
    '��������!', mb_IconExclamation + mb_OkCancel) = idOk then
  begin
   with ARM_Data do
    begin
     IBTransactionShort.StartTransaction;
     try
      IBQueryShort.SQL.Clear;
      IBQueryShort.SQL.Add('delete from S_STAFF where ID=:ID');
      IBQueryShort.ParamByName('ID').AsInteger := IBTableMV.FieldByName('ID').AsInteger;
      IBQueryShort.ExecSQL;
     finally
      IBTransactionShort.Commit;
     end;
     BM := IBTableMV.GetBookmark;
     IBTableMV.Refresh;
     IBTableMV.GotoBookmark(BM);
     PrintRecordsCount;
    end;
  end;
end;

//-----------------------------------------------
//��� ������� ����� �������� �������������� ������� ��� ���������� �����  +
procedure TARM_SprClientsForm.DBGrid1DblClick(Sender: TObject);
begin
 if not IBTableMV.IsEmpty then
  BitBtn2Click(Self)  //��������
 else
  BitBtn1Click(Self); //��������
end;

//-----------------------------------------------
//���������� �� ����� �� ��������� �������        +
procedure TARM_SprClientsForm.DBGrid1TitleClick(Column: TColumn);
var
 Index : integer;
 Id : Variant;
begin
 IBTableMV.DisableControls;
 Id := IBTableMV.FieldByName('ID').Value;
 if (Column.FieldName <> 'TNUM') and (Column.FieldName <> 'LAST_NAME') then
  IBTableMV.IndexFieldNames := Column.FieldName + '; LAST_NAME'
 else
  IBTableMV.IndexFieldNames := Column.FieldName;
 for index := 0 to DBGrid1.Columns.Count - 1 do
  DBGrid1.Columns[index].Title.Font.Style := DBGrid1.Columns[index].Title.Font.Style - [fsBold];
 Column.Title.Font.Style := [fsBold];
 IBTableMV.AfterScroll := nil;
 IBTableMV.Locate('ID', id, []);
 IBTableMV.AfterScroll := IBTableMVAfterScroll;
 IBTableMV.EnableControls;
end;

//-----------------------------------------------
//������� � ������ ���������� ����������� �������    +
procedure TARM_SprClientsForm.PrintRecordsCount;
begin
 Label12.Caption := '���-�� �������: ' + IntToStr(GetRecordsCount(IBTableMV));
 IBTableMVAfterScroll(IBTableMV);
 BitBtn2.Enabled := not IBTableMV.IsEmpty;
 BitBtn3.Enabled := not IBTableMV.IsEmpty;
end;

//-----------------------------------------------
//"�����" �����                                    +
procedure TARM_SprClientsForm.Edit1Change(Sender: TObject);
var
 St, St2, Value: string;
begin
 Value := Trim(Edit1.Text);
 St2 := '';
 if Value = '' then
  begin
   IBTableMV.Filter := '';
   PrintRecordsCount;
   Exit;
  end;
 if IsNumberStr(Value) then
  St2 := Format('(TNUM=%s)', [Value])
 else
  begin
   if Length(Value) > 48 then
    Delete(Value, 49, Length(Value) - 48);
   St := '%' + AnsiUpperCase(Value) + '%';
   St2 := Format('(upper(LAST_NAME) like ''%s'')', [St]);
  end;
 IBTableMV.Filter := St2;
 PrintRecordsCount;
end;

//-----------------------------------------------
//������ �� ���� '
procedure TARM_SprClientsForm.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
 (Sender as TEdit).ReadOnly := Key = #39;
end;

//-----------------------------------------------
//�������� �����                                  +
procedure TARM_SprClientsForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 IBTableMV.Close;
end;

//-----------------------------------------------
//�������� �����                                  +
procedure TARM_SprClientsForm.FormCreate(Sender: TObject);
begin
 IBTableMV.Active := True;
// IBTableMV.Filter := 'LAST_NAME';
 PrintRecordsCount;
end;

//-----------------------------------------------
//���������� ��������                             +
procedure TARM_SprClientsForm.FormShow(Sender: TObject);
begin
 IBTableMV.Refresh;
end;

//-----------------------------------------------
//���������� ���������� ������� � �������� IBTable  +
function TARM_SprClientsForm.GetRecordsCount(DS: TIBTable): integer;
var
 s : string;
begin
 Result := 0;
 if DS.IsEmpty then Exit;
 with ARM_Data do
  begin
   IBTransactionShort.StartTransaction;
   IBQueryShort.SQL.Clear;
   if DS.Filtered and (DS.Filter <> '') then
    s := 'select count(*) from ' + DS.TableName + ' where ' + DS.Filter
   else
    s := 'select count(*) from ' + DS.TableName;
   IBQueryShort.SQL.Add(s);
   IBQueryShort.Open;
   Result := IBQueryShort.FieldByName('count').AsInteger;
   IBQueryShort.Close;
   IBTransactionShort.Commit;
  end;
end;

//-----------------------------------------------
//��������� ����� ����� ������������� ������      +
procedure TARM_SprClientsForm.IBTableMVAfterScroll(DataSet: TDataSet);
var
 StreamValue: TMemoryStream;
 Jpg: TJPEGImage;
begin
 if DataSet.FieldByName('PHOTO').IsNull then
  begin
   Image1.Visible := False;
   Label1.Visible := True;
  end
 else
  begin
   StreamValue := TMemoryStream.Create;
   try
    TBlobField(DataSet.FieldByName('PHOTO')).SaveToStream(StreamValue);
    StreamValue.Position := 0;
    Jpg := TJPEGImage.Create;
    try
     Jpg.LoadFromStream(StreamValue);
     Image1.Picture.Assign(Jpg)
    finally
     Jpg.Free;
    end;
   finally
    StreamValue.Free;
   end;
   Image1.Visible := True;
   Label1.Visible := False;
  end;
end;

end.
