unit ARM_RepClientF;

interface

uses
  Windows,
  Messages,
  SysUtils,
  Variants,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  StdCtrls,
  ComCtrls,
  Grids,
  DBGrids,
  Buttons,
  ExtCtrls,
  frxClass,
  IBX.IBDatabase,
  DB,
  IBX.IBCustomDataSet,
  IBX.IBQuery,
  frxDBSet,
  ARM_Unit,
  ARM_DataMod,
  DateUtils,
  frxExportRTF,
  frxExportHTML,
  frxExportPDF,
  AppEvnts,
  System.UITypes,
  frxDesgn,
  IBX.IBTable;

type
  TARM_RepClientForm = class(TForm)
    Panel1: TPanel;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    Panel2: TPanel;
    GroupBox1: TGroupBox;
    Edit1: TEdit;
    GroupBox2: TGroupBox;
    DBGrid1: TDBGrid;
    GroupBox3: TGroupBox;
    DateTimePicker1: TDateTimePicker;
    DateTimePicker2: TDateTimePicker;
    Label1: TLabel;
    Label2: TLabel;
    RadioGroup1: TRadioGroup;
    frxDBDataset1: TfrxDBDataset;
    DataSource1: TDataSource;
    IBTransaction1: TIBTransaction;
    frxReport1: TfrxReport;
    IBQuery2: TIBQuery;
    IBTransaction2: TIBTransaction;
    IBTable1: TIBTable;
    Label3: TLabel;
    ComboBox1: TComboBox;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Edit1Change(Sender: TObject);
    procedure DBGrid1TitleClick(Column: TColumn);
    procedure IBQuery1AfterScroll(DataSet: TDataSet);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ARM_RepClientForm: TARM_RepClientForm;

implementation

{$R *.dfm}

//-----------------------------------------------
//������ "������"
procedure TARM_RepClientForm.BitBtn4Click(Sender: TObject);
begin
 IBTransaction2.StartTransaction;
 IBQuery2.SQL.Clear;
 if StartOfTheDay(DateTimePicker1.Date) = StartOfTheDay(DateTimePicker2.Date) then
  case RadioGroup1.ItemIndex of
   0: IBQuery2.SQL.Add('select * from OPER_VIEW_SHORT_BUF where cast(DATE_ as date) = :DATE_1 and (T_NUMBER = :T_NUMBER) and (KASSA = :KASSA) order by DATE_');
   1: IBQuery2.SQL.Add('select * from OPER_VIEW_BUF where cast(DATE_ as date) = :DATE_1 and (T_NUMBER = :T_NUMBER) and (KASSA = :KASSA) order by DATE_');
  end
 else
  case RadioGroup1.ItemIndex of
   0: IBQuery2.SQL.Add('select * from OPER_VIEW_SHORT_BUF where (DATE_ > :DATE_1) and (DATE_ < :DATE_2) and (T_NUMBER=:T_NUMBER) and (KASSA = :KASSA) order by DATE_');
   1: IBQuery2.SQL.Add('select * from OPER_VIEW_BUF where (DATE_ > :DATE_1) and (DATE_ < :DATE_2) and (T_NUMBER=:T_NUMBER) and (KASSA = :KASSA) order by DATE_');
  end;
 IBQuery2.ParamByName('DATE_1').AsDateTime := StartOfTheDay(DateTimePicker1.Date);
 if StartOfTheDay(DateTimePicker1.Date) <> StartOfTheDay(DateTimePicker2.Date) then
  IBQuery2.ParamByName('DATE_2').AsDateTime := EndOfTheDay(DateTimePicker2.Date);
 IBQuery2.ParamByName('T_NUMBER').AsString := Trim(IBTable1.FieldByName('TNUM').AsString);
 IBQuery2.ParamByName('KASSA').AsInteger := ComboBox1.ItemIndex + 1;
 IBQuery2.Open;
 case RadioGroup1.ItemIndex of
  0: frxReport1.LoadFromFile(ARM_ExePath + 'Reports\ClientShort.fr3');
  1: frxReport1.LoadFromFile(ARM_ExePath + 'Reports\ClientFull.fr3');
 end;
 frxReport1.ShowReport;
 IBQuery2.Close;
 IBTransaction2.Commit;
end;

//-----------------------------------------------
//������ "�������"
procedure TARM_RepClientForm.BitBtn5Click(Sender: TObject);
begin
 Close;
end;

//-----------------------------------------------
//
procedure TARM_RepClientForm.DBGrid1TitleClick(Column: TColumn);
var
 Index : integer;
 Id : Variant;
begin
 IBTable1.DisableControls;
 Id := IBTable1.FieldByName('ID').Value;
 if (Column.FieldName <> 'TNUM') and (Column.FieldName <> 'LAST_NAME') then
  IBTable1.IndexFieldNames := Column.FieldName + '; LAST_NAME'
 else
  IBTable1.IndexFieldNames := Column.FieldName;
 for index := 0 to DBGrid1.Columns.Count - 1 do
  DBGrid1.Columns[index].Title.Font.Style := DBGrid1.Columns[index].Title.Font.Style - [fsBold];
 Column.Title.Font.Style := [fsBold];
 IBTable1.Locate('ID', id, []);
 IBTable1.EnableControls;
end;

//-----------------------------------------------
//
procedure TARM_RepClientForm.Edit1Change(Sender: TObject);
var
 St, St2, Value: string;
begin
 Value := Trim(Edit1.Text);
 St2 := '';
 if Value = '' then
  Exit;
 if IsNumberStr(Value) then
  St2 := Format('(TNUM=%s)', [Value])
 else
  begin
   if Length(Value) > 48 then
    Delete(Value, 49, Length(Value) - 48);
   St := '%' + AnsiUpperCase(Value) + '%';
   St2 := Format('(upper(LAST_NAME) like ''%s'')', [St]);
  end;
 IBTable1.Filter := St2;
 BitBtn4.Enabled := not IBTable1.IsEmpty;
end;

//-----------------------------------------------
//
procedure TARM_RepClientForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 IBTable1.Close;
end;

//-----------------------------------------------
//
procedure TARM_RepClientForm.FormCreate(Sender: TObject);
begin
 IBTable1.Active := True;
 //IBTable1.Filter := 'LAST_NAME';
 DateTimePicker1.Date := Date;
 DateTimePicker2.Date := Date;
end;

//-----------------------------------------------
//
procedure TARM_RepClientForm.IBQuery1AfterScroll(DataSet: TDataSet);
begin
 BitBtn4.Enabled := not IBTable1.IsEmpty;
end;

end.
