unit ARM_RepDepartF;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.StdCtrls,
  Vcl.Buttons,
  Vcl.ComCtrls,
  System.Win.ComObj,
  DateUtils,
  IBX.IBDatabase,
  Data.DB,
  IBX.IBCustomDataSet,
  IBX.IBQuery,
  ARM_DataMod;

type
  TARM_RepDepartForm = class(TForm)
    ComboBox1: TComboBox;
    Label1: TLabel;
    DateTimePicker1: TDateTimePicker;
    DateTimePicker2: TDateTimePicker;
    Label2: TLabel;
    Label3: TLabel;
    BitBtn1: TBitBtn;
    Label4: TLabel;
    ComboBox2: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure DateTimePicker1Change(Sender: TObject);
    procedure DateTimePicker2Change(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ARM_RepDepartForm: TARM_RepDepartForm;

implementation

{$R *.dfm}

//-----------------------------------------------
//
procedure TARM_RepDepartForm.BitBtn1Click(Sender: TObject);
var
 a, b, c, RecCount : Integer;
 ExApp, WB, WS, FData, Range : Variant;
begin
 with ARM_Data do
  begin
   IBTransactionShort.StartTransaction;
   IBQueryShort.SQL.Clear;
   IBQueryShort.SQL.Add('select count(*) from REP_DIV_POS(:DIVISION, null, :DATE1, :DATE2, :KASSA)');
   IBQueryShort.ParamByName('DIVISION').AsString := ComboBox1.Text;
   IBQueryShort.ParamByName('DATE1').AsDateTime := DateTimePicker1.DateTime;
   IBQueryShort.ParamByName('DATE2').AsDateTime := DateTimePicker2.DateTime;
   IBQueryShort.ParamByName('KASSA').AsInteger := ComboBox2.ItemIndex + 1;
   IBQueryShort.Open;
   RecCount := IBQueryShort.FieldByName('COUNT').AsInteger;
   IBQueryShort.Close;
   IBQueryShort.SQL.Clear;
   IBQueryShort.SQL.Add('select * from REP_DIV_POS(:DIVISION, null, :DATE1, :DATE2, :KASSA)');
   IBQueryShort.ParamByName('DIVISION').AsString := ComboBox1.Text;
   IBQueryShort.ParamByName('DATE1').AsDateTime := DateTimePicker1.DateTime;
   IBQueryShort.ParamByName('DATE2').AsDateTime := DateTimePicker2.DateTime;
   IBQueryShort.ParamByName('KASSA').AsInteger := ComboBox2.ItemIndex + 1;
   IBQueryShort.Open;
   a := 0;
   ExApp := CreateOleObject('Excel.Application');
   WB := ExApp.WorkBooks.Add;
   WS := ExApp.Workbooks[1].WorkSheets[1];
   WS.Cells[1, 1] := ComboBox1.Text + ' (' + DateToStr(DateTimePicker1.DateTime) + ' - ' + DateToStr(DateTimePicker2.DateTime) + ')';
   WS.Rows[1].Font.Bold := True;
   FData := VarArrayCreate([1, RecCount, 1, 4], varVariant);
   with IBQueryShort do
    while not Eof do
     begin
      c := 0;
      for b := 0 to (Fields.Count - 1) do
       begin
        FData[a + 1, c + 1] := Fields[b].AsString;
        Inc(c)
       end;
      Next;
      Inc(a);
     end;
   IBQueryShort.Close;
   IBTransactionShort.Commit;
  end;
 Range := WS.Range[WS.Cells[2, 1], WS.Cells[VarArrayHighBound(FData, 1) + 1, VarArrayHighBound(FData, 2)]];
 Range.Value := FData;
 Range.Columns.AutoFit;
 ExApp.Visible := True;
 Close;
end;

//-----------------------------------------------
//
procedure TARM_RepDepartForm.DateTimePicker1Change(Sender: TObject);
begin
 DateTimePicker2.MinDate := DateTimePicker1.DateTime;
end;

//-----------------------------------------------
//
procedure TARM_RepDepartForm.DateTimePicker2Change(Sender: TObject);
begin
 DateTimePicker1.MaxDate := DateTimePicker2.DateTime;
end;

//-----------------------------------------------
//
procedure TARM_RepDepartForm.FormCreate(Sender: TObject);
begin
 DateTimePicker1.DateTime := StartOfTheMonth(Today);
 DateTimePicker2.DateTime := EndOfTheMonth(Today);
 DateTimePicker1.MaxDate := DateTimePicker2.DateTime;
 DateTimePicker2.MinDate := DateTimePicker1.DateTime;
 ComboBox1.Items.BeginUpdate;
 ComboBox1.Items.Clear;
 with ARM_Data do
  begin
   IBTransactionShort.StartTransaction;
   IBQueryShort.SQL.Clear;
   IBQueryShort.SQL.Add('select distinct DEPARTMENT from CLIENTS_VIEW order by DEPARTMENT');
   try
    IBQueryShort.Open;
    while not IBQueryShort.Eof do
     begin
      ComboBox1.Items.Add(IBQueryShort.FieldByName('DEPARTMENT').AsString);
      IBQueryShort.Next;
     end;
   finally
    IBQueryShort.Close;
    IBTransactionShort.Commit;
   end;
  end;
 ComboBox1.Items.EndUpdate;
 ComboBox1.ItemIndex := 0;
end;

end.
