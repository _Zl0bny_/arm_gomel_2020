unit ARM_ExportF;

interface

uses
  Windows,
  Messages,
  SysUtils,
  Variants,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  StdCtrls,
  Buttons,
  ExtCtrls,
  ComCtrls,
  ARM_DataMod,
  ARM_Unit, DB,
  IBX.IBCustomDataSet,
  IBX.IBQuery,
  IBX.IBDatabase,
  Vcl.ExtDlgs,
  System.DateUtils;

type
  TARM_ExportForm = class(TForm)
    DateTimePicker1: TDateTimePicker;
    Bevel1: TBevel;
    BitBtn1: TBitBtn;
    Label1: TLabel;
    IBTransaction1: TIBTransaction;
    IBQuery1: TIBQuery;
    Label2: TLabel;
    SaveTextFileDialog1: TSaveTextFileDialog;
    Label3: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure DateTimePicker1Change(Sender: TObject);
  private
    { Private declarations }
    RecToExp: integer;
  public
    { Public declarations }
  end;

var
  ARM_ExportForm: TARM_ExportForm;

implementation

{$R *.dfm}

//-----------------------------------------------
//������� ������
procedure TARM_ExportForm.BitBtn1Click(Sender: TObject);
var
 St: string;
 Txt: TextFile;
begin
 if SaveTextFileDialog1.Execute then
  begin
   AssignFile(Txt, SaveTextFileDialog1.FileName);
   Rewrite(Txt);
   IBTransaction1.StartTransaction;
   IBQuery1.SQL.Clear;
   IBQuery1.SQL.Add('select * from SUMM_REPORT(:BEGINDATE, :ENDDATE)');
   IBQuery1.ParamByName('BEGINDATE').AsDateTime := StartOfTheMonth(DateTimePicker1.DateTime);
   IBQuery1.ParamByName('ENDDATE').AsDateTime := EndOfTheMonth(DateTimePicker1.DateTime);
   IBQuery1.Open;
   Writeln(Txt, 'T_NUM;DATE_MMYY;SUM_1;SUM_2');
   while not IBQuery1.Eof do
    begin
     //St := Format('%6s %2.2d%2.2d215%14.2f   99  6 0', [IBQuery1.FieldByName('T_NUMBER').AsString, MonthOf(DateTimePicker1.DateTime), YearOf(DateTimePicker1.DateTime) - 2000, IBQuery1.FieldByName('SUMMA').AsCurrency]);
     //St[Pos(',', St)] := '.';
     St := IBQuery1.FieldByName('T_NUMBER').AsString + ';' +
           Format('%2.2d%2.2d', [MonthOf(DateTimePicker1.DateTime), YearOf(DateTimePicker1.DateTime) - 2000]) + ';' +
           IBQuery1.FieldByName('SUMMA').AsString + ';' + IBQuery1.FieldByName('SUMMA2').AsString;
     WriteLn(Txt, St);
     IBQuery1.Next;
    end;
   CloseFile(Txt);
   IBQuery1.Close;
   IBTransaction1.Commit;
   Close;
  end;
end;

//-----------------------------------------------
//������� ���������� ������� �� ��������� �����
procedure TARM_ExportForm.DateTimePicker1Change(Sender: TObject);
begin
 IBTransaction1.StartTransaction;
 IBQuery1.SQL.Clear;
 IBQuery1.SQL.Add('select count(*) from SUMM_REPORT(:BEGINDATE, :ENDDATE)');
 IBQuery1.ParamByName('BEGINDATE').AsDateTime := StartOfTheMonth(DateTimePicker1.DateTime);
 IBQuery1.ParamByName('ENDDATE').AsDateTime := EndOfTheMonth(DateTimePicker1.DateTime);
 IBQuery1.Open;
 RecToExp := IBQuery1.FieldByName('COUNT').AsInteger;
 IBQuery1.Close;
 IBTransaction1.Commit;
 Label2.Caption := '���������� �������: ' + IntToStr(RecToExp);
 BitBtn1.Enabled := RecToExp <> 0;
end;

//-----------------------------------------------
//������ ��������
procedure TARM_ExportForm.FormCreate(Sender: TObject);
begin
 DateTimePicker1.Date := Today;
 DateTimePicker1Change(Self);
end;

end.
