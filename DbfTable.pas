unit DbfTable;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
interface
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
uses
  Classes, Controls, SysUtils, Registry, Windows;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
const
	LatLetters : set of char = ['A'..'Z', 'a'..'z'];
  Digits : set of char = ['0'..'9'];
  AllowedChars : set of char = ['A'..'Z', 'a'..'z', '0'..'9', '_'];
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
type
	// ����� ������ � �����-������ ��� ��������� ����������
  EDbfError = class(Exception);

  // ������ ������������� �������
  EDbfInitError = class(EDbfError);

  // ������ ��� ������ ������
  EDbfReadError = class(EDbfError);

  // ������ ��� ������ ������
  EDbfWriteError = class(EDbfError);

  // ������ �����������
  EDbfModifyError = class(EdbfError);

  // ������������ ����� ������
  EDbfTypeError = class(EDbfError);

  // ������ ������ ������
  EDbfSeekError = class(EDbfError);

  // �������� �������� ����
  EDbfFieldError = class(EDbfError);

  // ���������� �������� NULL
  EDbfNullError = class(EDbfError);

  // ������ ������� � �����
  EDbfFileAccessError = class(EDbfError);

  (* �������������� ���� ������ - ����������, ��������, ����, ����������,
     � ��������� ������ (��� �� ��������), �������������� (������������
     ������ ������ ������) *)
	TDbfFieldType = (dftCharacter, dftNumeric, dftDate, dftLogical, dftFloat,
                   dftEmpty);

  (* ������������ ������ ���� *)
  TDateFormat = (dfDDMMYYYY, dfMMDDYYYY, dfYYYYMMDD);

  (* ����������� ������ *)
  TSearchDirection = (sdForward, sdBackward);

  (* ��� ������ ���������� ��������: T/F (True/False) ��� Y/N (Yes/No) *)
  TLogicalType = (ltTrueFalse, ltYesNo);

  (* ������������ ���������� ����������� *)
  TDecimalCharProperty = (dcpPoint, dcpComma);

  TDbfFieldPtr = ^TDbfField;

  // ������� ����
  TDbfField = record
    dfFieldName : string;
    dfFieldType : TDbfFieldType;
    dfFieldLength : byte;
    dfFieldDecimals : byte;
    dfNext : TDbfFieldPtr;
  end;

  // ���, �������������� ����� ��������� �������� ����
  TDbfVariant = record
  	// NULL ��� ��� (��� ����� �� ��������������)
    IsNull : boolean;
    (* ��� ������������ ��������. � ������������ � ��� �������� ���� ������
       �� ���� Valuexxxxxx *)
    VarType : TDbfFieldType;
    ValueNumeric : real;
    ValueCharacter : string;
    ValueDate : TDate;
    ValueFloat : real;
    ValueLogical : boolean;
  end;

	TDbfTable = class(TPersistent)
  	private
    	fErrorCode : byte;
      // ��� dbf-�����. ��� ����� ������ ���� � �����. �������������� ��� 03h
    	fDbfID : byte;
      fLastUpdated : TDate;
      fRecordsCount : LongWord;
      fDataOffset : word;
      fRecordSize : word;
      fFieldsCount : byte;
      fFieldsList : TDbfFieldPtr;
      fIsCanModify : boolean;
      fDbfPath : string;
      fFile : file of byte;
      fDecimalChar : char;
      fRegistryDecimalChar : char;
      fLogicalSymbol : TLogicalType;
      fDateFormat : TDateFormat;
      fIsFileClosed : boolean;
      fSkipDeleted : boolean;
      procedure AddNewField(NewField: TDbfField);
      procedure OpenTable;
      procedure NewFieldErrorRaise(ErrCode : byte);
      procedure WriteLastUpdated;
      procedure PopField;
      procedure FreeFieldsList;
      procedure WriteFieldErrorRaise(ErrCode: byte);
      function ValidatePath(InPath: string): boolean;
      function CheckField(NewField: TDbfField): byte;
      function CheckFieldName(FieldName: string): boolean;
      function SafeCnvStrToFloat(InFloat: string): real;
      function DateAddDelimiter(InDate: string): string;
      function ConvertValue(NewValue: TDbfVariant;
                            TargetField: TDbfField): string;
      function CheckValue(NewValue: string; TargetField: TDbfField): byte;
      function AlignValue(InValue: string; OutLength: integer;
                          IsString: boolean): string;
    public
      constructor Create(Path : string; DecimalCharacter : TDecimalCharProperty;
                  LogicalSymbol : TLogicalType; DateFormat : TDateFormat);
      // ������ �������� �������� ���������� �����
      // ����������� ������������ ������� �� ��������������
      procedure AddField(NewField : TDbfField); overload;
      procedure AddField(FieldName : string; FieldType : TDbfFieldType;
      					FieldLength : byte; FieldDecimals : byte); overload;
      procedure AddNumeric(FieldName: string; FieldLength,
      	                   FieldDecimals : byte);
      procedure AddFloat(FieldName: string; FieldLength, FieldDecimals : byte);
      procedure AddCharacter(FieldName: string; FieldLength: byte);
      procedure AddDate(FieldName : string);
      procedure AddLogical(FieldName : string);
      // �������� �������
      procedure CreateTable;
      // ����� �����������
      procedure Free;
      // ������� ������ �� ��������
      procedure DeleteRecord(RecordNumber: LongWord);
      // ������ ������� �� ��������
      procedure UndeleteRecord(RecordNumber: LongWord);
      // ������ ��������� ������������ ������� ��� �������� �����
      procedure ImportStructure(ImportFrom: string);
      // ������� ����� (�������� ���� �������)
      procedure Zap;
      // �������� ������ � ����� �����
      procedure Append;
      // �������� ���������� �� �������� �������
      procedure Pack;
      // ������ ��������
      procedure WriteValue(RecordNumber: LongWord; FieldNumber: byte;
                           NewValue: TDbfVariant); overload;
      // ������ ����������� ��������
      procedure WriteCharacter(RecordNumber: LongInt; FieldNumber: byte;
                               NewValue: string); overload;
      procedure WriteCharacter(RecordNumber: LongInt; FieldName: string;
                               NewValue: string); overload;
      // ������ ��������� ��������
      procedure WriteNumeric(RecordNumber: LongInt; FieldNumber: byte;
                             NewValue: real); overload;
      procedure WriteNumeric(RecordNumber: LongInt; FieldName: string;
                             NewValue: real); overload;
      // ������ ����
      procedure WriteDate(RecordNumber: LongInt; FieldNumber: byte;
                          NewValue: TDate); overload;
      procedure WriteDate(RecordNumber: LongInt; FieldName: string;
                          NewValue: TDate); overload;
      // ������ ����������� ��������
      procedure WriteLogical(RecordNumber: LongInt; FieldNumber: byte;
                             NewValue: boolean); overload;
      procedure WriteLogical(RecordNumber: LongInt; FieldName: string;
                             NewValue: boolean); overload;
      // ������ �������� � ��������� ������
      procedure WriteFloat(RecordNumber: LongInt; FieldNumber: byte;
                           NewValue: real); overload;
      procedure WriteFloat(RecordNumber: LongInt; FieldName: string;
                           NewValue: real); overload;
      // �������� �������� NULL
      procedure WriteNULL(RecordNumber: LongWord; FieldNumber: byte); overload;
      procedure WriteNULL(RecordNumber: LongWord; FieldName: string); overload;
      // ������ �������� �� ����� ����
      procedure WriteValue(RecordNumber : LongInt; FieldName : string;
                NewValue : TDbfVariant); overload;
      // �������� ������ �������
      procedure ExchangeRecords(RecordA, RecordB: LongWord);
      // ����������� ���� (���������� ������� ������ ����� ������ ����� �������)
      procedure Sort(ByField: byte);
      // �������� ������� ����
      function IsFieldExists(FieldName: string): boolean;
      // �������� �� ������ �� ��������
      function IsRecordDeleted(RecordNumber: LongWord): boolean;
      // �������� ����� ����
      function GetFieldLength(FieldNo: byte): byte;
      // �������� ��� ����
      function GetFieldName(FieldNo: byte): string;
      // �������� ������ � ��������� ������� (��� ���� ���������� � ����������
      // ������������� � ������� ������� ���� FloatToStr, DateToStr � �. �.
      function ReadAsString(RecordNumber: LongInt; FieldNumber: byte) : string;
      // �������� ��� ����
      function GetFieldType(FieldNo: byte): TDbfFieldType;
      // �������� ���������� ������ ����� ������� � ����
      function GetFieldDecimals(FieldNo: byte): byte;
      // �������� ��� ��������� ����
      function GetFieldParameters(FieldNumber: byte): TDbfField;
      // �������� �������� ����
      function GetField(RecordNumber: LongWord; FieldNumber: byte): TDbfVariant; overload;
      function GetField(RecordNumber: LongWord; FieldName: string): TDbfVariant; overload;
      // ��������� ���������� ���� (�������� NULL ���������� ����������)
      function ReadCharacter(RecordNumber: LongInt; FieldNumber: byte): string; overload;
      function ReadCharacter(RecordNumber: LongInt; FieldName: string): string; overload;
      // ��������� ���� (�������� NULL ���������� ����������)
      function ReadDate(RecordNumber: LongInt; FieldNumber: byte): TDate; overload;
      function ReadDate(RecordNumber: LongInt; FieldName: string): TDate; overload;
      // ��������� �������� ���� (�������� NULL ���������� ����������)
      function ReadNumeric(RecordNumber: LongInt; FieldNumber: byte): real; overload;
      function ReadNumeric(RecordNumber: LongInt; FieldName: string): real; overload;
      // ��������� ���������� ���� (�������� NULL ���������� ����������)
      function ReadLogical(RecordNumber: LongInt; FieldNumber: byte): boolean; overload;
      function ReadLogical(RecordNumber: LongInt; FieldName: string): boolean; overload;
      // ��������� ���� � ��������� ������ (�������� NULL ���������� ����������)
      function ReadFloat(RecordNumber: LongInt; FieldNumber: byte): real; overload;
      function ReadFloat(RecordNumber: LongInt; FieldName: string): real; overload;
      // ������ �� ���� ���� Numeric ��� Float ������ ��������. ���� � ���� ��-
      // �������� ����� � ��������� ������ - ��������� ����������
      function ReadInteger(RecordNumber: LongInt; FieldNumber: byte): integer; overload;
      function ReadInteger(RecordNumber: LongInt; FieldName: string): integer; overload;
      // �������� ����� ���� �� ��� �����
      function GetFieldNumber(FieldName: string): byte;
      // ����� ���������� ��������
      function FindCharacter(FindFrom: LongWord; FindIn: byte; FindWhat: string;
      											 Direction: TSearchDirection): LongInt;
      // ����� �������� �������� ��� �������� � ��������� ������
      function FindNumericOrFloat(FindFrom: LongWord; FindIn: byte;
                                  FindWhat: real;
                                  Direction: TSearchDirection): LongWord;
      // ����� ����
      function FindDate(FindFrom: LongWord; FindIn: byte; FindWhat: TDate;
                            Direction: TSearchDirection): LongWord;
      // ����� ���������� ��������
      function FindLogical(FindFrom: LongWord; FindIn: byte; FindWhat: boolean;
                               Direction: TSearchDirection): LongWord;
      // �������� ����� ����������� ��������
      // (���������������, ��� ��� ��������� ����)
      function FindCharacterBinary(FindIn: byte; FindWhat: string): LongWord;
      // �������� ����� ��������� �������� ��� �������� � ��������� ������
      // (���������������, ��� ��� ��������� ����)
      function FindNumericOrFloatBinary(FindIn: byte;
                                        FindWhat: real): LongWord;
      // �������� ����� ����
      // (���������������, ��� ��� ��������� ����)
      function FindDateBinary(FindIn: byte; FindWhat: TDate): LongWord;
      // ������ ������
      property RecordSize : word read fRecordSize;
      // ����� �������
      property RecordsCount : LongWord read fRecordsCount;
      // ���� ���������� ����������
      property LastUpdated : TDate read fLastUpdated;
      // ����� �����
      property FieldsCount : byte read fFieldsCount;
      // ����������� ����������� ���������
      property IsCanModify : boolean read fIsCanModify;
      // ���� � ����� �� �����
      property DbfPath : string read fDbfPath;
      // ������ ��������� �����
      property HeaderSize : word read fDataOffset;
      // ������� ��� ������ �������, ���������� �� ��������
      property SkipDeleted : boolean read fSkipDeleted write fSkipDeleted;
  end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
implementation
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
constructor TDbfTable.Create(Path: string;
                             DecimalCharacter: TDecimalCharProperty;
                             LogicalSymbol : TLogicalType;
                             DateFormat: TDateFormat);

var
  reg : TRegistry;
  s : string;

begin
	inherited Create;
  fDbfID := 3;
  fLastUpdated := Date;
  fFieldsCount := 0;
  fFieldsList := nil;
  fRecordsCount := 0;
  fDataOffset := 0;
  fRecordSize := 0;
  fIsCanModify := true;
  fIsFileClosed := true;
  if ValidatePath(Path) = false
  	then
    	begin
      	raise EDbfInitError.CreateFmt('"%s" is not a valid path', [Path]);
      end
    else
    	begin
		  	fDbfPath := Path;
      end;
  if DecimalCharacter = dcpPoint
    then
      begin
        fDecimalChar := '.';
      end
    else
      begin
        fDecimalChar := ',';
      end;
  fLogicalSymbol := LogicalSymbol;
  fSkipDeleted := false;
  reg := TRegistry.Create;
  reg.RootKey := $80000001;
  if reg.OpenKey('Control Panel\International', false) = true then
  	begin
		  s := reg.ReadString('sDecimal');
      fRegistryDecimalChar := s[1];
    end;
  reg.Free;
  fDateFormat := DateFormat;
  if FileExists(fDbfPath) = true then
  	begin
    	OpenTable;
      fIsCanModify := false;
    end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.ValidatePath(InPath: string): boolean;

var
	sFolder : string;
  sFileName : string;

begin
	result := true;
  sFolder := ExtractFilePath(InPath);
  sFileName := ExtractFileName(InPath);
  if ((DirectoryExists(sFolder) = false) and (sFolder = '')) or (sFileName = '')
    then
  	  begin
    	  result := false;
      end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.AddField(NewField: TDbfField);

var
  b : byte;

begin
  if fIsCanModify = true
    then
      begin
        b := CheckField(NewField);
        if b = 0
          then
            begin
              AddNewField(NewField);
            end
          else
            begin
              NewFieldErrorRaise(b);
            end;
      end
    else
      begin
        raise EDbfModifyError.Create('The table cannot be modified');
      end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.AddField(FieldName: string; FieldType: TDbfFieldType;
          FieldLength, FieldDecimals: byte);

var
	Tmp : TDbfField;
  b : byte;

begin
  if fIsCanModify = true
  	then
	  	begin
        with Tmp do
          begin
            dfNext := nil;
            dfFieldDecimals := FieldDecimals;
            dfFieldLength := FieldLength;
            dfFieldType := FieldType;
            dfFieldName := FieldName;
          end;
        b := CheckField(Tmp);
        if b = 0
          then
            begin
              AddNewField(Tmp);
            end
          else
            begin
              NewFieldErrorRaise(b);
            end;
      end
    else
      begin
        raise EDbfModifyError.Create('The table cannot be modified');
      end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.CheckField(NewField: TDbfField): byte;

begin
  result := 0;
  if CheckFieldName(NewField.dfFieldName) = false
    then
      begin
        result := 1; // ������������ ��� ����
      end
    else
      begin
        if NewField.dfFieldType = dftEmpty
          then
            begin
              result := 2; // �������� ��� ����
            end
          else
            begin
              if IsFieldExists(NewField.dfFieldName) = true
                then
                  begin
                    result := 3; // ������ ���� �����
                  end
                else
                  begin
                    case NewField.dfFieldType of
                      dftCharacter :
                        begin
                          if NewField.dfFieldLength > 254
                            then
                              begin
                                result := 4; // �������� ����� ����
                              end
                            else
                              begin
                                if NewField.dfFieldDecimals <> 0 then
                                  begin
                                    result := 5; // �������� ���-�� ������
                                                 // ����� �������
                                  end;
                              end;
                        end;
                      dftNumeric, dftFloat :
                        begin
                          if NewField.dfFieldLength > 20
                            then
                              begin
                                result := 4;
                              end
                            else
                              begin
                                if NewField.dfFieldDecimals > 15
                                  then
                                    begin
                                      result := 5;
                                    end
                                  else
                                    begin
                                      if ((NewField.dfFieldLength -
                                          NewField.dfFieldDecimals) < 2) and
                                          (NewField.dfFieldDecimals > 0)
                                      	then
                                    	    begin
                                      	    result := 5;
                                        	end;
                                    end;
                              end;
                        end;
                      dftDate :
                        begin
                          if NewField.dfFieldLength <> 8
                            then
                              begin
                                result := 4;
                              end
                            else
                              begin
                                if NewField.dfFieldDecimals <> 0 then
                                  begin
                                    result := 5;
                                  end;
                              end;
                        end;
                      dftLogical :
                        begin
                          if NewField.dfFieldLength <> 1
                            then
                              begin
                                result := 4;
                              end
                            else
                              begin
                                if NewField.dfFieldDecimals <> 0 then
                                  begin
                                    result := 5;
                                  end;
                              end;
                        end;
                    end;
                  end;
            end;
      end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.CheckFieldName(FieldName: string): boolean;

var
	i : integer;

begin
	result := true;
  if (FieldName = '') or (length(FieldName) > 11)
  	then
    	begin
      	result := false;
      end
    else
    	begin
      	if not(FieldName[1] in LatLetters)
        	then
          	begin
            	result := false;
            end
          else
          	begin
			        for i := 1 to length(FieldName) do
      			    begin
            			if not(FieldName[i] in AllowedChars) then
			              begin
      			          result := false;
            			    break;
			              end;
      			    end;
          	end;
      end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.AddNewField(NewField: TDbfField);

var
	TmpA : TDbfFieldPtr;
  TmpB : TDbfFieldPtr;

begin
  if fFieldsList = nil
  	then
    	begin
      	new(fFieldsList);
        fFieldsList^ := NewField;
        fFieldsCount := 1;
      end
    else
    	begin
        TmpA := fFieldsList;
        while TmpA^.dfNext <> nil do
        	begin
          	TmpA := TmpA^.dfNext;
          end;
        new(TmpB);
        TmpB^ := NewField;
        TmpA^.dfNext := TmpB;
        inc(fFieldsCount);
      end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.IsFieldExists(FieldName: string): boolean;

var
	TmpA : TDbfFieldPtr;

begin
	result := false;
  TmpA := fFieldsList;
  while TmpA <> nil do
  	begin
      if FieldName = TmpA^.dfFieldName then
      	begin
        	result := true;
          break;
        end;
    	TmpA := TmpA^.dfNext;
    end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.OpenTable;

var
	sYear : string;
  sMonth : string;
  sDay : string;
  b : byte;
  i : integer;
  s : string;
  j : integer;
  NameChar : char;
  IsReaded : boolean;
  TmpField : TDbfField;
  FillerA : array[1..4] of byte;
  FillerB : array[1..14] of byte;
  FillerC : array[1..20] of byte;

begin
	fErrorCode := 0;
  try
  		try
		  		AssignFile(fFile, fDbfPath);
    			reset(fFile);
          fIsFileClosed := false;
		      if FileSize(fFile) < 65
    		  	then
        			begin
                raise EDbfInitError.CreateFmt('%s is not a dbf-file',
                                              [fDbfPath]);
                fErrorCode := 1;
		          end
    		    else
        			begin
                read(fFile, fDbfID);
                if fDbfID <> 3
                	then
                  	begin
                      raise EDbfInitError.CreateFmt('%s is not a dBase file ' +
                                                    'without Memo', [fDbfPath]);
                      fErrorCode := 1;
                    end
                  else
                  	begin
                      // ������ ���� ���������� ����������
                      read(fFile, b);
                      sYear := IntToStr(b);
                      read(fFile, b);
                      sMonth := IntToStr(b);
                      read(fFile, b);
                      sDay := IntToStr(b);
                      fLastUpdated := StrToDate(sDay + '.' + sMonth + '.' +
                                      sYear);
                      // ������ ���-�� �������
                      BlockRead(fFile, fRecordsCount, 4);
                      // ������ ��������� ������ ������
                      BlockRead(fFile, fDataOffset, 2);
                      // ������ ������ ������
                      BlockRead(fFile, fRecordSize, 2);
                      // ��������� ����� �����
                      fFieldsCount := (fDataOffset - 1) div 32 - 1;
                      // ������ ����������� (��� �������)
                      BlockRead(fFile, FillerC, 20);
                      // ������ ����
                      for i := 1 to fFieldsCount do
                      	begin
                        	// ��� ����
                          s := '';
                          IsReaded := false;
                          with TmpField do
                          	begin
                            	dfFieldDecimals := 0;
                              dfFieldLength := 0;
                              dfFieldType := dftEmpty;
                              dfFieldName := '';
                              dfNext := nil;
                            end;
                          for j := 1 to 10 do
                          	begin
                            	read(fFile, b);
                              NameChar := chr(b);
                              if NameChar in AllowedChars
                              	then
                              		begin
                                		if IsReaded = false then
                                    	begin
                                      	s := s + NameChar;
                                      end;
                                	end
                                else
                                	begin
                                  	IsReaded := true;
                                  end;
                            end;
                          read(fFile, b);
                          TmpField.dfFieldName := s;
                          // ��� ����
                          read(fFile, b);
                          NameChar := chr(b);
                          case NameChar of
                          	'C' : TmpField.dfFieldType := dftCharacter;
                            'N' : TmpField.dfFieldType := dftNumeric;
                            'D' : TmpField.dfFieldType := dftDate;
                            'L' : TmpField.dfFieldType := dftLogical;
                            'F' : TmpField.dfFieldType := dftFloat;
                          end;
                          // ������ �����������
                          BlockRead(fFile, FillerA, 4);
                          // ����� ����
                          read(fFile, b);
                          TmpField.dfFieldLength := b;
                          // ���-�� ������ ����� �������
                          read(fFile, b);
                          TmpField.dfFieldDecimals := b;
                          // ������ �����������
                          BlockRead(fFile, FillerB, 14);
                          b := CheckField(TmpField);
                          if b = 0
                          	then
                            	begin
                              	AddNewField(TmpField);
                              end
                            else
                            	begin
                              	NewFieldErrorRaise(b);
                              end;
                        end;
                    end;
		          end;
		    except
    			on EInOutError do
      			begin
		        	raise EDbfFileAccessError.Create('I/O error');
    		    end;
      end;
  	finally
    	if fErrorCode <> 0 then
      	begin
        	if fIsFileClosed = false then CloseFile(fFile);
          fIsFileClosed := true;
        end;
  end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.NewFieldErrorRaise(ErrCode: byte);

begin
  case ErrCode of
    1 :
      begin
        raise EDbfModifyError.Create('Incorrect field name');
      end;
    2 :
      begin
        raise EDbfModifyError.Create('Incorrect field type');
      end;
    3 :
      begin
        raise EDbfModifyError.Create('Field name already exists');
      end;
    4 :
      begin
        raise EDbfModifyError.Create('Incorrect field length');
      end;
    5 :
      begin
        raise EDbfModifyError.Create('Incorrect field decimals');
      end;
  end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.CreateTable;

const
  Reserved : array[1..20] of byte = (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                     0, 0, 0, 0, 0, 0);

var
	b : byte;
  s : string;
  i : integer;
  j : integer;

begin
  fErrorCode := 0;
	if fIsCanModify = true
    then
      begin
        if fFieldsCount > 0
          then
            begin
              try
                  try
                      AssignFile(fFile, fDbfPath);
                      rewrite(fFile);
                      fIsFileClosed := false;
                      // ����� �������������
                      b := 3;
                      write(fFile, b);
                      // ����� ���� ���������� ����������
                      WriteLastUpdated;
                      // ����� ����� �������
                      BlockWrite(fFile, fRecordsCount, 4);
                      // ����� ��������� ������ ������
                      fDataOffset := (fFieldsCount + 1) * 32 + 1;
                      BlockWrite(fFile, fDataOffset, 2);
                      // ����� ������ ������
                      for i := 1 to fFieldsCount do
                        begin
                          fRecordSize := fRecordSize + GetFieldLength(i);
                        end;
                      inc(fRecordSize);
                      BlockWrite(fFile, fRecordSize, 2);
                      // ����� �����������
                      BlockWrite(fFile, Reserved, 20);
                      // ����� ����
                      for i := 1 to fFieldsCount do
                        begin
                          // ��� ����
                          s := GetFieldName(i);
                          for j := 1 to 10 do
                            begin
                              if j > length(s)
                                then
                                  begin
                                    b := 0;
                                  end
                                else
                                  begin
                                    b := ord(s[j]);
                                  end;
                              write(fFile, b);
                            end;
                          b := 0;
                          write(fFile, b);
                          // ��� ����
                          case GetFieldType(i) of
                            dftCharacter :
                              begin
                                b := Ord('C');
                                write(fFile, b);
                              end;
                            dftNumeric :
                              begin
                                b := Ord('N');
                                write(fFile, b);
                              end;
                            dftDate :
                              begin
                                b := Ord('D');
                                write(fFile, b);
                              end;
                            dftLogical :
                              begin
                                b := Ord('L');
                                write(fFile, b);
                              end;
                            dftFloat :
                              begin
                                b := Ord('F');
                                write(fFile, b);
                              end;
                          end;
                          // ������ �����������
                          b := 0;
                          for j := 1 to 4 do
                            begin
                              write(fFile, b);
                            end;
                          // ����� ����
                          b := GetFieldLength(i);
                          write(fFile, b);
                          // ���-�� ������ ����� �������
                          b := GetFieldDecimals(i);
                          write(fFile, b);
                          // ������ �����������
                          b := 0;
                          for j := 1 to 14 do
                            begin
                              write(fFile, b);
                            end;
                        end;
                      // ����� ���������
                      b := 13;
                      write(fFile, b);
                    except
                      on EInOutError do
                        begin
                          fErrorCode := 1;
                        end;
                  end;
                finally
                  if fErrorCode <> 0 then
                    begin
                      if fIsFileClosed = false then CloseFile(fFile);
                      fIsFileClosed := true;
                    end;
                  fIsCanModify := false;
              end;
            end
          else
            begin
              raise EDbfInitError.Create('The table cannot be created ' +
                                         'because fields are not defined');
            end;
      end
    else
      begin
        raise EDbfModifyError.Create('The table cannot be modified');
      end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.WriteLastUpdated;

var
  sDay : string;
  sMonth : string;
  sYear : string;
  s : string;
  b : byte;

begin
  seek(fFile, 1);
  s := DateToStr(Date);
  sDay := copy(s, 1, 2);
  sMonth := copy(s, 4, 2);
  sYear := copy(s, 9, 2);
  b := StrToInt(sYear);
  write(fFile, b);
  b := StrToInt(sMonth);
  write(fFile, b);
  b := StrToInt(sDay);
  write(fFile, b);
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.GetFieldLength(FieldNo: byte): byte;

var
	Tmp : TDbfFieldPtr;
  k : byte;

begin
	result := 0;
  Tmp := fFieldsList;
  k := 1;
  while Tmp <> nil do
  	begin
      if k = FieldNo then
      	begin
        	result := Tmp^.dfFieldLength;
          break;
        end;
    	Tmp := Tmp^.dfNext;
      inc(k);
    end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.GetFieldName(FieldNo: byte): string;

var
	Tmp : TDbfFieldPtr;
  k : word;

begin
	result := '';
  Tmp := fFieldsList;
  k := 1;
  while Tmp <> nil do
  	begin
      if k = FieldNo then
      	begin
        	result := Tmp^.dfFieldName;
          break;
        end;
    	Tmp := Tmp^.dfNext;
      inc(k);
    end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.GetFieldType(FieldNo: byte): TDbfFieldType;

var
	Tmp : TDbfFieldPtr;
  k : byte;

begin
	result := dftEmpty;
  Tmp := fFieldsList;
  k := 1;
  while Tmp <> nil do
  	begin
      if k = FieldNo then
      	begin
        	result := Tmp^.dfFieldType;
          break;
        end;
    	Tmp := Tmp^.dfNext;
      inc(k);
    end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.GetFieldDecimals(FieldNo: byte): byte;

var
	Tmp : TDbfFieldPtr;
  k : byte;

begin
	result := 0;
  Tmp := fFieldsList;
  k := 1;
  while Tmp <> nil do
  	begin
      if k = FieldNo then
      	begin
        	result := Tmp^.dfFieldDecimals;
          break;
        end;
    	Tmp := Tmp^.dfNext;
      inc(k);
    end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.PopField;

var
	Tmp : TDbfFieldPtr;

begin
  Tmp := fFieldsList;
  fFieldsList := Tmp^.dfNext;
  dispose(Tmp);
  dec(fFieldsCount);
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.FreeFieldsList;

begin
  while fFieldsList <> nil do PopField;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.Free;

begin
	FreeFieldsList;
  if fIsFileClosed = false then CloseFile(fFile);
  inherited Free;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.IsRecordDeleted(RecordNumber: LongWord): boolean;

var
	SeekTo : integer;
  b : byte;

begin
  if (RecordNumber < 1) or (RecordNumber > fRecordsCount)
  	then
    	begin
      	raise EDbfSeekError.CreateFmt('Invalid record number (%d)',
                                      [RecordNumber]);
      end
    else
    	begin
  	  	SeekTo := fDataOffset + fRecordSize * (RecordNumber - 1);
        seek(fFile, SeekTo);
        read(fFile, b);
        if b = 42
        	then
          	begin
            	result := true;
            end
          else
          	begin
            	result := false;
            end;
      end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.DeleteRecord(RecordNumber: LongWord);

var
	SeekTo : integer;
  b : byte;

begin
  if (RecordNumber < 1) or (RecordNumber > fRecordsCount) or
     (fIsFileClosed = true)
    then
      begin
        raise EDbfSeekError.CreateFmt('Invalid record number (%d)',
                                      [RecordNumber]);
      end
    else
      begin
        SeekTo := fDataOffset + fRecordSize * (RecordNumber - 1);
        seek(fFile, SeekTo);
        b := 42;
        write(fFile, b);
      end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.UndeleteRecord(RecordNumber: LongWord);

var
	SeekTo : integer;
  b : byte;

begin
  if (RecordNumber < 1) or (RecordNumber > fRecordsCount) or
  	 (fIsFileClosed = true)
    then
      begin
        raise EDbfSeekError.CreateFmt('Invalid record number (%d)',
                                      [RecordNumber]);
      end
    else
      begin
        SeekTo := fDataOffset + fRecordSize * (RecordNumber - 1);
        seek(fFile, SeekTo);
        b := 32;
        write(fFile, b);
      end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.GetFieldParameters(FieldNumber: byte): TDbfField;

var
	Tmp : TDbfFieldPtr;
  k : integer;

begin
  if (FieldNumber < 1) or (FieldNumber > fFieldsCount)
    then
      begin
        raise EDbfFieldError.CreateFmt('Invalid field number (%d)',
                                       [FieldNumber]);
      end
    else
      begin
        result.dfFieldType := dftEmpty;
        Tmp := fFieldsList;
        k := 1;
        while Tmp <> nil do
          begin
            if k = FieldNumber then
              begin
                result := Tmp^;
                break;
              end;
            Tmp := Tmp^.dfNext;
            inc(k);
          end;
    end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.ImportStructure(ImportFrom: string);

begin
  if fFieldsCount > 0
    then
      begin
        raise EDbfInitError.Create('Invalid structure import');
      end
    else
      begin
        if FileExists(ImportFrom) = false
          then
            begin
              raise EDbfInitError.CreateFmt('Invalid import path (%s)',
                                            [ImportFrom]);
            end
          else
            begin
              if CopyFile(PChar(ImportFrom), PChar(fDbfPath), true) = true
                then
                  begin
                    OpenTable;
                    Zap;
                  end
                else
                  begin
                    raise EDbfInitError.Create('File already exists');
                  end;
            end;
      end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.Zap;

var
  i : integer;

begin
  //CloseFile(fFile);
  CopyFile(PChar(fDbfPath), PChar(ChangeFileExt(fDbfPath, '.bak')), false);
  for i := 1 to Self.RecordsCount do
    begin
      Self.DeleteRecord(i);
    end;
  Self.Pack;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.Append;

var
	i : integer;
  b : byte;

begin
	// ���������� ������ ������
  if fRecordsCount = 0
  	then
    	begin
			  seek(fFile, FileSize(fFile));
      end
    else
    	begin
      	seek(fFile, fDataOffset + fRecordSize * fRecordsCount);
      end;
  b := 0;
  for i := 1 to fRecordSize do
  	begin
      write(fFile, b);
    end;
  b := 26;
  write(fFile, b);
  // ��������� ���� ���������� ����������
  WriteLastUpdated;
  // ���������� ����� �������
  inc(fRecordsCount);
  BlockWrite(fFile, fRecordsCount, 4);
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.Pack;

var
  f : file of byte;
  s : string;
  b : byte;
  i : integer;
  j : integer;
  k : integer;
  SeekTo : integer;

begin
  fErrorCode := 0;
  try
      try
          s := ExtractFilePath(fDbfPath) + 'pack.tmp';
          AssignFile(f, s);
          rewrite(f);
          // ������������ ���������
          seek(fFile, 0);
          for i := 1 to fDataOffset do
            begin
              read(fFile, b);
              write(f, b);
            end;
          i := 0;
          // ������������ ������
          for k := 1 to fRecordsCount do
            begin
              SeekTo := fDataOffset + fRecordSize * (k - 1);
              seek(fFile, SeekTo);
              read(fFile, b);
              if b <> 42 then
                begin
                  b := 0;
                  write(f, b);
                  for j := 1 to fRecordSize - 1 do
                    begin
                      read(fFile, b);
                      write(f, b);
                    end;
                  inc(i);
                end;
            end;
          //b := 26;
          //write(f, b);
          seek(f, 4);
          BlockWrite(f, i, 4);
          fRecordsCount := i;
        except
          on EInOutError do
            begin
              fErrorCode := 1;
            end;
      end;
    finally
      CloseFile(f);
  end;
  CloseFile(fFile);
  CopyFile(PChar(fDbfPath), PChar(ChangeFileExt(fDbfPath, '.bak')), false);
  CopyFile(PChar(s), PChar(fDbfPath), false);
  if FileExists(s) then DeleteFile(PChar(s));
  AssignFile(fFile, fDbfPath);
  reset(fFile);
  WriteLastUpdated;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.GetField(RecordNumber : LongWord;
                            FieldNumber: byte): TDbfVariant;

var
	SeekTo : integer;
  k : integer;
  Tmp : TDbfFieldPtr;
  FieldLength : byte;
  i : integer;
  s : string;
  b : byte;
  st : string;

begin
	with result do
  	begin
    	ValueLogical := false;
      ValueFloat := 0;
      ValueDate := 0;
      ValueCharacter := '';
      ValueNumeric := 0;
      VarType := dftEmpty;
      IsNull := false;
    end;
  if (RecordNumber < 1) or (RecordNumber > fRecordsCount)
    then
      begin
        raise EDbfSeekError.CreateFmt('Invalid record number (%d)',
                                      [RecordNumber]);
      end
    else
      begin
        if (FieldNumber < 1) or (FieldNumber > fFieldsCount)
          then
            begin
              raise EDbfFieldError.CreateFmt('Invalid field number (%d)',
                                   [FieldNumber]);
            end
          else
            begin
              if (fIsFileClosed = true)
                then
                  begin
                    raise EDbfInitError.Create('Table closed');
                  end
                else
                  begin
                    // ������� ������ ������
                    SeekTo := fDataOffset + fRecordSize * (RecordNumber - 1);
                    // ���������� ������ ��������
                    inc(SeekTo);
                    // ���� ������ ����
                    Tmp := fFieldsList;
                    k := 1;
                    while k < FieldNumber do
                      begin
                        SeekTo := SeekTo + Tmp^.dfFieldLength;
                        Tmp := Tmp^.dfNext;
                        inc(k);
                      end;
                    // ���������� ��� ����� � ���
                    FieldLength := Tmp^.dfFieldLength;
                    result.VarType := Tmp^.dfFieldType;
                    // ������
                    seek(fFile, SeekTo);
                    s := '';
                    for i := 1 to FieldLength do
                      begin
                        read(fFile, b);
                        s := s + chr(b);
                      end;
                    // �������������� ���������� ��������
                    case result.VarType of
                      dftCharacter :
                        begin
                          result.ValueCharacter := trim(s);
                        end;
                      dftNumeric, dftFloat :
                        begin
                          if trim(s) = ''
                            then
                              begin
                                result.IsNull := true;
                              end
                            else
                              begin
                                try
                                    result.ValueNumeric := SafeCnvStrToFloat(s);
                                  except
                                    on EConvertError do
                                      begin
                                        raise EDbfReadError.CreateFmt('$s is ' +
                                              'not a valid floating point ' +
                                              'value', [s]);
                                      end;
                                end;
                              end;
                        end;
                      dftDate :
                        begin
                          if trim(s) = ''
                            then
                              begin
                                result.IsNull := true;
                              end
                            else
                              begin
                                try
                                    case fDateFormat of
                                      dfDDMMYYYY :
                                        begin
                                          s := DateAddDelimiter(s);
                                          result.ValueDate := StrToDate(s);
                                        end;
                                      dfMMDDYYYY :
                                        begin
                                          st := copy(s, 3, 2) + copy(s, 1, 2) +
                                                copy(s, 5, 4);
                                          s := DateAddDelimiter(st);
                                          result.ValueDate := StrToDate(s);
                                        end;
                                      dfYYYYMMDD :
                                        begin
                                          st := copy(s, 7, 2) + copy(s, 5, 2) +
                                                copy(s, 1, 4);
                                          s := DateAddDelimiter(st);
                                          result.ValueDate := StrToDate(s);
                                        end;
                                    end;
                                  except
                                    on EConvertError do
                                      begin
                                        raise EDbfReadError.CreateFmt('$s is ' +
                                              'not a valid date', [s]);
                                      end;
                                end;
                              end;
                        end;
                      dftLogical :
                        begin
                          if s = ''
                            then
                              begin
                                result.IsNull := true;
                              end
                            else
                              begin
                                case s[1] of
                                  'T', 't', 'Y', 'y' :
                                    begin
                                      result.ValueLogical := true;
                                    end;
                                  'F', 'f', 'N', 'n' :
                                    begin
                                      result.ValueLogical := false;
                                    end;
                                  else
                                    begin
                                      raise EDbfReadError.CreateFmt('$s is ' +
                                            'not a valid logical value', [s]);
                                    end;
                                end;
                              end;
                        end;
                    end;
                  end;
            end;
      end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.SafeCnvStrToFloat(InFloat: string): real;

var
	DecimalPointPlace : integer;

begin
  if fDecimalChar <> fRegistryDecimalChar then
  	begin
    	DecimalPointPlace := pos(fDecimalChar, InFloat);
      InFloat[DecimalPointPlace] := fRegistryDecimalChar;
    end;
  result := StrToFloat(InFloat);
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.DateAddDelimiter(InDate: string): string;

begin
  result := copy(InDate, 1, 2) + '.' + copy(InDate, 3, 2) + '.' +
            copy(InDate, 5, 4);
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.ReadCharacter(RecordNumber: LongInt;
                                 FieldNumber: byte): string;

var
  Tmp : TDbfVariant;

begin
  Tmp := GetField(RecordNumber, FieldNumber);
  if Tmp.VarType <> dftCharacter
    then
      begin
        raise EDbfTypeError.CreateFmt('Field %d is not Character',
                                      [FieldNumber]);
      end
    else
      begin
        if Tmp.IsNull = true
          then
            begin
              raise EDbfNullError.CreateFmt('NULL value found in record %d, ' +
                                  'field %d', [RecordNumber, FieldNumber]);
            end
          else
            begin
              result := Tmp.ValueCharacter;
            end;
      end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.ReadDate(RecordNumber: LongInt; FieldNumber: byte): TDate;

var
  Tmp : TDbfVariant;

begin
  Tmp := GetField(RecordNumber, FieldNumber);
  if Tmp.VarType <> dftDate
    then
      begin
        raise EDbfTypeError.CreateFmt('Field %d is not Date', [FieldNumber]);
      end
    else
      begin
        if Tmp.IsNull = true
          then
            begin
              raise EDbfNullError.CreateFmt('NULL value found in record %d, ' +
                                  'field %d', [RecordNumber, FieldNumber]);
            end
          else
            begin
              result := Tmp.ValueDate;
            end;
      end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.ReadNumeric(RecordNumber: LongInt; FieldNumber: byte): real;

var
  Tmp : TDbfVariant;

begin
  Tmp := GetField(RecordNumber, FieldNumber);
  if Tmp.VarType <> dftNumeric
    then
      begin
        raise EDbfTypeError.CreateFmt('Field %d is not Numeric', [FieldNumber]);
      end
    else
      begin
        if Tmp.IsNull = true
          then
            begin
              raise EDbfNullError.CreateFmt('NULL value found in record %d, ' +
                                  'field %d', [RecordNumber, FieldNumber]);
            end
          else
            begin
              result := Tmp.ValueNumeric;
            end;
      end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.ReadLogical(RecordNumber: LongInt;
                               FieldNumber: byte): boolean;

var
  Tmp : TDbfVariant;

begin
  Tmp := GetField(RecordNumber, FieldNumber);
  if Tmp.VarType <> dftLogical
    then
      begin
        raise EDbfTypeError.CreateFmt('Field %d is not Logical', [FieldNumber]);
      end
    else
      begin
        if Tmp.IsNull = true
          then
            begin
              raise EDbfNullError.CreateFmt('NULL value found in record %d, ' +
                                  'field %d', [RecordNumber, FieldNumber]);
            end
          else
            begin
              result := Tmp.ValueLogical;
            end;
      end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.ReadFloat(RecordNumber: LongInt; FieldNumber: byte): real;

var
  Tmp : TDbfVariant;

begin
  Tmp := GetField(RecordNumber, FieldNumber);
  if Tmp.VarType <> dftFloat
    then
      begin
        raise EDbfTypeError.CreateFmt('Field %d is not Float', [FieldNumber]);
      end
    else
      begin
        if Tmp.IsNull = true
          then
            begin
              raise EDbfNullError.CreateFmt('NULL value found in record %d, ' +
                                  'field %d', [RecordNumber, FieldNumber]);
            end
          else
            begin
              result := Tmp.ValueFloat;
            end;
      end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.WriteValue(RecordNumber: LongWord; FieldNumber: byte;
                               NewValue: TDbfVariant);

var
	SeekTo : integer;
  k : integer;
  Tmp : TDbfFieldPtr;
  FieldLength : byte;
  i : integer;
  s : string;
  b : byte;
  TmpA : TDbfField;

begin
  if (RecordNumber < 1) or (RecordNumber > fRecordsCount)
    then
      begin
        raise EDbfSeekError.CreateFmt('Invalid record number (%d)',
                                      [RecordNumber]);
      end
    else
      begin
        if fIsFileClosed = true
          then
            begin
              raise EDbfInitError.Create('Table closed');
            end
          else
            begin
              if (FieldNumber < 1) or (FieldNumber > fFieldsCount)
              	then
                	begin
                  	raise EDbfFieldError.CreateFmt('Invalid field number (%d)',
                                   [FieldNumber]);
                  end
                else
                	begin
                    TmpA := GetFieldParameters(FieldNumber);
                  	s := ConvertValue(NewValue, TmpA);
                    b := CheckValue(s, TmpA);
                    if b > 0
                    	then
                      	begin
                        	WriteFieldErrorRaise(b);
                        end
                      else
                      	begin
                        	// ������� ������ ������
                          SeekTo := fDataOffset + fRecordSize *
                                    (RecordNumber - 1);
                          // ���������� ������ ��������
                          inc(SeekTo);
                          // ���� ������ ����
                          Tmp := fFieldsList;
                          k := 1;
                          while k < FieldNumber do
                            begin
                              SeekTo := SeekTo + Tmp^.dfFieldLength;
                              Tmp := Tmp^.dfNext;
                              inc(k);
                            end;
                        	seek(fFile, SeekTo);
                          // ���������� ��� �����
              		        FieldLength := TmpA.dfFieldLength;
                          // ����������� ����� ������, ������ �����, ����
                          //  � ���������� �� �������
                          case NewValue.VarType of
                            dftCharacter :
                            	begin
                              	s := AlignValue(s, FieldLength, true);
                              end;
                            dftNumeric, dftFloat :
                              begin
                              	s := AlignValue(s, FieldLength, false);
                              end;
                          end;
                          // �����
                          for i := 1 to FieldLength do
                            begin
                            	b := ord(s[i]);
                              write(fFile, b);
                            end;
                          // ���������� ���� ���������� ����������
                          WriteLastUpdated;
                        end;
                  end;
            end;
      end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.ConvertValue(NewValue: TDbfVariant;
                                TargetField : TDbfField): string;

var
	IntegerPart : integer;
  FractionalPart : real;
  FloatValue : real;
  IntegerString : string;
  FractionalString : string;
  s : string;

begin
	FloatValue := 0;
	if NewValue.VarType = dftNumeric then
  	begin
    	FloatValue := NewValue.ValueNumeric;
    end;
  if NewValue.VarType = dftFloat then
  	begin
    	FloatValue := NewValue.ValueFloat;
    end;
  case NewValue.VarType of
    dftCharacter :
    	begin
      	result := trim(NewValue.ValueCharacter);
      end;
    dftNumeric, dftFloat :
    	begin
        if Trunc(FloatValue) <> FloatValue
          then
            begin
              IntegerPart := Trunc(FloatValue);
              FractionalPart := Frac(FloatValue);
              IntegerString := IntToStr(IntegerPart);
              FractionalString := FloatToStrF(FractionalPart, ffFixed, 18,
                                  TargetField.dfFieldDecimals);
              delete(FractionalString, 1, 2);
              result := IntegerString + fDecimalChar + FractionalString;
            end
          else
            begin
              result := FloatToStr(FloatValue);
              if TargetField.dfFieldDecimals > 0 then result := result +
                                                                fDecimalChar;
            end;
        while length(result) - pos(fDecimalChar, result) <
              TargetField.dfFieldDecimals do
          begin
            result := result + '0';
          end;
      end;
    dftDate :
    	begin
      	s := DateToStr(NewValue.ValueDate);
        delete(s, 6, 1);
        delete(s, 3, 1);
        case fDateFormat of
          dfDDMMYYYY :
            begin
              result := s;
            end;
          dfMMDDYYYY :
            begin
              result := copy(s, 3, 2) + copy(s, 1, 2) + copy(s, 5, 4);
            end;
          dfYYYYMMDD :
            begin
              result := copy(s, 5, 4) + copy(s, 3, 2) + copy(s, 1, 2);
            end;
        end;
      end;
    dftLogical :
    	begin
      	if NewValue.ValueLogical = true
        	then
          	begin
            	if fLogicalSymbol = ltTrueFalse
              	then
                	begin
			            	result := 'T';
                  end
                else
                	begin
                  	result := 'Y';
                  end;
            end
          else
          	begin
            	if fLogicalSymbol = ltYesNo
              	then
                	begin
                  	result := 'N';
                  end
                else
                	begin
                  	result := 'F';
                  end;
            end;
      end;
  end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.CheckValue(NewValue: string; TargetField: TDbfField): byte;

var
  CheckFieldType : TDbfFieldType;
  CheckFieldLength : byte;
  CheckFieldDecimals : byte;
  s : string;
  PosDelimiter : integer;

begin
	result := 0;
  CheckFieldType := TargetField.dfFieldType;
  CheckFieldLength := TargetField.dfFieldLength;
  case CheckFieldType of
    dftCharacter :
    	begin
      	if length(NewValue) > CheckFieldLength then
        	begin
          	result := 1; // �������� ����� ����
          end;
      end;
    dftNumeric, dftFloat :
    	begin
      	CheckFieldDecimals := TargetField.dfFieldDecimals;
      	PosDelimiter := pos(fDecimalChar, NewValue);
        s := copy(NewValue, PosDelimiter + 1, length(NewValue) - PosDelimiter);
        if (length(s) > CheckFieldDecimals) and (PosDelimiter > 0)
        	then
          	begin
              result := 2; // �������� ����� ������� �����
            end
          else
          	begin
        			if length(NewValue) > CheckFieldLength then
              	begin
                	result := 1;
                end;
            end;
      end;
    dftEmpty :
    	begin
      	result := 3; // �������� ��� ����
      end;
  end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.WriteFieldErrorRaise(ErrCode: byte);

begin
  case ErrCode of
    1 :
      begin
        raise EDbfWriteError.Create('Incorrect field length');
      end;
    2 :
      begin
      	raise EDbfWriteError.Create('Incorrect field decimals');
      end;
    3 :
      begin
        raise EDbfWriteError.Create('Incorrect field type');
      end;
  end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.AlignValue(InValue: string; OutLength: integer;
                              IsString: boolean): string;

begin
  while length(InValue) < OutLength do
  	begin
    	if IsString = true
      	then
        	begin
            InValue := InValue + ' ';
          end
        else
        	begin
          	InValue := ' ' + InValue;
          end;
    end;
  result := InValue;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.WriteCharacter(RecordNumber: LongInt; FieldNumber: byte;
                                   NewValue: string);

var
  Tmp : TDbfVariant;

begin
  with Tmp do
    begin
      ValueCharacter := NewValue;
      VarType := dftCharacter;
    end;
  WriteValue(RecordNumber, FieldNumber, Tmp);
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.WriteNumeric(RecordNumber: LongInt; FieldNumber: byte;
                                 NewValue: real);

var
  Tmp : TDbfVariant;

begin
  with Tmp do
    begin
      ValueNumeric := NewValue;
      VarType := dftNumeric;
    end;
  WriteValue(RecordNumber, FieldNumber, Tmp);
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.WriteDate(RecordNumber: LongInt; FieldNumber: byte;
                              NewValue: TDate);

var
  Tmp : TDbfVariant;

begin
  with Tmp do
    begin
      ValueDate := NewValue;
      VarType := dftDate;
    end;
  WriteValue(RecordNumber, FieldNumber, Tmp);
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.WriteLogical(RecordNumber: LongInt; FieldNumber: byte;
                                 NewValue: boolean);

var
  Tmp : TDbfVariant;

begin
  with Tmp do
    begin
      ValueLogical := NewValue;
      VarType := dftLogical;
    end;
  WriteValue(RecordNumber, FieldNumber, Tmp);
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.WriteFloat(RecordNumber: LongInt; FieldNumber: byte;
                               NewValue: real);

var
  Tmp : TDbfVariant;

begin
  with Tmp do
    begin
      ValueFloat := NewValue;
      VarType := dftFloat;
    end;
  WriteValue(RecordNumber, FieldNumber, Tmp);
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.ExchangeRecords(RecordA, RecordB: LongWord);

var
  BufferA : array[1..65536] of byte;
  BufferB : array[1..65536] of byte;
  SeekTo : LongInt;

begin
	if (RecordA < 1) or (RecordA > fRecordsCount) or (RecordB < 1) or
     (RecordB > fRecordsCount)
  	then
    	begin
      	raise EDbfSeekError.CreateFmt('Invalid record number (%d) or (%d)',
                                      [RecordA, RecordB]);
      end
    else
    	begin
        // ������� ������ ������ A
        SeekTo := fDataOffset + fRecordSize * (RecordA - 1);
        seek(fFile, SeekTo);
        // ������ �� ���
        BlockRead(fFile, BufferA, fRecordSize);
        // ������� ������ ������ B
        SeekTo := fDataOffset + fRecordSize * (RecordB - 1);
        seek(fFile, SeekTo);
        // ������ �� ���
        BlockRead(fFile, BufferB, fRecordSize);
        // ������� ������ ������ A
        SeekTo := fDataOffset + fRecordSize * (RecordA - 1);
        seek(fFile, SeekTo);
        // ����� ���� ������ B
        BlockWrite(fFile, BufferB, fRecordSize);
        // ������� ������ ������ B
        SeekTo := fDataOffset + fRecordSize * (RecordB - 1);
        seek(fFile, SeekTo);
        // ����� ���� ������ A
        BlockWrite(fFile, BufferA, fRecordSize);
    	end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.GetFieldNumber(FieldName: string): byte;

var
	Tmp : TDbfFieldPtr;
  k : integer;

begin
	result := 0;
  Tmp := fFieldsList;
  k := 1;
  while Tmp <> nil do
  	begin
      if AnsiUpperCase(Tmp^.dfFieldName) = AnsiUpperCase(FieldName) then
      	begin
        	result := k;
          break;
        end;
    	Tmp := Tmp^.dfNext;
      inc(k);
    end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.Sort(ByField: byte);

var
  i : integer;
  j : integer;
  FieldType : TDbfFieldType;
  x : real;
  y : real;
  s : string;
  st : string;
  d : TDate;
  dd : TDate;
  a : boolean;
  aa : boolean;

begin
  FieldType := GetFieldType(ByField);
  if (ByField > fFieldsCount) or (ByField < 1)
  	then
    	begin
      	raise EDbfFieldError.CreateFmt('Invalid field number (%d)', [ByField]);
      end
    else
    	begin
      	if fIsFileClosed = true
        	then
          	begin
            	raise EDbfInitError.Create('Table closed');
            end
          else
          	begin
              for i := 1 to fRecordsCount - 1 do
                begin
                  for j := i + 1 to fRecordsCount do
                    begin
                      case FieldType of
                        dftCharacter :
                          begin
                            s := ReadCharacter(i, ByField);
                            st := ReadCharacter(j, ByField);
                            if s > st then
                              begin
                                ExchangeRecords(i, j);
                              end;
                          end;
                        dftNumeric :
                          begin
                            x := ReadNumeric(i, ByField);
                            y := ReadNumeric(j, ByField);
                            if x > y then
                              begin
                                ExchangeRecords(i, j);
                              end;
                          end;
                        dftFloat :
                          begin
                            x := ReadFloat(i, ByField);
                            y := ReadFloat(j, ByField);
                            if x > y then
                              begin
                                ExchangeRecords(i, j);
                              end;
                          end;
                        dftDate :
                          begin
                            d := ReadDate(i, ByField);
                            dd := ReadDate(j, ByField);
                            if d > dd then
                              begin
                                ExchangeRecords(i, j);
                              end;
                          end;
                        dftLogical :
                          begin
                            a := ReadLogical(i, ByField);
                            aa := ReadLogical(j, ByField);
                            if a > aa then
                              begin
                                ExchangeRecords(i, j);
                              end;
                          end;
                      end;
                    end;
                end;
            end;
      end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.FindCharacter(FindFrom: LongWord; FindIn: byte;
                                 FindWhat: string;
                                 Direction: TSearchDirection): LongInt;

var
  i : integer;

begin
  result := 0;
  if (FindFrom < 1) or (FindFrom > fRecordsCount)
  	then
    	begin
      	raise EDbfSeekError.CreateFmt('Invalid record number (%d)',
                                      [FindFrom]);
      end
    else
    	begin
      	if (FindIn < 1) or (FindIn > fFieldsCount)
        	then
          	begin
            	raise EDbfFieldError.CreateFmt('Invalid field number (%d)',
                                             [FindIn]);
            end
          else
          	begin
            	if GetFieldType(FindIn) <> dftCharacter
              	then
                	begin
                  	raise EDbfTypeError.CreateFmt('Field %d is not Character',
                                                  [FindIn]);
                  end
                else
                	begin
                  	if Direction = sdForward
                      then
                        begin
                          for i := FindFrom to fRecordsCount do
                            begin
                              if ReadCharacter(i, FindIn) = FindWhat then
                                begin
                                	if fSkipDeleted = false
                                  	then
                                    	begin
			                                  result := i;
      			                            break;
                                      end
                                    else
                                    	begin
                                      	if IsRecordDeleted(i) = false then
                                        	begin
                                          	result := i;
		      			                            break;
                                          end;
                                      end;
                                end;
                            end;
                        end
                      else
                        begin
                          for i := FindFrom downto 1 do
                            begin
                              if ReadCharacter(i, FindIn) = FindWhat then
                                begin
                                	if fSkipDeleted = false
                                  	then
                                    	begin
			                                  result := i;
      			                            break;
                                      end
                                    else
                                    	begin
                                        if IsRecordDeleted(i) = false then
                                        	begin
                                          	result := i;
		      			                            break;
                                          end;
                                      end;
                                end;
                            end;
                        end;
                  end;
            end;
      end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.FindNumericOrFloat(FindFrom: LongWord; FindIn: byte;
                                      FindWhat: real;
                                      Direction: TSearchDirection): LongWord;

var
  i : integer;
  Tmp : TDbfVariant;

begin
  result := 0;
  if (FindFrom < 1) or (FindFrom > fRecordsCount)
  	then
    	begin
      	raise EDbfSeekError.CreateFmt('Invalid record number (%d)',
                                      [FindFrom]);
      end
    else
    	begin
      	if (FindIn < 1) or (FindIn > fFieldsCount)
        	then
          	begin
            	raise EDbfFieldError.CreateFmt('Invalid field number (%d)',
                                             [FindIn]);
            end
          else
          	begin
            	if (GetFieldType(FindIn) <> dftNumeric) or
              	 (GetFieldType(FindIn) <> dftFloat)
              	then
                	begin
                  	raise EDbfTypeError.CreateFmt('Field %d is not Float or ' +
                                                  'Numeric', [FindIn]);
                  end
                else
                	begin
                    if Direction = sdForward
                      then
                        begin
                          for i := FindFrom to fRecordsCount do
                            begin
                              Tmp := GetField(i, FindIn);
                              if (Tmp.ValueNumeric = FindWhat) or
                                 (Tmp.ValueFloat = FindWhat)
                                  then
                                    begin
                                    	if fSkipDeleted = false
                                      	then
                                        	begin
			                                      result := i;
      			                                break;
                                          end
                                        else
                                        	begin
                                          	if IsRecordDeleted(i) = false then
                                            	begin
                                              	result := i;
				      			                            break;
                                              end;
                                          end;
                                    end;
                            end;
                        end
                      else
                        begin
                          for i := FindFrom downto 1 do
                            begin
                              Tmp := GetField(i, FindIn);
                              if (Tmp.ValueNumeric = FindWhat) or
                                 (Tmp.ValueFloat = FindWhat)
                                  then
                                    begin
                                    	if fSkipDeleted = false
                                      	then
                                        	begin
			                                      result := i;
      			                                break;
                                          end
                                        else
                                        	begin
                                          	if IsRecordDeleted(i) = false then
                                            	begin
                                              	result := i;
				      			                            break;
                                              end;
                                          end;
                                    end;
                            end;
                        end;
                	end;
            end;
      end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.FindDate(FindFrom: LongWord; FindIn: byte; FindWhat: TDate;
                            Direction: TSearchDirection): LongWord;

var
  i : integer;

begin
  result := 0;
  if (FindFrom < 1) or (FindFrom > fRecordsCount)
  	then
    	begin
      	raise EDbfSeekError.CreateFmt('Invalid record number (%d)',
                                      [FindFrom]);
      end
    else
    	begin
      	if (FindIn < 1) or (FindIn > fFieldsCount)
        	then
          	begin
            	raise EDbfFieldError.CreateFmt('Invalid field number (%d)',
                                             [FindIn]);
            end
          else
          	begin
            	if GetFieldType(FindIn) <> dftDate
              	then
                	begin
                  	raise EDbfTypeError.CreateFmt('Field %d is not Date',
                                                  [FindIn]);
                  end
                else
                	begin
                    if Direction = sdForward
                      then
                        begin
                          for i := FindFrom to fRecordsCount do
                            begin
                              if ReadDate(i, FindIn) = FindWhat then
                                begin
                                	if fSkipDeleted = false
                                  	then
                                    	begin
			                                  result := i;
      			                            break;
                                      end
                                    else
                                    	begin
                                      	if IsRecordDeleted(i) = false then
                                        	begin
                                          	result := i;
		      			                            break;
                                          end;
                                      end;
                                end;
                            end;
                        end
                      else
                        begin
                          for i := FindFrom downto 1 do
                            begin
                              if ReadDate(i, FindIn) = FindWhat then
                                begin
                                	if fSkipDeleted = false
                                  	then
                                    	begin
			                                  result := i;
      			                            break;
                                      end
                                    else
                                    	begin
                                      	if IsRecordDeleted(i) = false then
                                        	begin
                                          	result := i;
		      			                            break;
                                          end;
                                      end;
                                end;
                            end;
                        end;
                  end;
            end;
      end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.FindLogical(FindFrom: LongWord; FindIn: byte;
	                             FindWhat: boolean;
                               Direction: TSearchDirection): LongWord;

var
  i : integer;

begin
  result := 0;
  if (FindFrom < 1) or (FindFrom > fRecordsCount)
  	then
    	begin
      	raise EDbfSeekError.CreateFmt('Invalid record number (%d)',
                                      [FindFrom]);
      end
    else
    	begin
      	if (FindIn < 1) or (FindIn > fFieldsCount)
        	then
          	begin
            	raise EDbfFieldError.CreateFmt('Invalid field number (%d)',
                                             [FindIn]);
            end
          else
          	begin
            	if GetFieldType(FindIn) <> dftLogical
              	then
                	begin
                  	raise EDbfTypeError.CreateFmt('Field %d is not Logical',
                                                  [FindIn]);
                  end
                else
                	begin
                    if Direction = sdForward
                      then
                        begin
                          for i := FindFrom to fRecordsCount do
                            begin
                              if ReadLogical(i, FindIn) = FindWhat then
                                begin
                                	if fSkipDeleted = false
                                  	then
                                    	begin
			                                  result := i;
      			                            break;
                                      end
                                    else
                                    	begin
                                      	if IsRecordDeleted(i) = false then
                                        	begin
                                          	result := i;
		      			                            break;
                                          end;
                                      end;
                                end;
                            end;
                        end
                      else
                        begin
                          for i := FindFrom downto 1 do
                            begin
                              if ReadLogical(i, FindIn) = FindWhat then
                                begin
                                	if fSkipDeleted = false
                                  	then
                                    	begin
			                                  result := i;
      			                            break;
                                      end
                                    else
                                    	begin
                                      	if IsRecordDeleted(i) = false then
                                        	begin
                                          	result := i;
		      			                            break;
                                          end;
                                      end;
                                end;
                            end;
                        end;
                  end;
            end;
      end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.FindCharacterBinary(FindIn: byte; FindWhat: string): LongWord;

var
  LowBound : LongInt;
  HighBound : LongInt;
  i : integer;
  IsFound : boolean;
  CurrentElem : string;
  Tmp : TDbfField;

begin
  i := 0;
  Tmp := GetFieldParameters(FindIn);
  if Tmp.dfFieldType <> dftCharacter
    then
      begin
        raise EDbfTypeError.CreateFmt('Field %d is not Character', [FindIn]);
      end
    else
      begin
      	if Tmp.dfFieldLength < length(FindWhat)
        	then
          	begin
            	result := 0;
            end
          else
          	begin
              LowBound := 1;
              HighBound := fRecordsCount;
              IsFound := false;
              while (LowBound <= HighBound) and (IsFound = false) do
                begin
                  i := (LowBound + HighBound) div 2;
                  CurrentElem := ReadCharacter(i, FindIn);
                  if CurrentElem > FindWhat
                    then
                      begin
                        HighBound := i - 1;
                      end
                    else
                      begin
                        if CurrentElem < FindWhat
                          then
                            begin
                              LowBound := i + 1;
                            end
                          else
                            begin
                              IsFound := true;
                            end;
                      end;
                end;
              if IsFound = true
                then
                  begin
                    result := i;
                  end
                else
                  begin
                    result := 0;
                  end;
            end;
      end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.FindNumericOrFloatBinary(FindIn: byte;
                                            FindWhat: real): LongWord;

var
  LowBound : LongInt;
  HighBound : LongInt;
  i : integer;
  IsFound : boolean;
  CurrentElem : real;
  FieldType : TDbfFieldType;

begin
  i := 0;
  FieldType := GetFieldType(FindIn);
  if (FieldType <> dftNumeric) and (FieldType <> dftFloat)
    then
      begin
        raise EDbfTypeError.CreateFmt('Field %d is not Numeric or Float',
                                      [FindIn]);
      end
    else
      begin
        LowBound := 1;
        HighBound := fRecordsCount;
        IsFound := false;
        while (LowBound <= HighBound) and (IsFound = false) do
          begin
            i := (LowBound + HighBound) div 2;
            if FieldType = dftNumeric
              then
                begin
                  CurrentElem := ReadNumeric(i, FindIn);
                end
              else
                begin
                  CurrentElem := ReadFloat(i, FindIn);
                end;
            if CurrentElem > FindWhat
              then
                begin
                  HighBound := i - 1;
                end
              else
                begin
                  if CurrentElem < FindWhat
                    then
                      begin
                        LowBound := i + 1;
                      end
                    else
                      begin
                        IsFound := true;
                      end;
                end;
          end;
        if IsFound = true
          then
            begin
              result := i;
            end
          else
            begin
              result := 0;
            end;
      end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.FindDateBinary(FindIn: byte; FindWhat: TDate): LongWord;

var
  LowBound : LongInt;
  HighBound : LongInt;
  i : integer;
  IsFound : boolean;
  CurrentElem : TDate;

begin
  i := 0;
  if GetFieldType(FindIn) <> dftDate
    then
      begin
        raise EDbfTypeError.CreateFmt('Field %d is not Date', [FindIn]);
      end
    else
      begin
        LowBound := 1;
        HighBound := fRecordsCount;
        IsFound := false;
        while (LowBound <= HighBound) and (IsFound = false) do
          begin
            i := (LowBound + HighBound) div 2;
            CurrentElem := ReadDate(i, FindIn);
            if CurrentElem > FindWhat
              then
                begin
                  HighBound := i - 1;
                end
              else
                begin
                  if CurrentElem < FindWhat
                    then
                      begin
                        LowBound := i + 1;
                      end
                    else
                      begin
                        IsFound := true;
                      end;
                end;
          end;
        if IsFound = true
          then
            begin
              result := i;
            end
          else
            begin
              result := 0;
            end;
      end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.WriteNULL(RecordNumber: LongWord; FieldNumber: byte);

var
	SeekTo : integer;
  k : integer;
  Tmp : TDbfFieldPtr;
  FieldLength : byte;
  i : integer;
  b : byte;
  TmpA : TDbfField;

begin
  if (RecordNumber < 1) or (RecordNumber > fRecordsCount)
    then
      begin
        raise EDbfSeekError.CreateFmt('Invalid record number (%d)',
                                      [RecordNumber]);
      end
    else
      begin
        if fIsFileClosed = true
          then
            begin
              raise EDbfInitError.Create('Table closed');
            end
          else
            begin
              if (FieldNumber < 1) or (FieldNumber > fFieldsCount)
              	then
                	begin
                  	raise EDbfFieldError.CreateFmt('Invalid field number (%d)',
                                   [FieldNumber]);
                  end
                else
                	begin
                    // ������� ������ ������
                    SeekTo := fDataOffset + fRecordSize *
                              (RecordNumber - 1);
                    // ���������� ������ ��������
                    inc(SeekTo);
                    // ���� ������ ����
                    Tmp := fFieldsList;
                    k := 1;
                    while k < FieldNumber do
                      begin
                        SeekTo := SeekTo + Tmp^.dfFieldLength;
                        Tmp := Tmp^.dfNext;
                        inc(k);
                      end;
                    seek(fFile, SeekTo);
                    // ���������� ��� �����
                    TmpA := GetFieldParameters(FieldNumber);
                    FieldLength := TmpA.dfFieldLength;
                    // �����
                    b := 0;
                    for i := 1 to FieldLength do
                      begin
                        write(fFile, b);
                      end;
                    // ���������� ���� ���������� ����������
                    WriteLastUpdated;
                  end;
            end;
      end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.AddCharacter(FieldName: string; FieldLength: byte);

begin
  AddField(FieldName, dftCharacter, FieldLength, 0);
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.AddDate(FieldName: string);

begin
  AddField(FieldName, dftDate, 8, 0);
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.AddFloat(FieldName: string; FieldLength,
                             FieldDecimals: byte);

begin
  AddField(FieldName, dftFloat, FieldLength, FieldDecimals);
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.AddLogical(FieldName: string);

begin
  AddField(FieldName, dftLogical, 1, 0);
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.AddNumeric(FieldName: string; FieldLength,
                               FieldDecimals: byte);

begin
  AddField(FieldName, dftNumeric, FieldLength, FieldDecimals);
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.ReadAsString(RecordNumber: Integer;
                                FieldNumber: byte): string;

var
	Tmp : TDbfVariant;

begin
  Tmp := GetField(RecordNumber, FieldNumber);
  if Tmp.IsNull = false
  	then
    	begin
			  case GetFieldParameters(FieldNumber).dfFieldType of
          dftCharacter :
          	begin
            	result := Tmp.ValueCharacter;
            end;
          dftNumeric :
          	begin
            	result := FloatToStr(Tmp.ValueNumeric);
            end;
          dftFloat :
          	begin
            	result := FloatToStr(Tmp.ValueFloat);
            end;
          dftDate :
          	begin
            	result := DateToStr(Tmp.ValueDate);
            end;
          dftLogical :
            begin
            	case Self.fLogicalSymbol of
                ltTrueFalse :
                	begin
                  	if Tmp.ValueLogical = true
                    	then
                      	begin
                        	result := 'true';
                        end
                      else
                      	begin
                        	result := 'false';
                        end;
                  end;
                ltYesNo :
                  begin
                  	if Tmp.ValueLogical = true
                    	then
                      	begin
                        	result := 'yes';
                        end
                      else
                      	begin
                        	result := 'no';
                        end;
                  end;
              end;
            end;
        end;
      end
    else
    	begin
      	result := '';
      end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.WriteValue(RecordNumber: Integer; FieldName: string;
          NewValue : TDbfVariant);

var
  FldNum : byte;

begin
  FldNum := GetFieldNumber(FieldName);
  if NewValue.IsNull = true
    then
      begin
        WriteNULL(RecordNumber, FldNum);
      end
    else
      begin
        WriteValue(RecordNumber, FldNum, NewValue);
      end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.ReadInteger(RecordNumber: Integer; FieldNumber: byte): integer;

begin
	if GetFieldDecimals(FieldNumber) <> 0
  	then
    	begin
      	raise EDbfTypeError.CreateFmt('%d is not an integer field!',
                            [FieldNumber]);
      end
    else
    	begin
        case GetFieldType(FieldNumber) of
          dftNumeric :
            begin
              result := Trunc(ReadNumeric(RecordNumber, FieldNumber));
            end;
          dftFloat :
            begin
              result := Trunc(ReadFloat(RecordNumber, FieldNumber));
            end;
          else
            begin
              raise EDbfTypeError.CreateFmt('Field %d is not Numeric or Float',
                                  [FieldNumber]);
            end;
        end;
      end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.ReadInteger(RecordNumber: Integer; FieldName: string): integer;

var
	FieldNumber : byte;

begin
	FieldNumber := GetFieldNumber(FieldName);
  if GetFieldDecimals(FieldNumber) <> 0
  	then
    	begin
      	raise EDbfTypeError.CreateFmt('%d is not an integer field!',
                                  [FieldName]);
      end
    else
    	begin
        case GetFieldType(FieldNumber) of
          dftNumeric :
            begin
              result := Trunc(ReadNumeric(RecordNumber, FieldNumber));
            end;
          dftFloat :
            begin
              result := Trunc(ReadFloat(RecordNumber, FieldNumber));
            end;
          else
            begin
              raise EDbfTypeError.CreateFmt('Field %d is not Numeric or Float',
                                  [FieldName]);
            end;
        end;
      end;
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.GetField(RecordNumber: LongWord; FieldName:
                                          string): TDbfVariant;

begin
  result := GetField(RecordNumber, GetFieldNumber(FieldName));
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.ReadCharacter(RecordNumber: Integer;
                                 FieldName: string): string;

begin
  result := ReadCharacter(RecordNumber, GetFieldNumber(FieldName));
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.ReadDate(RecordNumber: Integer;
                            FieldName: string): TDate;

begin
  result := ReadDate(RecordNumber, GetFieldNumber(FieldName));
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.ReadFloat(RecordNumber: Integer;
                             FieldName: string): real;

begin
  result := ReadFloat(RecordNumber, GetFieldNumber(FieldName));
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.ReadLogical(RecordNumber: Integer;
                               FieldName: string): boolean;

begin
  result := ReadLogical(RecordNumber, GetFieldNumber(FieldName));
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
function TDbfTable.ReadNumeric(RecordNumber: Integer;
                               FieldName: string): real;

begin
  result := ReadNumeric(RecordNumber, GetFieldNumber(FieldName));
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.WriteCharacter(RecordNumber: Integer; FieldName,
                                   NewValue: string);

begin
  WriteCharacter(RecordNumber, GetFieldNumber(FieldName), NewValue);
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.WriteDate(RecordNumber: Integer; FieldName: string;
                              NewValue: TDate);

begin
  WriteDate(RecordNumber, GetFieldNumber(FieldName), NewValue);
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.WriteFloat(RecordNumber: Integer; FieldName: string;
                               NewValue: real);

begin
  WriteFloat(RecordNumber, GetFieldNumber(FieldName), NewValue);
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.WriteLogical(RecordNumber: Integer; FieldName: string;
                                 NewValue: boolean);

begin
  WriteLogical(RecordNumber, GetFieldNumber(FieldName), NewValue);
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.WriteNULL(RecordNumber: LongWord; FieldName: string);

begin
  WriteNULL(RecordNumber, GetFieldNumber(FieldName));
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
procedure TDbfTable.WriteNumeric(RecordNumber: Integer; FieldName: string;
                                 NewValue: real);

begin
  WriteNumeric(RecordNumber, GetFieldNumber(FieldName), NewValue);
end;
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
end.
