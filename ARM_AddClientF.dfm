object ARM_AddClientForm: TARM_AddClientForm
  Left = 0
  Top = 0
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsSingle
  Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1103
  ClientHeight = 454
  ClientWidth = 436
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  Scaled = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 3
    Top = 0
    Width = 429
    Height = 251
    Caption = #1044#1072#1085#1085#1099#1077' '#1087#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1103
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 0
    object Label1: TLabel
      Left = 7
      Top = 168
      Width = 84
      Height = 13
      Caption = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077':'
    end
    object Label2: TLabel
      Left = 7
      Top = 207
      Width = 61
      Height = 13
      Caption = #1044#1086#1083#1078#1085#1086#1089#1090#1100':'
    end
    object Image1: TImage
      Left = 273
      Top = 31
      Width = 152
      Height = 213
      Proportional = True
      Stretch = True
    end
    object Label3: TLabel
      Left = 297
      Top = 129
      Width = 100
      Height = 13
      Caption = #1060#1086#1090#1086' '#1085#1077' '#1085#1072#1081#1076#1077#1085#1086
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object LabeledEdit1: TLabeledEdit
      Left = 7
      Top = 31
      Width = 260
      Height = 19
      EditLabel.Width = 48
      EditLabel.Height = 13
      EditLabel.Caption = #1060#1072#1084#1080#1083#1080#1103':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object LabeledEdit2: TLabeledEdit
      Left = 7
      Top = 69
      Width = 260
      Height = 19
      EditLabel.Width = 23
      EditLabel.Height = 13
      EditLabel.Caption = #1048#1084#1103':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
    object LabeledEdit3: TLabeledEdit
      Left = 7
      Top = 107
      Width = 260
      Height = 19
      EditLabel.Width = 53
      EditLabel.Height = 13
      EditLabel.Caption = #1054#1090#1095#1077#1089#1090#1074#1086':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
    object ComboBox1: TComboBox
      Left = 7
      Top = 184
      Width = 260
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
    end
    object ComboBox2: TComboBox
      Left = 7
      Top = 223
      Width = 260
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
    end
    object LabeledEdit7: TLabeledEdit
      Left = 7
      Top = 145
      Width = 87
      Height = 19
      EditLabel.Width = 89
      EditLabel.Height = 13
      EditLabel.Caption = #1058#1072#1073#1077#1083#1100#1085#1099#1081' '#1085#1086#1084#1077#1088
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
    end
    object Button1: TButton
      Left = 136
      Top = 142
      Width = 131
      Height = 25
      Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1092#1086#1090#1086' ->'
      TabOrder = 4
      OnClick = Button1Click
    end
  end
  object GroupBox3: TGroupBox
    Left = 3
    Top = 252
    Width = 429
    Height = 53
    Caption = #1050#1072#1088#1090#1072' '#1076#1086#1089#1090#1091#1087#1072
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 1
    object Label6: TLabel
      Left = 238
      Top = 21
      Width = 99
      Height = 13
      Caption = #1044#1077#1081#1089#1090#1074#1080#1090#1077#1083#1100#1085#1072' '#1076#1086':'
    end
    object LabeledEdit6: TLabeledEdit
      Left = 46
      Top = 19
      Width = 88
      Height = 19
      TabStop = False
      EditLabel.Width = 35
      EditLabel.Height = 13
      EditLabel.Caption = #1050#1072#1088#1090#1072':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      LabelPosition = lpLeft
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
    end
    object BitBtn3: TBitBtn
      Left = 140
      Top = 13
      Width = 92
      Height = 31
      Caption = #1057#1095#1080#1090#1099#1074#1072#1090#1077#1083#1100
      TabOrder = 1
      OnClick = BitBtn3Click
    end
    object MaskEdit3: TMaskEdit
      Left = 343
      Top = 19
      Width = 82
      Height = 19
      Ctl3D = False
      EditMask = '!99/99/9999;1; '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 10
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      Text = '  .  .    '
    end
  end
  object BitBtn6: TBitBtn
    Left = 319
    Top = 415
    Width = 113
    Height = 35
    Caption = #1047#1072#1082#1088#1099#1090#1100
    TabOrder = 4
    OnClick = BitBtn6Click
  end
  object BitBtn7: TBitBtn
    Left = 207
    Top = 415
    Width = 106
    Height = 35
    Caption = #1054#1082
    TabOrder = 3
    OnClick = BitBtn7Click
  end
  object GroupBox2: TGroupBox
    Left = 3
    Top = 307
    Width = 429
    Height = 50
    Caption = #1051#1080#1084#1080#1090' '#1073#1077#1079#1085#1072#1083#1080#1095#1085#1099#1093' '#1087#1086#1082#1091#1087#1086#1082' '#1074' '#1084#1077#1089#1103#1094
    TabOrder = 2
    object Label4: TLabel
      Left = 100
      Top = 23
      Width = 36
      Height = 13
      Caption = #1088#1091#1073#1083#1077#1081
    end
    object Label5: TLabel
      Left = 190
      Top = 23
      Width = 177
      Height = 13
      Caption = #1048#1079#1088#1072#1089#1093#1086#1076#1086#1074#1072#1085#1086' '#1074' '#1090#1077#1082#1091#1097#1077#1084' '#1084#1077#1089#1103#1094#1077':'
    end
    object Edit1: TEdit
      Left = 7
      Top = 20
      Width = 87
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
  end
  object GroupBox4: TGroupBox
    Left = 3
    Top = 359
    Width = 429
    Height = 50
    Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
    TabOrder = 5
    object Edit2: TEdit
      Left = 7
      Top = 19
      Width = 418
      Height = 21
      TabOrder = 0
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnMessage = ApplicationEvents1Message
    Left = 368
    Top = 186
  end
end
