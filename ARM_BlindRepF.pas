unit ARM_BlindRepF;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  ARM_DataMod,
  Data.DB,
  IBX.IBDatabase,
  IBX.IBCustomDataSet,
  IBX.IBTable,
  Vcl.Grids,
  Vcl.DBGrids,
  Vcl.ExtCtrls,
  Vcl.StdCtrls,
  Vcl.ComCtrls,
  System.UITypes,
  System.DateUtils,
  Vcl.Buttons,
  System.Win.ComObj;

type
  TARM_BlindRepForm = class(TForm)
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    IBTable1: TIBTable;
    DataSource1: TDataSource;
    IBTransaction1: TIBTransaction;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    DateTimePicker1: TDateTimePicker;
    DateTimePicker2: TDateTimePicker;
    MonthCalendar1: TMonthCalendar;
    MonthCalendar2: TMonthCalendar;
    CheckBox3: TCheckBox;
    Edit1: TEdit;
    CheckBox4: TCheckBox;
    Edit2: TEdit;
    CheckBox5: TCheckBox;
    Edit3: TEdit;
    BitBtn1: TBitBtn;
    BitBtn4: TBitBtn;
    ComboBox1: TComboBox;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1TitleClick(Column: TColumn);
    procedure CheckBox1Click(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
    procedure CheckBox3Click(Sender: TObject);
    procedure CheckBox4Click(Sender: TObject);
    procedure CheckBox5Click(Sender: TObject);
    procedure DateTimePicker1Change(Sender: TObject);
    procedure MonthCalendar1Click(Sender: TObject);
    procedure MonthCalendar2Click(Sender: TObject);
    procedure DateTimePicker2Change(Sender: TObject);
    procedure Edit2Change(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure Edit3Change(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
  private
    { Private declarations }
    GridFilter: string;
    StDate1, StDate2, StFIO, StDiv, StPos: string;
    procedure ChangeDate1;
    procedure ChangeDate2;
    procedure ChangeFIO;
    procedure ChangeDiv;
    procedure ChangePos;
    procedure ToExcel;
    procedure ApplyFilter;
    function GetRecordsCount(DS: TIBTable): integer; //���������� ���������� ����������� ������� � ��������
    function GetStrFromDT(DT: TDateTime): string;
  public
    { Public declarations }
  end;

var
  ARM_BlindRepForm: TARM_BlindRepForm;

implementation

{$R *.dfm}

//-----------------------------------------------
//������� � ������ ������� �������
procedure TARM_BlindRepForm.ToExcel;
var
 a, b, c, id : Integer;
 ExApp, WB, WS, FData, Range : Variant;
begin
 a := 0;
 IBTable1.DisableControls;
 id := IBTable1.FieldByName('id').Value;
 IBTable1.First;
 c := 0;
 ExApp := CreateOleObject('Excel.Application');
 WB := ExApp.WorkBooks.Add;
 WS := ExApp.Workbooks[1].WorkSheets[1];
 for b := 0 to DBGrid1.Columns.Count - 1 do
  if DBGrid1.Columns[b].Visible then
   begin
    WS.Cells[1, c + 1] := DBGrid1.Columns[b].Title.Caption;
    WS.Rows[1].Font.Bold := True;
    inc(c);
   end;
 FData := VarArrayCreate([1, GetRecordsCount(IBTable1), 1, c + 1], varVariant);
 with DBGrid1 do
  while not DataSource.DataSet.Eof do
   begin
    c := 0;
    for b := 0 to (Columns.Count - 1) do
     if Columns[b].Visible then
      begin
       FData[a + 1, c + 1] := Columns[b].Field.AsString;
       Inc(c)
      end;
    DataSource.DataSet.Next;
    Inc(a);
   end;
 IBTable1.Locate('id', id, []);
 IBTable1.EnableControls;
 Range := WS.Range[WS.Cells[2, 1], WS.Cells[VarArrayHighBound(FData, 1) + 1, VarArrayHighBound(FData, 2)]];
 Range.Value := FData;
 Range := WS.Range[WS.Cells[1, 1], WS.Cells[VarArrayHighBound(FData, 1) + 1, VarArrayHighBound(FData, 2)]];
 Range.Columns.AutoFit;
 ExApp.Visible := true;
end;

//-----------------------------------------------
//������ �������
procedure TARM_BlindRepForm.ApplyFilter;
begin
 IBTable1.Filter := '(' + GridFilter + ')' + StDate1 + StDate2 + StFIO + StDiv + StPos;
end;

//-----------------------------------------------
//
procedure TARM_BlindRepForm.BitBtn1Click(Sender: TObject);
begin
 if not DBGrid1.DataSource.DataSet.IsEmpty then
  ToExcel;
end;

//-----------------------------------------------
//�������
procedure TARM_BlindRepForm.BitBtn4Click(Sender: TObject);
begin
 Close;
end;

//-----------------------------------------------
//������� �� ��������� ������ �������
procedure TARM_BlindRepForm.ChangeDate1;
var
 Dt1: TDateTime;
begin
 if CheckBox1.Checked then
  begin
   ReplaceDate(Dt1, MonthCalendar1.Date);
   ReplaceTime(Dt1, DateTimePicker1.Time);
   //DateTimeToString(StDate1, 'yyyy-mm-dd hh:nn:ss', Dt1);
   StDate1 := GetStrFromDT(Dt1);
   StDate1 := ' and (DATE_>=''' + StDate1 + ''')';
  end
 else
  StDate1 := '';
 ApplyFilter;
end;

//-----------------------------------------------
//������� �� ��������� ����� �������
procedure TARM_BlindRepForm.ChangeDate2;
var
 Dt2: TDateTime;
begin
 if CheckBox2.Checked then
  begin
   ReplaceDate(Dt2, MonthCalendar2.Date);
   ReplaceTime(Dt2, DateTimePicker2.Time);
   //DateTimeToString(StDate2, 'yyyy-mm-dd hh:nn:ss', Dt2);
   StDate2 := GetStrFromDT(Dt2);
   StDate2 := ' and (DATE_<=''' + StDate2 + ''')';
  end
 else
  StDate2 := '';
 ApplyFilter;
end;

//-----------------------------------------------
//������� �� ��������� �������������
procedure TARM_BlindRepForm.ChangeDiv;
var
 Value: string;
begin
 if CheckBox4.Checked then
  begin
   Value := Trim(Edit2.Text);
   if Length(Value) > 48 then
    Delete(Value, 48, Length(Value) - 48);
   Value := '%' + AnsiUpperCase(Value) + '%';
   StDiv := Format(' and (upper(DIVISION) like ''%s'')', [Value]);
  end
 else
  StDiv := '';
 ApplyFilter;
end;

//-----------------------------------------------
//������� �� ��������� �.�.�. �������
procedure TARM_BlindRepForm.ChangeFIO;
var
 Value: string;
begin
 if CheckBox3.Checked then
  begin
   Value := Trim(Edit1.Text);
   if Length(Value) > 48 then
    Delete(Value, 48, Length(Value) - 48);
   Value := '%' + AnsiUpperCase(Value) + '%';
   StFIO := Format(' and (upper(F_I_O) like ''%s'')', [Value]);
  end
 else
  StFIO := '';
 ApplyFilter;
end;

//-----------------------------------------------
//������� �� ��������� ���������
procedure TARM_BlindRepForm.ChangePos;
var
 Value: string;
begin
 if CheckBox5.Checked then
  begin
   Value := Trim(Edit3.Text);
   if Length(Value) > 48 then
    Delete(Value, 48, Length(Value) - 48);
   Value := '%' + AnsiUpperCase(Value) + '%';
   StPos := Format(' and (upper(POSITION_) like ''%s'')', [Value]);
  end
 else
  StPos := '';
 ApplyFilter;
end;

//-----------------------------------------------
//������ �������
procedure TARM_BlindRepForm.CheckBox1Click(Sender: TObject);
begin
 MonthCalendar1.Enabled := CheckBox1.Checked;
 DateTimePicker1.Enabled := CheckBox1.Checked;
 ChangeDate1;
end;

//-----------------------------------------------
//
procedure TARM_BlindRepForm.DateTimePicker1Change(Sender: TObject);
begin
 ChangeDate1;
end;

//-----------------------------------------------
//
procedure TARM_BlindRepForm.DateTimePicker2Change(Sender: TObject);
begin
 ChangeDate2;
end;

//-----------------------------------------------
//
procedure TARM_BlindRepForm.MonthCalendar1Click(Sender: TObject);
begin
 ChangeDate1;
end;

//-----------------------------------------------
//
procedure TARM_BlindRepForm.MonthCalendar2Click(Sender: TObject);
begin
 ChangeDate2;
end;

//-----------------------------------------------
//����� �������
procedure TARM_BlindRepForm.CheckBox2Click(Sender: TObject);
begin
 MonthCalendar2.Enabled := CheckBox2.Checked;
 DateTimePicker2.Enabled := CheckBox2.Checked;
 ChangeDate2;
end;

//-----------------------------------------------
//�.�.�. �������
procedure TARM_BlindRepForm.CheckBox3Click(Sender: TObject);
begin
 Edit1.Enabled := CheckBox3.Checked;
 ChangeFIO;
end;

//-----------------------------------------------
//�������������
procedure TARM_BlindRepForm.CheckBox4Click(Sender: TObject);
begin
 Edit2.Enabled := CheckBox4.Checked;
 ChangeDiv;
end;

//-----------------------------------------------
//���������
procedure TARM_BlindRepForm.CheckBox5Click(Sender: TObject);
begin
 Edit3.Enabled := CheckBox5.Checked;
 ChangePos;
end;

//-----------------------------------------------
//
procedure TARM_BlindRepForm.ComboBox1Change(Sender: TObject);
begin
 GridFilter := 'KASSA = ' + IntToStr(ComboBox1.ItemIndex + 1);
 ApplyFilter;
end;

//-----------------------------------------------
//����������
procedure TARM_BlindRepForm.DBGrid1TitleClick(Column: TColumn);
var
 index : integer;
 id : Variant;
begin
 IBTable1.DisableControls;
 id := IBTable1.FieldByName('ID').Value;
 IBTable1.IndexFieldNames := Column.FieldName;
 for index := 0 to DBGrid1.Columns.Count - 1 do
  DBGrid1.Columns[index].Title.Font.Style := DBGrid1.Columns[index].Title.Font.Style - [fsBold];
 Column.Title.Font.Style := [fsBold];
 IBTable1.Locate('ID', id, []);
 IBTable1.EnableControls;
end;

//-----------------------------------------------
//
procedure TARM_BlindRepForm.Edit1Change(Sender: TObject);
begin
 ChangeFIO;
end;

//-----------------------------------------------
//
procedure TARM_BlindRepForm.Edit2Change(Sender: TObject);
begin
 ChangeDiv;
end;

//-----------------------------------------------
//
procedure TARM_BlindRepForm.Edit3Change(Sender: TObject);
begin
 ChangePos;
end;

//-----------------------------------------------
//
procedure TARM_BlindRepForm.FormCreate(Sender: TObject);
begin
 GridFilter := 'KASSA = ' + IntToStr(ComboBox1.ItemIndex + 1);
 IBTable1.Open;
 IBTable1.Filter := GridFilter;
 DateTimePicker1.Time := StartOfTheMonth(Today);
 MonthCalendar1.Date := StartOfTheMonth(Today);
 DateTimePicker2.Time := EndOfTheDay(Today);
 MonthCalendar2.Date := Today;
end;

//-----------------------------------------------
//����� ������� � ������� � ������ ����������
function TARM_BlindRepForm.GetRecordsCount(DS: TIBTable): integer;
var
 s : string;
begin
 Result := 0;
 if DS.IsEmpty then Exit;
 with ARM_Data do
  begin
   IBTransactionShort.StartTransaction;
   IBQueryShort.SQL.Clear;
   if DS.Filtered and (DS.Filter <> '') then
    s := 'select count(*) from ' + DS.TableName + ' where ' + DS.Filter
   else
    s := 'select count(*) from ' + DS.TableName;
   IBQueryShort.SQL.Add(s);
   IBQueryShort.Open;
   Result := IBQueryShort.FieldByName('count').AsInteger;
   IBQueryShort.Close;
   IBTransactionShort.Commit;
  end;
end;

//-----------------------------------------------
//
function TARM_BlindRepForm.GetStrFromDT(DT: TDateTime): string;
begin
 Result := IntToStr(YearOf(DT)) + '-' + Format('%.2u', [MonthOf(DT)]) + '-' + Format('%.2u', [DayOf(DT)]) + ' ' +
           Format('%.2u', [HourOf(DT)]) + ':' + Format('%.2u', [MinuteOf(DT)]) + ':' + Format('%.2u', [SecondOf(DT)]);
end;

end.
