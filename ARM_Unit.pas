unit ARM_Unit;

interface

uses
 WinSock,
 Printers,
 Windows,
 Classes,
 SysUtils;

const
 ARM_Version = '3.1.1';
 ARM_Date = '20.12.2020';
 //��������� ������ ����
 ch_FontSize = 7;
 ch_LineLength = 40;
 ch_LeftField = 28;
 ch_PageWidth = 570;
 ch_LogoSizeX = 250;
 ch_LogoSizeY = 80;

var
 DB_Login: string;              {��� ������������ ���� ������}
 DB_Password: string;           {������ ���� ������}
 //DBSec_Login: string;           {��� ������������ ���� ���������}
 //DBSec_Password: string;        {������ ���� ������ ���������}
 ARM_ExePath: string;           {���� � ���������}
 MainDB: string;                {�������� ���� ���������}
 //SecurityDB: string;            {���� ���������}
 WaitTime: integer;             {�������� �� �����������}
 ComPtName: string;             {COM-port �����������}
 IsEmul: boolean;               {������ ��� �����������}
 //IsPrint: boolean;              {������ ����}
 CardGroup: integer;            {��� ������ �����}
 CardNumber: int64;             {������ ����� �����}
 ch_StolovName: string;         {������������ ��������}
 LastExpNum: string;            {����� ���������� ������}
 PhotoPath: string;             {���� � ����� � ������������}
 LogFontSize: integer;          {������ ������ � ��������� �����}

  //�������� �� ������� � ������ �� ����
 Function IsNumberStr(s: string): boolean;
 //������ ������ ��� ������ ����
 Procedure CheckLine(s_nam, s_sum: string; var s1, s2: string);
 //����������������� � ����������
 Function HexCharToByte(Ch: char): byte;
 //
 Function SplitCheckList(s: string; i: integer): string;

implementation

//-----------------------------------------------
//�������� �� ������� � ������ �� ����
function IsNumberStr(s: string): boolean;
var
 index: integer;
 Ch: char;
begin
 Result := True;
 for index := 1 to Length(s) do
  begin
   Ch := s[index];
   if not CharInSet(Ch, ['0'..'9', ',']) then
    Result := False;
  end;
 if Length(s) = 0 then
  Result := False;
end;

//-----------------------------------------------
//������ ������ ��� ������ ����
Procedure CheckLine(s_nam, s_sum: string; var s1, s2: string);
var
 st_p: string;
 index: integer;
begin
 s1 := '';
 s2 := '';
 if Length(s_nam + s_sum) < ch_LineLength then
  begin
   s1 := s_nam + StringOfChar('.', ch_LineLength - Length(s_nam + s_sum)) + s_sum;
   s2 := '';
   Exit;
  end;
 if Length(s_nam + s_sum) >= ch_LineLength then
  begin
   if Length(s_nam) < ch_LineLength then
    begin
     s1 := s_nam + StringOfChar('.', ch_LineLength - Length(s_nam));
     s2 := StringOfChar('.', ch_LineLength - Length(s_sum)) + s_sum;
     Exit;
    end;
   if Length(s_nam) >= ch_LineLength then
    begin
     index := ch_LineLength;
     while s_nam[index] <> ' ' do dec(index);
     s1 := copy(s_nam, 1, index);
     delete(s_nam, 1, index);
     if Length(s_nam + s_sum) >= ch_LineLength then
      begin
       index := ch_LineLength - Length(s_sum);
       while s_nam[index] <> ' ' do
        dec(index);
       st_p := copy(s_nam, 1, index);
       s2 := st_p + StringOfChar('.', ch_LineLength - Length(st_p + s_sum)) + s_sum;
       Exit;
      end
     else
      begin
       s2 := s_nam+StringOfChar('.', ch_LineLength - Length(s_nam + s_sum)) + s_sum;
       Exit;
      end;
    end;
  end;
end;

//-----------------------------------------------
//����������������� � ����������
function HexCharToByte(Ch: char): byte;
begin
 Result := 0;
 case Ch of
  '0': Result := 0;
  '1': Result := 1;
  '2': Result := 2;
  '3': Result := 3;
  '4': Result := 4;
  '5': Result := 5;
  '6': Result := 6;
  '7': Result := 7;
  '8': Result := 8;
  '9': Result := 9;
  'A': Result := 10;
  'B': Result := 11;
  'C': Result := 12;
  'D': Result := 13;
  'E': Result := 14;
  'F': Result := 15;
 end;
end;

//-----------------------------------------------
//
Function SplitCheckList(s: string; i: integer): string;
var
 TS: TStringList;
begin
 TS := TStringList.Create;
 TS.Delimiter := '^';
 TS.QuoteChar := #0;
 TS.StrictDelimiter := True;
 TS.DelimitedText := s;
 Result := TS.Strings[i];
 TS.Destroy;
end;

end.
