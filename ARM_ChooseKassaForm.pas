unit ARM_ChooseKassaForm;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs, Vcl.StdCtrls;

type
  TARM_ChooseKassaF = class(TForm)
    Label1: TLabel;
    Button1: TButton;
    Button2: TButton;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Res: Integer;
  end;

var
  ARM_ChooseKassaF: TARM_ChooseKassaF;

implementation

{$R *.dfm}

//-----------------------------------------------
//
procedure TARM_ChooseKassaF.Button1Click(Sender: TObject);
begin
 Res := 1;
 Close;
end;

//-----------------------------------------------
//
procedure TARM_ChooseKassaF.Button2Click(Sender: TObject);
begin
 Res := 2;
 Close;
end;

//-----------------------------------------------
//
procedure TARM_ChooseKassaF.FormCreate(Sender: TObject);
begin
 Res := 1;
end;

//-----------------------------------------------
//
procedure TARM_ChooseKassaF.FormKeyPress(Sender: TObject; var Key: Char);
begin
 if Key = '1' then
  Button1Click(Self);
 if Key = '2' then
  Button2Click(Self);
end;

end.
