unit ARM_CardReadF;

interface

uses
  Windows,
  Messages,
  SysUtils,
  Variants,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  StdCtrls,
  ExtCtrls,
  ARM_Unit,
  ARM_DataMod;

type
  C_Data = record
   number_hi: byte;
   number_lo: byte;
  end;

  TARM_CardReadForm = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Timer1: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
    TimeUp: integer;
    ComFile: THandle;
    //CardGroup: integer;
    CardCode: integer;
    function OpenCOMPort: Boolean;
    function SetupCOMPort: Boolean;
    procedure CloseCOMPort;
    procedure ReadCard;
  public
    { Public declarations }
  end;

const
 RxBufferSize = 256;
 TxBufferSize = 256;

var
  ARM_CardReadForm: TARM_CardReadForm;

implementation

{$R *.dfm}

//-----------------------------------------------
//��������� COM-����
function TARM_CardReadForm.OpenCOMPort: Boolean;
var
 DeviceName: array[0..80] of Char;
begin
 StrPCopy(DeviceName, ComPtName + ':');
 ComFile := CreateFile(DeviceName, GENERIC_READ or GENERIC_WRITE, 0, nil, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
 if ComFile = INVALID_HANDLE_VALUE then
  Result := False
 else
  Result := True;
end;

//-----------------------------------------------
//����������� COM-����
function TARM_CardReadForm.SetupCOMPort: Boolean;
var
 DCB: TDCB;
 Config: string;
 CommTimeouts: TCommTimeouts;
begin
 Result := True;
 if not SetupComm(ComFile, RxBufferSize, TxBufferSize) then
  Result := False;
 if not GetCommState(ComFile, DCB) then
  Result := False;
 Config := 'baud=9600 parity=n data=8 stop=1';
 if not BuildCommDCB(@Config[1], DCB) then
  Result := False;
 if not SetCommState(ComFile, DCB) then
  Result := False;
 with CommTimeouts do
  begin
   ReadIntervalTimeout         := 5;
   ReadTotalTimeoutMultiplier  := 0;
   ReadTotalTimeoutConstant    := 10;
   WriteTotalTimeoutMultiplier := 0;
   WriteTotalTimeoutConstant   := 0;
  end;
 if not SetCommTimeouts(ComFile, CommTimeouts) then
  Result := False;
end;

//-----------------------------------------------
//��������� ����� �������
procedure TARM_CardReadForm.ReadCard;
var
 is_0D, is_0A: boolean;
 S_Type, S_Number, S_Series: string;
 St_: string;
 Series: byte;
 BtRead: DWORD;
begin
 TimeUp := WaitTime;
 Timer1.Enabled := True;
 Series := 0;
 St_ := '';
 is_0D := False;
 is_0A := False;
 while (not is_0A) do
  begin
   if TimeUp = 0 then Exit;
   Application.ProcessMessages;
   ReadFile(ComFile, Series, SizeOf(Series), BtRead, nil);
   Sleep(10);
   case Series of
    $0D: is_0D := True;
    $0A: if is_0D then is_0A := True;
   else
    if (Series <> 0) then St_ := St_ + Char(Series);
   end;
  end;
 if is_0A and (not ((St_ = 'No card') and is_0A)) then
  begin
   S_Type := Trim(Copy(St_, 1, pos('[',St_) - 1));
   S_Series := Trim(Copy(St_, pos('[', St_) + 1, pos(']', St_) - pos('[', St_) - 1));
   S_Number := Trim(Copy(St_, pos(']', St_) + 2, length(St_) - pos(']', St_) - 1));
   if S_Type = 'HID' then
    begin
     CardGroup := HexCharToByte(S_Series[7]) * 16 + HexCharToByte(S_Series[8]);
     CardCode := StrToInt(S_Number);
     CardNumber := (CardGroup shl 16) or CardCode;
    end;
   if S_Type = 'Em-Marine' then
    begin
     CardGroup := StrToInt(Copy(S_Number, 1, pos(',', S_Number) - 1));
     Delete(S_Number, 1, 4);
     CardCode := StrToInt(S_Number);
     CardNumber := (CardGroup shl 16) or CardCode;
    end;
  end
 else
  begin
   CardGroup := 0;
   CardCode := 0;
   CardNumber := 0;
  end;
 CloseCOMPort;
end;

//-----------------------------------------------
//��������� COM-����
procedure TARM_CardReadForm.CloseCOMPort;
begin
 CloseHandle(ComFile);
end;

//-----------------------------------------------
//
procedure TARM_CardReadForm.FormActivate(Sender: TObject);
begin
 ReadCard;
end;

//-----------------------------------------------
//����� ��������
procedure TARM_CardReadForm.FormCreate(Sender: TObject);
begin
 CardGroup := 0;
 CardCode := 0;
 CardNumber := 0;
 if OpenCOMPort then
  SetupCOMPort
 else
  ShowMessage('������ �������� COM-�����!');
 Label2.Caption := IntToStr(WaitTime);
end;

//-----------------------------------------------
//��������� �������
procedure TARM_CardReadForm.Timer1Timer(Sender: TObject);
begin
 Dec(TimeUp);
 if (TimeUp < 0) or (CardGroup <> 0) then
  begin
   Timer1.Enabled := False;
   if CardGroup = 0 then CloseCOMPort;
   Close;
  end;
 Label2.Caption := IntToStr(TimeUp);
end;

end.
