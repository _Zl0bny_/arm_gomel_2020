unit ARM_SetupF;

interface

uses
  Windows,
  Messages,
  System.SysUtils,
  Variants,
  System.Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  StdCtrls,
  ExtCtrls,
  Buttons,
  ComCtrls,
  ARM_Unit,
  Data.DB,
  IBX.IBDatabase,
  IniFiles,
  OleServer,
  OutlookXP,
  ActnList,
  StdActns,
  System.Actions,
  IBX.IBScript,
  IBX.IBCustomDataSet,
  IBX.IBTable,
  ARM_DataMod,
  WinApi.ShlObj;

type
  TARM_SetupForm = class(TForm)
    GroupBox1: TGroupBox;
    LabeledEdit1: TLabeledEdit;
    BitBtn1: TBitBtn;
    GroupBox3: TGroupBox;
    ComboBox1: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    TrackBar1: TTrackBar;
    Label3: TLabel;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    CheckBox1: TCheckBox;
    LabeledEdit3: TLabeledEdit;
    LabeledEdit4: TLabeledEdit;
    GroupBox2: TGroupBox;
    LabeledEdit2: TLabeledEdit;
    Button1: TButton;
    UpDown1: TUpDown;
    Edit1: TEdit;
    Label4: TLabel;
    procedure TrackBar1Change(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Res: boolean;

  end;

var
  ARM_SetupForm: TARM_SetupForm;
  Settings: TIniFile;

implementation

{$R *.dfm}

//-----------------------------------------------
// ������� ��������� ������, ���������������
// ��������� ����������� ���� ����� �����
function BrowseCallbackProc(hwnd: HWND; uMsg: UINT; lParam: LPARAM; lpData: LPARAM): Integer; stdcall;
begin
 if (uMsg = BFFM_INITIALIZED) then
  SendMessage(hwnd, BFFM_SETSELECTION, 1, lpData);
 BrowseCallbackProc := 0;
end;

//-----------------------------------------------
// ������ "����� �����"
function GetFolderDialog(Handle: Integer; Caption: string; var strFolder: string): Boolean;
var
 BrowseInfo: TBrowseInfo;
 ItemIDList: PItemIDList;
 JtemIDList: PItemIDList;
 Path: PWideChar;
begin
 Result := False;
 Path := StrAlloc(MAX_PATH);
 SHGetSpecialFolderLocation(Handle, CSIDL_DESKTOP, JtemIDList);
 with BrowseInfo do
  begin
   hwndOwner := GetActiveWindow;
   pidlRoot := JtemIDList;
   SHGetSpecialFolderLocation(hwndOwner, CSIDL_DESKTOP, JtemIDList);
   { return display name of item selected }
   pszDisplayName := StrAlloc(MAX_PATH);
   { ��������� ���� }
   lpszTitle := PChar(Caption);
   { ��������� ����������� ���� }
   ulFlags := BIF_USENEWUI and BIF_RETURNONLYFSDIRS;
   { flags that control the return stuff }
   lpfn := @BrowseCallbackProc;
   { extra info that's passed back in callbacks }
   lParam := LongInt(PChar(strFolder));
  end;
 ItemIDList := SHBrowseForFolder(BrowseInfo);
 if (ItemIDList <> nil) then
  if SHGetPathFromIDList(ItemIDList, Path) then
   begin
    strFolder := Path;
    Result := True
   end;
end;

//-----------------------------------------------
//���� ���������� � �� ���
procedure TARM_SetupForm.BitBtn1Click(Sender: TObject);
begin
 with ARM_Data do
  begin
   IBDB.Connected := False;
   IBDB.Params.Clear;
   IBDB.DatabaseName := LabeledEdit1.Text;
   IBDB.Params.Add('user_name=' + LabeledEdit3.Text);
   IBDB.Params.Add('password=' + LabeledEdit4.Text);
   IBDB.Params.Add('lc_ctype=UTF8');
   try
    IBDB.Connected := True;
    ShowMessage('���������� ��������� �������!');
    IBDB.Connected := False;
   except
    ShowMessage('����������� �� �������!');
   end;
  end;
end;

//-----------------------------------------------
//������ "�������"
procedure TARM_SetupForm.BitBtn4Click(Sender: TObject);
begin
 Close;
end;

//-----------------------------------------------
//������ "Ok"
procedure TARM_SetupForm.BitBtn5Click(Sender: TObject);
begin
 MainDB := LabeledEdit1.Text;
 DB_Login := LabeledEdit3.Text;
 DB_Password := LabeledEdit4.Text;
 WaitTime := TrackBar1.Position;
 ComPtName := ComboBox1.Text;
 IsEmul := CheckBox1.Checked;
 LogFontSize := StrToInt(Edit1.Text);
 if not ARM_Data.CheckDB then
  begin
   ShowMessage('��������� �� ��� �� ������������� ������ ���������!');
   LabeledEdit1.SetFocus;
  end;
 Settings := TIniFile.Create(ChangeFileExt(ParamStr(0), '.ini'));
 Settings.WriteString('UserInfo', 'Login', DB_Login);
 Settings.WriteString('UserInfo', 'Password', DB_Password);
 Settings.WriteString('Location', 'Path', MainDB);
 Settings.WriteInteger('Settings', 'WaitTime', WaitTime);
 Settings.WriteString('Settings', 'ComPt', ComPtName);
 Settings.WriteBool('Settings', 'Emul', IsEmul);
 Settings.WriteString('Settings', 'LastExpNum', LastExpNum);
 Settings.WriteString('Settings', 'PhotoPath', PhotoPath);
 Settings.WriteInteger('Settings', 'LogFontSize', LogFontSize);
 Res := True;
 Close;
end;

//-----------------------------------------------
//����� ����� ����������
procedure TARM_SetupForm.Button1Click(Sender: TObject);
var
 St: string;
begin
 St := PhotoPath;
 if GetFolderDialog(Application.Handle, '����� � ������������ ����������:', St) then
  begin
   LabeledEdit2.Text := St + '\';
   PhotoPath := St + '\';
  end;
end;

//-----------------------------------------------
//������ �����
procedure TARM_SetupForm.FormCreate(Sender: TObject);
begin
 Res := False;
 LabeledEdit1.Text := MainDB;
 LabeledEdit2.Text := PhotoPath;
 LabeledEdit3.Text := DB_Login;
 LabeledEdit4.Text := DB_Password;
 TrackBar1.Position := WaitTime;
 ComboBox1.ItemIndex := ComboBox1.Items.IndexOf(ComPtName);
 CheckBox1.Checked := IsEmul;
 UpDown1.Position := LogFontSize;
end;

//-----------------------------------------------
//������� �� ����������� ��������
procedure TARM_SetupForm.TrackBar1Change(Sender: TObject);
begin
 Label2.Caption := IntToStr(TrackBar1.Position) + ' ���.';
end;

end.
