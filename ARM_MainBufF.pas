unit ARM_MainBufF;

interface

uses
  Windows,
  Messages,
  SysUtils,
  Variants,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  StdCtrls,
  Buttons,
  ExtCtrls,
  Menus,
  Jpeg,
  AppEvnts,
  DB,
  Printers,
  ARM_Unit,
  ARM_CardReadF,
  ARM_SetupF,
  ARM_DataMod,
  ARM_HelpF,
  ARM_AboutF,
  ARM_CardEmulF,
  ARM_RepDailyF,
  ARM_RepClientF,
  ARM_ExportF,
  DateUtils,
  Math,
  ARM_SprClientsF,
  ARM_StatRepF,
  ARM_RepDepartF,
  ARM_ChooseKassaForm;

type
  TARM_MainBufForm = class(TForm)
    MainMenu1: TMainMenu;
    N3: TMenuItem;
    N14: TMenuItem;
    N17: TMenuItem;
    N15: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N20: TMenuItem;
    N21: TMenuItem;
    N22: TMenuItem;
    GroupBox3: TGroupBox;
    Image1: TImage;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label13: TLabel;
    ApplicationEvents1: TApplicationEvents;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Edit1: TEdit;
    Panel2: TPanel;
    Memo1: TMemo;
    BitBtn11: TBitBtn;
    BitBtn13: TBitBtn;
    N1: TMenuItem;
    Label4: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Image2: TImage;
    N7: TMenuItem;
    N10: TMenuItem;
    N2: TMenuItem;
    N6: TMenuItem;
    procedure N1Click(Sender: TObject);  //�������� �������
    procedure N4Click(Sender: TObject);  //��������� �����
    procedure N7Click(Sender: TObject);  //������ �����
    procedure N8Click(Sender: TObject);  //������� �������
    procedure N9Click(Sender: TObject);  //� ���������
    procedure N17Click(Sender: TObject); //����� �� �������
    procedure N14Click(Sender: TObject); //����� �� �����
    procedure N15Click(Sender: TObject); //������� � ��������
    procedure ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
    procedure BitBtn11Click(Sender: TObject);
    procedure BitBtn13Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure N10Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
  private
    { Private declarations }
    // IsCount: boolean;
    Summ: Currency;
    Count: Currency;
    Price: Currency;
    Client_Id: integer;
    CheckList: TStringList;
    F_I_O: string;            //������ ��� �������
    TNumber: string;          //��������� �����
    Position: string;         //���������
    Department: string;       //�����
    Limit_: Currency;         //�������� �����
    SumMnth: Currency;        //����� �� �����
    Mnth: integer;            //����� ������
    ValidThru: TDate;         //���� �������� �����
    LimStr: string;           //��������� ������������� ������
    Accepted: boolean;
    CardAccepted: boolean;    //�������������� �� ����� � �����������
    function GetSum: Currency;
    function GetLastSum: Currency;
    procedure SetSum(const Value: Currency);
    procedure ClearCheck;
    procedure ServReset;
    procedure ServAccept;
    function CheckCard: boolean;
    procedure ServNum(Ch: integer);
    procedure NotAccepted;
    procedure AcceptCard(Kassa: Integer);
    function GetCardCode: boolean; //��������� �����. True ���� ����� ���� ���������
    function LimAndValidDateOk: boolean;
    function GetKassaNum: Integer;
  public
    { Public declarations }
    property Sum_: Currency Read GetSum Write SetSum;
  end;

var
  Prn: TextFile;
  ARM_MainBufForm: TARM_MainBufForm;

implementation

{$R *.dfm}

uses ARM_BlindRepF;

//-----------------------------------------------
//������ �����                                   +
procedure TARM_MainBufForm.ApplicationEvents1Message(var Msg: tagMSG;
  var Handled: Boolean);
begin
 if Edit1.Focused and (Msg.message=wm_Char) then
  begin
   case Msg.wParam of
    42, 44, 46, 48..57: ServNum(Msg.wParam); // * . , 0..9
    8, 27: ServReset;  //Esc, BckSpace
    13: ServAccept;    //Enter
   end;
   Handled := True;
  end;
 if Active and (Msg.message = wm_Char) and (Msg.wParam = 32) and BitBtn11.Enabled then
  begin
   BitBtn11Click(Self);
   Handled := True;
  end;
end;

//-----------------------------------------------
//��������� �������������/�����������            +
procedure TARM_MainBufForm.BitBtn11Click(Sender: TObject);
begin
 if CardAccepted then  //���� ����� �������������� � �� � �������
  AcceptCard(GetKassaNum)
 else                  //��� ����� � ��������� ���� �� �������
  NotAccepted;
end;

//-----------------------------------------------
//������ ������������� �������� ������-���������  +
procedure TARM_MainBufForm.NotAccepted;
begin
 if GetCardCode then  //�� ����������� ���� ���������� � ����� �������
  if CheckCard then   //����� � ������ ��������
   begin
    Label19.Caption := IntToStr(CardNumber);
    Label6.Caption := F_I_O;
    Label2.Caption := TNumber;
    Label15.Caption := Position;
    Label17.Caption := Department;
    Label10.Caption := Format('%f', [SumMnth]) + ' / ' + LimStr;
    if LimAndValidDateOk then   //����� �� �������� � ����� �������������
     begin                                                 //
      CardAccepted := True;                                //
      BitBtn11.Caption := '�����������';                   //
      BitBtn11.Enabled := True;                            //
     end
    else
     CardAccepted := False;
   end; //if CheckCard
 if not CardAccepted then
  Edit1.SetFocus           //���� ��������
 else
  Edit1.Enabled := False;  //���� �������� �� ������������� ��� ������ ����
end;

//-----------------------------------------------
//������������� ��������
procedure TARM_MainBufForm.AcceptCard(Kassa: Integer);
var
 Accept_Id, index: integer;
 StIn: string;
begin
 BitBtn11.Enabled := False;
 with ARM_Data do
  begin
   IBTrans_Main.StartTransaction;
   IBQuery_Main.SQL.Clear;
   IBQuery_Main.SQL.Add('insert into ACCEPTED_EVENTS(CARD_NUM, CLIENT_ID, SUMM, F_I_O, DIVISION, POSITION_, T_NUM, KASSA)');
   IBQuery_Main.SQL.Add('values (:CARD_NUM, :CLIENT_ID, :SUMM, :F_I_O, :DIVISION, :POSITION_, :T_NUM, :KASSA)');
   IBQuery_Main.ParamByName('CARD_NUM').AsInteger := CardNumber;
   IBQuery_Main.ParamByName('CLIENT_ID').AsInteger := Client_Id;
   IBQuery_Main.ParamByName('SUMM').AsCurrency := Sum_;
   IBQuery_Main.ParamByName('F_I_O').AsString := F_I_O;
   IBQuery_Main.ParamByName('DIVISION').AsString := Department;
   IBQuery_Main.ParamByName('POSITION_').AsString := Position;
   IBQuery_Main.ParamByName('T_NUM').AsString := TNumber;
   IBQuery_Main.ParamByName('KASSA').AsInteger := Kassa;
   IBQuery_Main.ExecSQL;
   IBTrans_Main.Commit;
   //���������� ������������� ���������� � �������� �� �����
   IBTrans_Main.StartTransaction;
   IBQuery_Main.SQL.Clear;
   IBQuery_Main.SQL.Add('update s_limits set summ_mnth=:summnth, mnth=:mnth where (staff_id=:client_id)');
   IBQuery_Main.ParamByName('client_id').AsInteger := Client_Id;
   SumMnth := SumMnth + Sum_;
   IBQuery_Main.ParamByName('summnth').AsCurrency := SumMnth;
   IBQuery_Main.ParamByName('mnth').AsInteger := Mnth;
   IBQuery_Main.ExecSQL;
   IBTrans_Main.Commit;
   //���������������� "���"
   Accept_Id := ARM_Data.GetMaxId(Kassa);
   IBTrans_Main.StartTransaction;
   for index := 0 to CheckList.Count - 1 do
    begin
     IBQuery_Main.SQL.Clear;
     IBQuery_Main.SQL.Add('insert into detailed_log values (null, :id, :name, :count_, :price, :summ)');
     IBQuery_Main.ParamByName('id').AsInteger := Accept_Id;
     IBQuery_Main.ParamByName('name').AsString := CheckList[index];
     IBQuery_Main.ParamByName('count_').AsCurrency := 1;
     IBQuery_Main.ParamByName('price').AsCurrency := StrToCurr(Copy(CheckList[index], Pos('=',CheckList[index]) + 1, Length(CheckList[index])));
     IBQuery_Main.ParamByName('summ').AsCurrency := StrToCurr(Copy(CheckList[index], Pos('=',CheckList[index]) + 1, Length(CheckList[index])));
     IBQuery_Main.ExecSQL;
    end;
   IBTrans_Main.Commit;
   DateTimeToString(StIn, 'c', Now);
   Memo1.Lines.Add(' ---------- ');
   Memo1.Lines.Add(Label6.Caption + ': '+Format('%f', [Sum_]) + ' ���.');
   Memo1.Lines.Add(' --- ' + StIn + ' --- ');
   Memo1.Lines.Add('');
   ShowWaitMsg('�������� ������������!', clNavy, clWhite, clBlack);
  end;
 CheckList.Clear;
 ClearCheck;
 Edit1.SetFocus;
end;

//-----------------------------------------------
//�������� ���
procedure TARM_MainBufForm.BitBtn13Click(Sender: TObject);
var
 PosCount: integer;
 index: Integer;
begin
 ARM_Data.ShowWaitMsg('��� �������!', clRed, clWhite, clBlack);
 PosCount := CheckList.Count;
 CheckList.Clear;
 for index := 0 to PosCount - 1 do
  Memo1.Lines.Delete(Memo1.Lines.Count - 1);
 ClearCheck;
 Edit1.SetFocus;
end;

//-----------------------------------------------
//��������� �����
procedure TARM_MainBufForm.FormActivate(Sender: TObject);
begin
 if Edit1.Enabled then
  Edit1.SetFocus;
 Edit1.SelLength := 0;
end;

//-----------------------------------------------
//�������� �����
procedure TARM_MainBufForm.FormCreate(Sender: TObject);
begin
 CheckList := TStringList.Create;
 ClearCheck;
 Caption := '��� ������� ������ v.' + ARM_Version + ' ' + ARM_Date;
 Memo1.Font.Size := LogFontSize;
end;

//-----------------------------------------------
//�������� �� ����������� ���������� � �����
//True ���� ����� ���� ���������
function TARM_MainBufForm.GetCardCode: boolean;
var
 ARM_Card: TForm;
begin
 if IsEmul then         //���� �������� �������� �����������
  begin
   N21.Enabled := False;
   BitBtn11.Enabled := False;
   ARM_Card := TARM_CardEmulForm.Create(Self);
   ARM_Card.ShowModal;
   N21.Enabled := True;
  end
 else
  begin
   N21.Enabled := False;
   BitBtn11.Enabled := False;
   ARM_Card := TARM_CardReadForm.Create(Self);
   ARM_Card.ShowModal;
   N21.Enabled := True;
  end;
 ARM_Card.Destroy;
 Result := CardGroup <> 0;
end;

//-----------------------------------------------
//
function TARM_MainBufForm.GetKassaNum: Integer;
begin
 ARM_ChooseKassaF := TARM_ChooseKassaF.Create(Self);
 ARM_ChooseKassaF.ShowModal;
 Result := ARM_ChooseKassaF.Res;
 ARM_ChooseKassaF.Free;
end;

//-----------------------------------------------
//���������� ����� ��������� �������� �� ����
function TARM_MainBufForm.GetLastSum: Currency;
var
 LastStr: string;
begin
 if CheckList.Count = 0 then
  Result := 0
 else
  begin
   LastStr := CheckList[CheckList.Count - 1];
   Result := StrToCurr(Copy(LastStr, Pos('=', LastStr) + 1, Length(LastStr)));
  end;
end;

//-----------------------------------------------
//
function TARM_MainBufForm.GetSum: Currency;
begin
 Result := Summ;
end;

//-----------------------------------------------
//����� �� �������� � ����� �������
function TARM_MainBufForm.LimAndValidDateOk: boolean;
begin
 Result := True;
 if (SumMnth + Sum_ > Limit_) and (Limit_ <> -1) then   //
  begin                                                 //
   ARM_Data.ShowWaitMsg('�������� ����� ����� �� ������� �����!', clRed, clWhite, clBlack);
   Result := False;
  end;
 if ValidThru <= Today then
  begin
   ARM_Data.ShowWaitMsg('���� �������� �����-�������� ����!', clRed, clWhite, clBlack);
   Result := False;
  end;
end;

//-----------------------------------------------
//���������� ��������
procedure TARM_MainBufForm.N10Click(Sender: TObject);
begin
 ARM_SprClientsForm := TARM_SprClientsForm.Create(Self);
 ARM_SprClientsForm.ShowModal;
 ARM_SprClientsForm.Destroy;
end;

//-----------------------------------------------
//����� �� �����
procedure TARM_MainBufForm.N14Click(Sender: TObject);
begin
 ARM_RepDailyForm := TARM_RepDailyForm.Create(Self);
 ARM_RepDailyForm.ShowModal;
 ARM_RepDailyForm.Destroy;
end;

//-----------------------------------------------
//������� � ��������
procedure TARM_MainBufForm.N15Click(Sender: TObject);
begin
 ARM_ExportForm:=TARM_ExportForm.Create(Self);
 ARM_ExportForm.ShowModal;
 ARM_ExportForm.Destroy;
end;

//-----------------------------------------------
//����� �� �������
procedure TARM_MainBufForm.N17Click(Sender: TObject);
begin
 ARM_RepClientForm := TARM_RepClientForm.Create(Self);
 ARM_RepClientForm.ShowModal;
 ARM_RepClientForm.Destroy;
end;

//-----------------------------------------------
//�������� �������
procedure TARM_MainBufForm.N1Click(Sender: TObject);
begin
 Sum_ := Sum_ - GetLastSum;
 Memo1.Lines.Delete(Memo1.Lines.Count - 1);
 CheckList.Delete(CheckList.Count - 1);
 Edit1.SetFocus;
end;

//-----------------------------------------------
//����� �� ���������� ��������
procedure TARM_MainBufForm.N2Click(Sender: TObject);
begin
 ARM_StatRepForm := TARM_StatRepForm.Create(Self);
 ARM_StatRepForm.ShowModal;
 ARM_StatRepForm.Destroy;
end;

//-----------------------------------------------
//��������� �����
procedure TARM_MainBufForm.N4Click(Sender: TObject);
begin
 ARM_SetupForm := TARM_SetupForm.Create(Self);
 ARM_SetupForm.ShowModal;
 ARM_SetupForm.Destroy;
end;

//-----------------------------------------------
//����� �� �������������
procedure TARM_MainBufForm.N6Click(Sender: TObject);
begin
 ARM_RepDepartForm := TARM_RepDepartForm.Create(Self);
 ARM_RepDepartForm.ShowModal;
 ARM_RepDepartForm.Destroy;
end;

//-----------------------------------------------
//������ �����
procedure TARM_MainBufForm.N7Click(Sender: TObject);
begin
 ARM_BlindRepForm := TARM_BlindRepForm.Create(Self);
 ARM_BlindRepForm.ShowModal;
 ARM_BlindRepForm.Destroy;
end;

//-----------------------------------------------
//������� �������
procedure TARM_MainBufForm.N8Click(Sender: TObject);
begin
 ARM_HelpForm := TARM_HelpForm.Create(Self);
 ARM_HelpForm.ShowModal;
 ARM_HelpForm.Destroy;
end;

//-----------------------------------------------
//� ���������
procedure TARM_MainBufForm.N9Click(Sender: TObject);
begin
 ARM_AboutForm := TARM_AboutForm.Create(Self);
 ARM_AboutForm.ShowModal;
 ARM_AboutForm.Destroy;
end;

//-----------------------------------------------
//������ �������� ���� "�����"
procedure TARM_MainBufForm.SetSum(const Value: Currency);
begin
 if Value > 0 then
  begin
   Summ := Value;
   BitBtn11.Enabled := True;
   N21.Enabled := True;
   N22.Enabled := True;
   N1.Enabled := True;
   BitBtn13.Enabled := True;
  end
 else
  begin
   Summ := 0;
   BitBtn11.Enabled := False;
   N21.Enabled := False;
   N22.Enabled := False;
   N1.Enabled := False;
   BitBtn13.Enabled := False;
  end;
 Label8.Caption := Format('%f', [Summ]);
end;

//-----------------------------------------------
//�������� ���
procedure TARM_MainBufForm.ClearCheck;
begin
 BitBtn11.Caption := '��������� �������������';
 Edit1.Enabled := True;
 Sum_ := 0;
 CardAccepted := False;
 Client_Id := -1;
 Label6.Caption := '';
 Label2.Caption := '';
 Label15.Caption := '';
 Label17.Caption := '';
 Label19.Caption := '';
 Label10.Caption := '';
 Image1.Visible := False;
 Label13.Visible := False;
 BitBtn13.Enabled := False;
 BitBtn11.Enabled := False;
 ServReset;
end;

//-----------------------------------------------
//����� ������ ����� � ��������� ���������
procedure TARM_MainBufForm.ServReset;
begin
 // IsCount:=true;
 Accepted := False;
 Edit1.Text := '0';
end;

//-----------------------------------------------
//������������� ����� �������
procedure TARM_MainBufForm.ServAccept;
var
 StCount, StPrice: string;
 SumPos: Currency;
begin
 if (Edit1.Text <> '') and (Pos('*', Edit1.Text) <> Length(Edit1.Text)) then
  begin
   if (Pos('*', Edit1.Text) <> 0) then
    begin
     StCount := Copy(Edit1.Text, 1, Pos('*', Edit1.Text) - 1);
     StPrice := Copy(Edit1.Text, Pos('*', Edit1.Text) + 1, Length(Edit1.Text));
    end
   else
    begin
     StCount := '1';
     StPrice := Edit1.Text;
    end;
   Edit1.Text := '0';
   Count := StrToCurr(StCount);
   Price := StrToCurr(StPrice);
   SumPos := SimpleRoundTo(Count * Price, -2);
   Memo1.Lines.Add(CurrToStr(Count) + ' x ' + Format('%f', [Price]) + ' = ' + Format('%f', [SumPos]));
   CheckList.Add(CurrToStr(Count) + 'x' + CurrToStr(Price) + '=' + CurrToStr(SumPos));
   Sum_ := Sum_ + SumPos;
  end;
end;

//-----------------------------------------------
//�������� ����� �� ������� � ����
function TARM_MainBufForm.CheckCard: boolean;
var
 StreamValue:TMemoryStream;
 Jpg: TJPEGImage;
begin
 Result := False;
 with ARM_Data do
  begin
   IBTrans_Main.StartTransaction;
   IBQuery_Main.SQL.Clear;
   IBQuery_Main.SQL.Add('select * from clients_view where card_code=:card_code');
   IBQuery_Main.ParamByName('card_code').AsInteger := CardNumber;
   IBQuery_Main.Open;
   if IBQuery_Main.Eof then
    ShowMessage('����� �� ������� � ����! ������ �� �������!')
   else
    begin
     Client_Id := IBQuery_Main.FieldByName('id').AsInteger;
     F_I_O := IBQuery_Main.FieldByName('last_name').AsString + ' ' +
            IBQuery_Main.FieldByName('first_name').AsString + ' ' +
            IBQuery_Main.FieldByName('middle_name').AsString;
     TNumber := Trim(IBQuery_Main.FieldByName('tnum').AsString);
     Position := Trim(IBQuery_Main.FieldByName('pos').AsString);
     Department := Trim(IBQuery_Main.FieldByName('department').AsString);
     if IBQuery_Main.FieldByName('lim').IsNull then
      begin
       LimStr := '���';
       Limit_ := -1;
      end
     else
      begin
       LimStr := Format('%f', [IBQuery_Main.FieldByName('lim').AsCurrency]);
       Limit_ := IBQuery_Main.FieldByName('lim').AsCurrency;
      end;
     if IBQuery_Main.FieldByName('VALIDTHRU').IsNull then
      ValidThru := IncYear(Today, 1)
     else
      ValidThru := IBQuery_Main.FieldByName('VALIDTHRU').AsDateTime;
     SumMnth := IBQuery_Main.FieldByName('summ_mnth').AsCurrency;
     Mnth := IBQuery_Main.FieldByName('mnth').AsInteger;
     if Mnth <> MonthOf(Today) then
      begin
       SumMnth := 0;
       Mnth := MonthOf(Today);
      end;
     if IBQuery_Main.FieldByName('PHOTO').IsNull then
      begin
       Image1.Visible := False;
       Label13.Visible := True;
      end
     else
      begin
       StreamValue := TMemoryStream.Create;
       try
        TBlobField(IBQuery_Main.FieldByName('PHOTO')).SaveToStream(StreamValue);
        StreamValue.Position := 0;
        Jpg := TJPEGImage.Create;
        try
         Jpg.LoadFromStream(StreamValue);
         Image1.Picture.Assign(Jpg)
        finally
         Jpg.Free;
        end;
       finally
        StreamValue.Free;
       end;
       Image1.Visible := True;
       Label13.Visible := False;
      end;
     Result := True;
    end;
   IBQuery_Main.Close;
   IBTrans_Main.Commit;
  end;
end;

//-----------------------------------------------
//��������� ����� �����
procedure TARM_MainBufForm.ServNum(Ch: integer);
begin
 case Ch of
  42: if (Pos('*', Edit1.Text) = 0) and (Edit1.Text <> '') then
       begin
        Count := StrToFloat(Edit1.Text);
        Edit1.Text := Edit1.Text + '*';
       end;
  44, 46: if Pos(',', Edit1.Text) = 0 then
           Edit1.Text := Edit1.Text + ','
          else
           if (Pos(',', Edit1.Text) < Pos('*', Edit1.Text)) and (Pos('*', Edit1.Text) <> 0) then
            Edit1.Text := Edit1.Text + ',';
  48..57: if Edit1.Text = '0' then
           Edit1.Text := chr(Ch)
          else
           Edit1.Text := Edit1.Text + chr(Ch);
 end;
end;

end.
