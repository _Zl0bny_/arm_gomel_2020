unit ARM_StatRepF;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Data.DB,
  IBX.IBCustomDataSet,
  System.Win.ComObj,
  IBX.IBQuery,
  IBX.IBDatabase,
  Vcl.StdCtrls,
  Vcl.ComCtrls,
  ARM_DataMod,
  System.DateUtils,
  ARM_Unit,
  Vcl.OleServer,
  Excel2000;

type
  TARM_StatRepForm = class(TForm)
    DateTimePicker1: TDateTimePicker;
    DateTimePicker2: TDateTimePicker;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Button1: TButton;
    IBTransaction1: TIBTransaction;
    IBQuery1: TIBQuery;
    SaveDialog1: TSaveDialog;
    procedure FormCreate(Sender: TObject);
    procedure DateTimePicker1Change(Sender: TObject);
    procedure DateTimePicker2Change(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    Date3, Date4: TDateTime;
    procedure SecondPeriodRefresh;
  public
    { Public declarations }
  end;

const
  LCID = LOCALE_USER_DEFAULT;

var
  ARM_StatRepForm: TARM_StatRepForm;

implementation

{$R *.dfm}

//-----------------------------------------------
//
procedure TARM_StatRepForm.Button1Click(Sender: TObject);
var
 a, RecCount : Integer;
 ExApp : TExcelApplication;
 WB, WS : Variant;
begin
 if SaveDialog1.Execute then
  begin
   IBTransaction1.StartTransaction;
   IBQuery1.SQL.Clear;
   IBQuery1.SQL.Add('select count(distinct DEPARTMENT) from clients_view');
   IBQuery1.Open;
   RecCount := IBQuery1.FieldByName('COUNT').AsInteger;
   IBQuery1.Close;
   IBQuery1.SQL.Clear;
   try
    IBQuery1.SQL.Add('select * from STAT_REPORT(:DATE1, :DATE2, :DATE3, :DATE4)');
    IBQuery1.ParamByName('DATE1').AsDateTime := DateTimePicker1.DateTime;
    IBQuery1.ParamByName('DATE2').AsDateTime := DateTimePicker2.DateTime;
    IBQuery1.ParamByName('DATE3').AsDateTime := Date3;
    IBQuery1.ParamByName('DATE4').AsDateTime := Date4;
    IBQuery1.Open;
    a := 0; //�������� ������
    ExApp := TExcelApplication.Create(nil); //��������� ����� ��������� Excel'�
    ExApp.ConnectKind := ckNewInstance; //������ ��������� ����� ��������� Excel
    ExApp.Connect;         //�����������
    ExApp.AutoQuit := False; //�� ��������� ��� �������� True ������ � unit ExcelXP
    ExApp.Visible[LCID] := False; //Excel �� �����
    if FileExists(ARM_ExePath + 'Template\tpl_statrep.xls') then
     WB := ExApp.WorkBooks.Add(ARM_ExePath + 'Template\tpl_statrep.xls', LCID)
    else
     WB := ExApp.WorkBooks.Add(EmptyParam, LCID);
    WS := ExApp.Workbooks[1].WorkSheets[1];
    WS.Range['D2',EmptyParam].Value2 := DateToStr(Date3) + ' - ' + DateToStr(Date4);
    WS.Range['H2',EmptyParam].Value2 := DateToStr(DateTimePicker1.DateTime) + ' - ' + DateToStr(DateTimePicker2.DateTime);
    //FData := VarArrayCreate([1, RecCount, 1, c], varVariant);
    with IBQuery1 do
     while not Eof do
      begin
       WS.Range['A' + IntToStr(a + 5), EmptyParam].Value2 := a + 1;
       WS.Range['B' + IntToStr(a + 5), EmptyParam].Value2 := Fields[0].AsString;
       WS.Range['C' + IntToStr(a + 5), EmptyParam].Value2 := Fields[1].AsInteger;
       WS.Range['D' + IntToStr(a + 5), EmptyParam].Value2 := Fields[6].AsInteger;
       WS.Range['E' + IntToStr(a + 5), EmptyParam].Formula := '=D' + IntToStr(a + 5) + '/C' + IntToStr(a + 5);
       WS.Range['F' + IntToStr(a + 5), EmptyParam].Value2 := Fields[8].AsFloat;
       WS.Range['G' + IntToStr(a + 5), EmptyParam].Formula := '=F' + IntToStr(a + 5) + '/C' + IntToStr(a + 5);
       WS.Range['H' + IntToStr(a + 5), EmptyParam].Value2 := Fields[2].AsInteger;
       WS.Range['I' + IntToStr(a + 5), EmptyParam].Formula := '=H' + IntToStr(a + 5) + '/C' + IntToStr(a + 5);
       WS.Range['J' + IntToStr(a + 5), EmptyParam].Value2 := Fields[4].AsFloat;
       WS.Range['K' + IntToStr(a + 5), EmptyParam].Formula := '=J' + IntToStr(a + 5) + '/C' + IntToStr(a + 5);
       WS.Range['L' + IntToStr(a + 5), EmptyParam].Formula := '=K' + IntToStr(a + 5) + '/G' + IntToStr(a + 5);
       WS.Range['A' + IntToStr(a + 5), 'L' + IntToStr(a + 5)].Borders.LineStyle := 1;
       Next;
       Inc(a);
      end;
    WS.Range['B' + IntToStr(RecCount + 5), EmptyParam].Value2 := '�����';
    WS.Range['C' + IntToStr(RecCount + 5), EmptyParam].Formula := '=sum(C5:C' + IntToStr(RecCount + 4) + ')';
    WS.Range['D' + IntToStr(RecCount + 5), EmptyParam].Formula := '=sum(D5:D' + IntToStr(RecCount + 4) + ')';
    WS.Range['E' + IntToStr(RecCount + 5), EmptyParam].Formula := '=D' + IntToStr(RecCount + 5) + '/C' + IntToStr(RecCount + 5);
    WS.Range['F' + IntToStr(RecCount + 5), EmptyParam].Formula := '=sum(F5:F' + IntToStr(RecCount + 4) + ')';
    WS.Range['G' + IntToStr(RecCount + 5), EmptyParam].Formula := '=F' + IntToStr(RecCount + 5) + '/C' + IntToStr(RecCount + 5);
    WS.Range['H' + IntToStr(RecCount + 5), EmptyParam].Formula := '=sum(H5:H' + IntToStr(RecCount + 4) + ')';
    WS.Range['I' + IntToStr(RecCount + 5), EmptyParam].Formula := '=H' + IntToStr(RecCount + 5) + '/C' + IntToStr(RecCount + 5);
    WS.Range['J' + IntToStr(RecCount + 5), EmptyParam].Formula := '=sum(J5:J' + IntToStr(RecCount + 4) + ')';
    WS.Range['K' + IntToStr(RecCount + 5), EmptyParam].Formula := '=J' + IntToStr(RecCount + 5) + '/C' + IntToStr(RecCount + 5);
    WS.Range['L' + IntToStr(RecCount + 5), EmptyParam].Formula := '=K' + IntToStr(RecCount + 5) + '/G' + IntToStr(RecCount + 5);
    WS.Range['B' + IntToStr(RecCount + 5), 'L' + IntToStr(RecCount + 5)].Font.Bold := True;
    WS.Range['B' + IntToStr(RecCount + 5), 'L' + IntToStr(RecCount + 5)].Borders.LineStyle := 1;
    ExApp.ActiveWorkbook.SaveAs(SaveDialog1.FileName, //��������� �������� �����
        xlNormal,    //FileFormat
        EmptyParam,  //Password
        EmptyParam,  //WriteResPassword
        False,       //ReadOnlyRecommended
        False,       //CreateBackup
        xlNoChange,  //AccessMode
        xlLocalSessionChanges, //ConflictResolution
        False,       //AddToMru
        EmptyParam,  //TextCodePage
        EmptyParam,  //TextVisualLayout
        LCID);       //Local
   finally
    ExApp.UserControl := True; //������� ���������� ������������
    ExApp.Visible[LCID] := True; //�������� Excel
    ExApp.Disconnect; //�������������
    IBQuery1.Close;
    IBTransaction1.Commit;
    Close;
   end;
  end;
end;

//-----------------------------------------------
//
procedure TARM_StatRepForm.DateTimePicker1Change(Sender: TObject);
begin
 DateTimePicker2.MinDate := IncDay(DateTimePicker1.DateTime, 1);
 SecondPeriodRefresh;
end;

//-----------------------------------------------
//
procedure TARM_StatRepForm.DateTimePicker2Change(Sender: TObject);
begin
 DateTimePicker1.MaxDate := IncDay(DateTimePicker2.DateTime, -1);
 SecondPeriodRefresh;
end;

//-----------------------------------------------
//
procedure TARM_StatRepForm.FormCreate(Sender: TObject);
begin
 DateTimePicker1.DateTime := StartOfTheWeek(Today);
 DateTimePicker2.DateTime := EndOfTheWeek(Today);
 DateTimePicker1.MaxDate := IncDay(DateTimePicker2.DateTime, -1);
 DateTimePicker2.MinDate := IncDay(DateTimePicker1.DateTime, 1);
 SecondPeriodRefresh;
end;

//-----------------------------------------------
//
procedure TARM_StatRepForm.SecondPeriodRefresh;
begin
 Date3 := IncDay(DateTimePicker1.DateTime, - (1 + DaysBetween(DateTimePicker1.DateTime, DateTimePicker2.DateTime)));
 Date4 := IncDay(DateTimePicker2.DateTime, - (1 + DaysBetween(DateTimePicker1.DateTime, DateTimePicker2.DateTime)));
 Label5.Caption := '������ �������: ' + DateToStr(Date3);
 Label6.Caption := '����� �������: ' + DateToStr(Date4);
end;

end.
