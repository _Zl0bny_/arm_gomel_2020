unit ARM_LimClientF;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, Grids, DBGrids, Buttons, ExtCtrls, frxClass,
  IBX.IBDatabase, DB, IBX.IBCustomDataSet, IBX.IBQuery, frxDBSet, ARM_Unit, ARM_DataMod,
  DateUtils, AppEvnts, System.UITypes;

type
  TLimRec = record
    EmptyRec: boolean;
    EmptyLim: boolean;
    ChangeRec: boolean;
    Staff_id: integer;
    T_num: string;
    Mnth: integer;
    Lim: Currency;
    SumMnth: Currency;
   end;

  TARM_LimClientForm = class(TForm)
    Panel1: TPanel;
    BitBtn5: TBitBtn;
    Panel2: TPanel;
    GroupBox1: TGroupBox;
    Edit1: TEdit;
    GroupBox2: TGroupBox;
    DBGrid1: TDBGrid;
    GroupBox3: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    IBQuery1: TIBQuery;
    DataSource1: TDataSource;
    IBTransaction1: TIBTransaction;
    IBQuery2: TIBQuery;
    IBTransaction2: TIBTransaction;
    Edit2: TEdit;
    CheckBox1: TCheckBox;
    Label3: TLabel;
    Edit3: TEdit;
    Label4: TLabel;
    IBQuery3: TIBQuery;
    IBTransaction3: TIBTransaction;
    ApplicationEvents1: TApplicationEvents;
    procedure Panel2Resize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure DBGrid1TitleClick(Column: TColumn);
    procedure IBQuery1AfterScroll(DataSet: TDataSet);
    procedure BitBtn5Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
    procedure Edit2Change(Sender: TObject);
  private
    { Private declarations }
    StSQL, StQuery, StSearch, StOrder: string;
    //---------------------
    LimRec: TLimRec;
    procedure RefreshGrid(SeekRec: boolean);
    procedure InsertLimRec;
    procedure UpdateLimRec;
    procedure DisplayLimRec;
    function IsChangedLimRec: boolean;
    procedure FillLimRec;
  public
    { Public declarations }
  end;

var
  ARM_LimClientForm: TARM_LimClientForm;

implementation

{$R *.dfm}

//-----------------------------------------------
//
procedure TARM_LimClientForm.ApplicationEvents1Message(var Msg: tagMSG;
  var Handled: Boolean);
begin
 if (Edit2.Focused and (Msg.message = WM_CHAR)) then
  begin
   case Msg.wParam of
    44, 46: if (Pos(',', Edit2.Text) = 0) then
           Edit2.Text := Edit2.Text + ',';
    48..57: if Edit2.Text = '0' then
           Edit2.Text := chr(Msg.wParam)
          else
           Edit2.Text := Edit2.Text + chr(Msg.wParam);
   end;
   Handled := True;
  end;
end;

//-----------------------------------------------
//
procedure TARM_LimClientForm.BitBtn5Click(Sender: TObject);
begin
 if IsChangedLimRec then //���� ������ � ������� ����������, ������� ��������� � ����
  begin
   FillLimRec;
   UpdateLimRec;
  end;
 IBQuery1.Close;
 Close;
end;

//-----------------------------------------------
//
procedure TARM_LimClientForm.CheckBox1Click(Sender: TObject);
begin
 Edit2.Enabled := not CheckBox1.Checked;
 LimRec.ChangeRec := True;
end;

//-----------------------------------------------
//
procedure TARM_LimClientForm.DBGrid1TitleClick(Column: TColumn);
var
 index: Integer;
begin
 for index := 0 to 6 do
  DBGrid1.Columns[index].Title.Font.Style := DBGrid1.Columns[index].Title.Font.Style - [fsBold];
 case Column.Index of
  0: StOrder := ' order by upper(last_name)';
  1: StOrder := ' order by upper(first_name)';
  2: StOrder := ' order by upper(middle_name)';
  3: StOrder := ' order by tnum';
  4: StOrder := ' order by card_code';
  5: StOrder := ' order by department';
  6: StOrder := ' order by pos';
 end;
 Column.Title.Font.Style := [fsBold];
 RefreshGrid(True);
end;

//-----------------------------------------------
//
procedure TARM_LimClientForm.Edit1Change(Sender: TObject);
var
 St, St2: string;
begin
 if Trim(Edit1.Text) = '' then
  begin
   if StSearch <> '' then
    begin
     StSearch := '';
     RefreshGrid(False);
    end;
   Exit;
  end;
 St := '%' + AnsiUpperCase(Trim(Edit1.Text)) + '%';
 if not IsNumberStr(Trim(Edit1.Text)) then
  StSearch := Format(' where (upper(last_name) like ''%s'')', [St])
 else
  begin
   if Length(Trim(Edit1.Text)) > 6 then
    begin
     St2 := Trim(Edit1.Text);
     Delete(St2, 7, Length(St2) - 6);
     St := AnsiUpperCase(St2);
    end;
   if Length(Trim(Edit1.Text)) = 6 then
    St := AnsiUpperCase(Trim(Edit1.Text));
   if Length(Trim(Edit1.Text)) = 5 then
    St := AnsiUpperCase(Trim(Edit1.Text)) + '%';
   StSearch := Format(' where (tnum like ''%s'')', [St]);
  end;
 RefreshGrid(False);
end;

//-----------------------------------------------
//
procedure TARM_LimClientForm.Edit2Change(Sender: TObject);
begin
 LimRec.ChangeRec := True;
end;

//-----------------------------------------------
//
procedure TARM_LimClientForm.FillLimRec;
begin
 LimRec.EmptyLim := CheckBox1.Checked;
 LimRec.Lim := StrToCurr(Edit2.Text);
end;

//-----------------------------------------------
//
procedure TARM_LimClientForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 IBQuery1.Close;
end;

//-----------------------------------------------
//
procedure TARM_LimClientForm.FormCreate(Sender: TObject);
begin
 IBQuery1.Active := True;
end;

//-----------------------------------------------
//
procedure TARM_LimClientForm.FormShow(Sender: TObject);
begin
 StOrder := ' order by upper(last_name)';
 StSearch := ' ';
 StQuery := 'select * from clients';
 RefreshGrid(False);
end;

//-----------------------------------------------
//
procedure TARM_LimClientForm.IBQuery1AfterScroll(DataSet: TDataSet);
begin
 if IsChangedLimRec then //���� ������ � ������� ����������, ������� ��������� � ����
  begin
   FillLimRec;
   UpdateLimRec;
  end;
 IBTransaction2.StartTransaction;
 IBQuery2.SQL.Clear;
 IBQuery2.SQL.Add('select t_num, summ_mnth, mnth, lim from s_limits where staff_id=:id');
 IBQuery2.ParamByName('id').AsInteger := IBQuery1.FieldByName('id').AsInteger;
 IBQuery2.Open;
 if IBQuery2.IsEmpty then
  begin
   LimRec.EmptyRec := True;
   LimRec.EmptyLim := True;
   LimRec.Staff_id := IBQuery1.FieldByName('id').AsInteger;
   LimRec.T_num := IBQuery2.FieldByName('t_num').AsString;
   LimRec.Mnth := MonthOf(Now);
   LimRec.SumMnth := 0;
   LimRec.ChangeRec := False;
   InsertLimRec;      //���� ��� ������� �� ���� ������ � ������� - ������ ������
  end
 else
  begin
   LimRec.EmptyRec := False;
   if IBQuery2.FieldByName('lim').IsNull then
    LimRec.EmptyLim := True
   else
    begin
     LimRec.EmptyLim := False;
     LimRec.Lim := IBQuery2.FieldByName('lim').AsCurrency;
    end;
   LimRec.Staff_id := IBQuery1.FieldByName('id').AsInteger;
   LimRec.T_num := IBQuery2.FieldByName('t_num').AsString;
   LimRec.Mnth := IBQuery2.FieldByName('mnth').AsInteger;
   LimRec.SumMnth := IBQuery2.FieldByName('summ_mnth').AsCurrency;
   LimRec.ChangeRec := False;
  end;
 DisplayLimRec;       //���������� ���� �� �������� �������
 IBTransaction2.Commit;
end;

//-----------------------------------------------
//
procedure TARM_LimClientForm.DisplayLimRec;
begin
 CheckBox1.Checked := LimRec.EmptyLim;
 if LimRec.EmptyLim then
  Edit2.Text := '0'
 else
  Edit2.Text := CurrToStr(LimRec.Lim);
 Label4.Caption := IntToStr(LimRec.Mnth);
 Edit3.Text := CurrToStr(LimRec.SumMnth);
end;

//-----------------------------------------------
//
procedure TARM_LimClientForm.InsertLimRec;
begin
 IBTransaction3.StartTransaction;
 IBQuery3.SQL.Clear;
 IBQuery3.SQL.Add('insert into s_limits values (:st_id, :t_num, :summ_mnth, :mnth, :lim)');
 IBQuery3.ParamByName('st_id').AsInteger := LimRec.Staff_id;
 IBQuery3.ParamByName('t_num').AsString := LimRec.T_num;
 IBQuery3.ParamByName('summ_mnth').AsCurrency := LimRec.SumMnth;
 IBQuery3.ParamByName('mnth').AsInteger := LimRec.Mnth;
 if LimRec.EmptyLim then
  IBQuery3.ParamByName('lim').Clear
 else
  IBQuery3.ParamByName('lim').AsCurrency := LimRec.Lim;
 IBQuery3.ExecSQL;
 IBTransaction3.Commit;
end;

//-----------------------------------------------
//
function TARM_LimClientForm.IsChangedLimRec: boolean;
begin
 Result := LimRec.EmptyLim <> CheckBox1.Checked;
 if not Result then
  Result := LimRec.Lim <> StrToCurr(Edit2.Text);
end;

//-----------------------------------------------
//
procedure TARM_LimClientForm.UpdateLimRec;
begin
 IBTransaction3.StartTransaction;
 IBQuery3.SQL.Clear;
 IBQuery3.SQL.Add('update s_limits set lim=:lim where staff_id=:st_id');
 IBQuery3.ParamByName('st_id').AsInteger := LimRec.Staff_id;
 if LimRec.EmptyLim then
  IBQuery3.ParamByName('lim').Clear
 else
  IBQuery3.ParamByName('lim').AsCurrency := LimRec.Lim;
 IBQuery3.ExecSQL;
 IBTransaction3.Commit;
end;

//-----------------------------------------------
//
procedure TARM_LimClientForm.Panel2Resize(Sender: TObject);
begin
 Edit1.Width := GroupBox1.Width - 9;
end;

//-----------------------------------------------
//
procedure TARM_LimClientForm.RefreshGrid(SeekRec: boolean);
var
 index: integer;
begin
 index := -1;
 StSQL := StQuery + StSearch + StOrder;
 if SeekRec and (IBQuery1.FieldByName('id').IsNull) then SeekRec := False;
 if SeekRec then index := IBQuery1.FieldByName('id').AsInteger;
 DataSource1.Enabled := False;
 IBQuery1.Close;
 IBTransaction1.Commit;
 IBTransaction1.StartTransaction;
 IBQuery1.SQL.Clear;
 IBQuery1.SQL.Add(StSQL);
 IBQuery1.Open;
 if SeekRec then
  while (IBQuery1.FieldByName('id').AsInteger<>index) and (not IBQuery1.Eof) do
   IBQuery1.Next;
 DataSource1.Enabled := True;
end;

end.
