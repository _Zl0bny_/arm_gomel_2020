unit ARM_DataMod;

interface

uses
  System.SysUtils,
  System.Classes,
  IBX.IBDatabase,
  Data.DB,
  IBX.IBCustomDataSet,
  IBX.IBQuery,
  IniFiles,
  ARM_Unit,
  Dialogs,
  Forms,
  IBX.IBScript,
  System.DateUtils,
  Graphics,
  StdCtrls,
  Controls,
  ExtCtrls,
  IBX.IBTable,
  Vcl.Imaging.JPEG;

type
  TARM_Data = class(TDataModule)
    IBDB_Main: TIBDatabase;
    IBTrans_Main: TIBTransaction;
    IBQuery_Main: TIBQuery;
    IBTransactionShort: TIBTransaction;
    IBQueryShort: TIBQuery;
    IBDB: TIBDatabase;
    IBTrans: TIBTransaction;
    IBTable1: TIBTable;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    function CallSetup: boolean;
    procedure ReadBDConfig;     //�������� �� INI-����� ���� ��� ������������ ��
  public
    { Public declarations }
    procedure ShowWaitMsg(Msg: string; TxColor, BgColor, BvColor: TColor);
    function GetMaxId(KassNum: integer): integer;
    function CheckDB: boolean;   //�������� ��������� �� ���
    function EndOfDemo: boolean; //�������� ��������� ����������������� �������
    procedure ImportPhoto(Cl_Id: integer; FIO: string);
  end;

  function ResizeJpegPhoto(FileName: string): string;

var
  ARM_Data: TARM_Data;

implementation

uses ARM_MainBufF, ARM_SetupF;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}


//-----------------------------------------------
//
function ResizeJpegPhoto(FileName: string): string;
var
 Bmp: TBitmap;
 Jpg: TJpegImage;
 Scale: Double;
 new_Width, new_Height, x_Pos: integer;
const
 c_Width = 300;
 c_Height = 400;
begin
 Jpg := TJpegImage.Create;
 try
  Jpg.LoadFromFile(FileName);
  Scale := Jpg.Height / c_Height;
  new_Width := Round(Jpg.Width / Scale);
  new_Height := c_Height;
  Bmp := TBitmap.Create;
  try
   Bmp.Width := c_Width;
   Bmp.Height:= c_Height;
   Bmp.Canvas.Brush.Color := clWhite;
   Bmp.Canvas.FillRect(Bmp.Canvas.ClipRect);
   x_Pos := ((c_Width - new_Width) div 2) + 1;
   Bmp.Canvas.StretchDraw(Rect(x_Pos, 0, x_Pos + new_Width, new_Height), Jpg);
   Jpg.Assign(Bmp);
   Jpg.CompressionQuality := 95;
   Jpg.SaveToFile(Format('%sTmp\' + ExtractFileName(FileName), [ARM_ExePath]));
   Result := Format('%sTmp\' + ExtractFileName(FileName), [ARM_ExePath]);
  finally
   Bmp.Free;
  end;
 finally
  Jpg.Free;
 end;
end;

//-----------------------------------------------
//���������� ���� � ����������
procedure TARM_Data.ShowWaitMsg(Msg: string; TxColor, BgColor, BvColor: TColor);
var
 frm: TForm;
 lb: TLabel;
 sh: TShape;
begin
 frm := TForm.Create(Application);
 with frm do
  begin
   BorderIcons := [];
   BorderStyle := bsNone;
   Position := poScreenCenter;
   sh := TShape.Create(frm);
   sh.Parent := frm;
   sh.Align := alClient;
   sh.Brush.Color := BgColor;
   sh.Pen.Color := BvColor;
   sh.Pen.Style := psSolid;
   sh.Pen.Width := 1;
   lb := TLabel.Create(frm);
   lb.Parent := frm;
   lb.Left := 10;
   lb.Top := 5;
   lb.Caption := msg;
   lb.Font.SIZE := 14;
   lb.Font.Style := [fsBold];
   lb.Alignment := taCenter;
   lb.AutoSize := True;
   lb.Transparent := True;
   lb.WordWrap := True;
   ClientWidth := lb.Canvas.TextWidth(msg) + 20;
   ClientHeight := lb.Canvas.TextHeight(msg) + 10;
   lb.Font.Color := TxColor;
   Show;
  end;
 Application.ProcessMessages;
 Sleep(1200);
 frm.Destroy;
end;

//-----------------------------------------------
//���������� ������������ ID � ������� +
function TARM_Data.GetMaxId(KassNum: integer): integer;
begin
 Result := -1;
 IBTransactionShort.StartTransaction;
 IBQueryShort.SQL.Clear;
 IBQueryShort.SQL.Add('select max(ID) from ACCEPTED_EVENTS where KASSA=:KASSA');
 IBQueryShort.Prepare;
 IBQueryShort.ParamByName('KASSA').AsInteger := KassNum;
 try
  IBQueryShort.Open;
  Result := IBQueryShort.FieldByName('MAX').AsInteger;
 finally
  IBTransactionShort.Commit;
 end;
end;

//-----------------------------------------------
//
procedure TARM_Data.ImportPhoto(Cl_Id: integer; FIO: string);
var
 TmpFileName: string;
 Dt1, Dt2: TDateTime;
begin
 IBTransactionShort.StartTransaction;
 IBQueryShort.SQL.Clear;
 IBQueryShort.SQL.Add('select DATE_ from S_PHOTO where STAFF_ID=:STAFF_ID');
 IBQueryShort.ParamByName('STAFF_ID').AsInteger := Cl_ID;
 IBQueryShort.Open;
 if not IBQueryShort.FieldByName('DATE_').IsNull then
  Dt1 := IBQueryShort.FieldByName('DATE_').AsDateTime
 else
  Dt1 := 0;
 IBQueryShort.Close;
 IBTransactionShort.Commit;
 if FileAge(PhotoPath + FIO + '.jpg', Dt2) then
  //������� ���� �������� ����� � �� � ����� �� �����
  if IncMilliSecond(Dt1, - integer(MilliSecondOf(Dt1))) < IncMilliSecond(Dt2, - integer(MilliSecondOf(Dt2))) then
   //���� � ����� � ������� ������� ����� ������ ����� ��������
   begin
    IBTransactionShort.StartTransaction;
    IBQueryShort.SQL.Clear;
    IBQueryShort.SQL.Add('update S_PHOTO set PHOTO=:PHOTO, DATE_=:DATE_ where STAFF_ID=:STAFF_ID');
    IBQueryShort.ParamByName('STAFF_ID').AsInteger := Cl_Id;
    //�� ��������� ����� ������ ������ ������� �������
    TmpFileName := ResizeJpegPhoto(PhotoPath + FIO + '.jpg');
    IBQueryShort.ParamByName('PHOTO').LoadFromFile(TmpFileName, ftBlob);
    //������� ��������� ������
    DeleteFile(TmpFileName);
    IBQueryShort.ParamByName('DATE_').AsDateTime := Dt2;
    IBQueryShort.ExecSQL;
    IBTransactionShort.Commit;
   end;
end;

//-----------------------------------------------
//�������� ��������� �� ���
function TARM_Data.CheckDB: boolean;
var
 TablesN: TStringList;
begin
 Result := False;
 TablesN := TStringList.Create;
 IBDB.Connected := False;
 IBDB.Params.Clear;
 IBDB.DatabaseName := MainDB;
 IBDB.Params.Add('user_name=' + DB_Login);
 IBDB.Params.Add('password=' + DB_Password);
 IBDB.Params.Add('lc_ctype=UTF8');
 IBDB.Connected := True;
 IBDB.GetTableNames(TablesN);
 if not ((TablesN.IndexOf('ACCEPTED_EVENTS') = -1) or (TablesN.IndexOf('DETAILED_LOG') = -1) or
   (TablesN.IndexOf('S_PHOTO') = -1) or (TablesN.IndexOf('S_STAFF') = -1) or (TablesN.IndexOf('S_LIMITS') = -1) or
   (IBTable1.TableNames.IndexOf('OPER_VIEW_BUF') = -1) or (IBTable1.TableNames.IndexOf('LOG_REC') = -1) or
   (IBTable1.TableNames.IndexOf('OPER_VIEW_CASH_BUF') = -1) or (IBTable1.TableNames.IndexOf('OPER_VIEW_SHORT_BUF') = -1) or
   (IBTable1.TableNames.IndexOf('OPER_VIEW_SHORT_BUF') = -1) or (IBTable1.TableNames.IndexOf('CLIENTS_VIEW') = -1)) then
  Result := True;
end;

//-----------------------------------------------
//�������� �� INI-����� ���� ��� ������������ ��
procedure TARM_Data.ReadBDConfig;
var
 ConfigBD: TIniFile;
begin
 ARM_ExePath := ExtractFilePath(ParamStr(0));
 if not FileExists(ChangeFileExt(ParamStr(0), '.ini')) then //���� INI-���� �� ����������
  begin
   WaitTime := 5;
   ComPtName := 'COM1';
   DB_Login := 'SYSDBA';
   DB_Password := 'masterkey';
   ShowMessage('�� ������ ���� ��������!' + #13 + '����������, ��������� ��������� ���������.');
   if not CallSetup then Application.Terminate;   //��������� ����������
  end;
 ConfigBD := TIniFile.Create(ChangeFileExt(ParamStr(0), '.ini'));
 MainDB := ConfigBD.ReadString('Location', 'Path', '');
 WaitTime := ConfigBD.ReadInteger('Settings', 'WaitTime', 0);
 ComPtName := ConfigBD.ReadString('Settings', 'ComPt', 'COM1');
 IsEmul := ConfigBD.ReadBool('Settings', 'Emul', False);
 LastExpNum := ConfigBD.ReadString('Settings', 'LastExpNum', '000');
 PhotoPath := ConfigBD.ReadString('Settings', 'PhotoPath', '');
 LogFontSize := ConfigBD.ReadInteger('Settings', 'LogFontSize', 8);
 DB_Login := ConfigBD.ReadString('UserInfo', 'Login', 'sysdba');
 DB_Password := ConfigBD.ReadString('UserInfo', 'Password', 'masterkey');
 if not IBDB_Main.Connected then
  begin
   IBDB_Main.DatabaseName := MainDB;
   IBDB_Main.Params.Clear;
   IBDB_Main.Params.Add('user_name=' + DB_Login);
   IBDB_Main.Params.Add('password=' + DB_Password);
   IBDB_Main.Params.Add('lc_ctype=UTF8');
  end;
end;

//-----------------------------------------------
//����� ��������
function TARM_Data.CallSetup: boolean;
begin
 ARM_SetupForm := TARM_SetupForm.Create(Self);
 ARM_SetupForm.ShowModal;
 Result := ARM_SetupForm.Res;
 ARM_SetupForm.Destroy;
end;

//-----------------------------------------------
//����� ���������. �������� �������� � ����������� ������������
procedure TARM_Data.DataModuleCreate(Sender: TObject);
begin
 ReadBDConfig;   //�������� �� INI-����� ���� ��� ����������� � ��
 try
  IBDB_Main.Connected := True; //������� ����������� � ��
 except
  ShowMessage('����������� � �� ��� �� ���������!' + #13 + '� INI-����� ������� �������� ���������.');
  if not CallSetup then
   begin
    Application.Terminate; //��������� ����������
    Exit;
   end
  else
   begin
    ReadBDConfig;
    IBDB_Main.Connected := True; //������� ����������� � ��
   end;
 end;
 if not ARM_Data.CheckDB then
  begin
   ShowMessage('��������� �� ��� �� ������������� ������ ���������!' + #13 + '���������� � ������������');
   Application.Terminate;
  end;
 if EndOfDemo then
  begin
   ShowMessage('���� ���������������� ������ ����������� ����������' + #13 + '���������� � ������������');
   Application.Terminate;
  end;
 //������ ��������� ����� ����� � EXE
 if not DirectoryExists(ARM_ExePath + 'Tmp') then
  CreateDir(ARM_ExePath + 'Tmp');
 Application.CreateForm(TARM_MainBufForm, ARM_MainBufForm);
end;

//-----------------------------------------------
//�������� ��������� ����������������� �������
function TARM_Data.EndOfDemo: boolean;
begin
 {IBTransactionShort.StartTransaction;
 IBQueryShort.SQL.Clear;
 IBQueryShort.SQL.Add('select count(*) from ACCEPTED_EVENTS');
 IBQueryShort.Open;
 Result := IBQueryShort.FieldByName('count').AsInteger > 10000;
 IBQueryShort.Close;
 IBTransactionShort.Commit;}
 Result := False;
end;

end.
