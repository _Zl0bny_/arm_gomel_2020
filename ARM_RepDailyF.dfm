object ARM_RepDailyForm: TARM_RepDailyForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #1054#1090#1095#1105#1090' '#1079#1072' '#1089#1084#1077#1085#1091
  ClientHeight = 166
  ClientWidth = 347
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Icon.Data = {
    0000010001001818000001001800480700001600000028000000180000003000
    00000100180000000000C0060000000000000000000000000000000000000000
    00000000000000000000000000B38B84B38B84B38B84B38B84B38B84B38B84B3
    8B84B38B84B38B84B38B84B38B84B38B84B38B84B38B84B38B84B38B84000000
    000000000000000000000000000000000000000000B38B84FDE8CCFFE8C7FFE3
    BFFFDFB8FFDBB0FFD8A9FFD4A1FFD19CFFD19CFFD19CFFD19CFFD19CFFD19CFF
    D49DBB8F7E000000000000000000000000000000000000000000000000B38B84
    FDE5CEFFE5CAFEE0C2FEDDBBFED9B4FED6ADFED3A7FECE9FFECC9AFECC9AFECC
    9AFECC9AFECC9AFFCF9BBB8E7E00000000000000000000000000000000000000
    0000000000B38B85FDE9D4FFE8D0FEE4C8FEE0C1FEDDBBFED9B4FED6ACFED2A6
    FECE9FFECC9AFECC9AFECC9AFECC9AFFCF9BBB8E7E0000000000000000000000
    00000000B38B84B38B84B38B84B38C86FDECDBDF993EDF993EDF993EDF993EDF
    993EDF993EDF993EDF993EDF993EDF993EDF993EDF993EFFCF9BBB8E7E000000
    000000000000000000000000B38B84FDE8CCFFE8C7B38C87FDEFE2FFEFDEFFEA
    D5FFE7CFFEE3C8FEE0C1FEDCBAFED9B3FED6ADFED2A6FECE9EFECC9AFECC9AFF
    CF9BBB8E7E000000000000000000000000000000B38B84FDE5CEFFE5CAB68F89
    FDF2E9DF993EDF993EDF993EDF993EDF993EDF993EDF993EDF993EDF993EDF99
    3EDF993EDF993EFFCF9BBB8E7E000000000000000000000000000000B38B85FD
    E9D4FFE8D0BA948CFDF6EFFFF6ECFFF1E3FFEEDDFFEBD6FFE7CEFEE3C8FEE0C2
    FEDDBAFED9B4FED6ADFED2A5FECF9EFFCF9BBB8E7E0000000000000000000000
    00000000B38C86FDECDBDF993EBF988EFEF9F6DF993EDF993EDF993EDF993EDF
    993EDF993EDF993EDF993EDF993EDF993EDF993EDF993EFFD1A0BB8E7E000000
    000000000000000000000000B38C87FDEFE2FFEFDEC39C90FEFCFCFFFDF9FFF8
    F0FFF4EAFFF0E3FFEDDCFFEAD5FFE6CEFEE4C8FEDFC0FEDCB9FED9B3FED5ACFF
    D5A6BB8E80000000000000000000000000000000B68F89FDF2E9DF993EC7A091
    FEFDFDDF993EDF993EDF993EDF993EDF993EDF993EDF993EDF993EDF993EDF99
    3EDF993EDF993EFFD8AEBB8F81000000000000000000000000000000BA948CFD
    F6EFFFF6ECCBA393FEFDFDFFFFFFFFFFFEFFFCF8FFF8F1FFF5EAFFF1E3FFEEDC
    FFEAD6FFE7CFFEE3C8FEE0C1FEDDBAFFDCB5BB90830000000000000000000000
    00000000BF988EFEF9F6DF993ED0A795FEFDFDDF993EDF993EDF993EDF993EDF
    993EDF993EDF993EDF993EDF993EDF993EDF993EDF993EFFDFBCBB9185000000
    000000000000000000000000C39C90FEFCFCFFFDF9D4AB96FEFDFDFFFFFFFFFF
    FFFFFFFFFFFEFEFFFBF8FFF7F1FFF4EAFFF1E3FFEEDCFFECD8FFE9D0FFE6CAFF
    E6C5BB9387000000000000000000000000000000C7A091FEFDFDDF993ED8AE97
    FEFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFBF7FFF8F0FFF4EAFEEFE0FFEC
    D8FFE9D0FFE6CAFFE6C5B68F8A000000000000000000000000000000CBA393FE
    FDFDFFFFFFDDB299FEFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFFFBF7
    FFFBF4FFFBF4B68F8AB68F8AB68F8AB68F8AB68F8A0000000000000000000000
    00000000D0A795FEFDFDDF993EE1B69AFEFDFDFFFFFFFFFFFFFFFFFFFFFFFFFF
    FFFFFFFFFFFFFFFEFFFFFBFFFBF4B68F8AF3C381F8B85BEEAC51000000000000
    000000000000000000000000D4AB96FEFDFDFFFFFFE2B69BFEFEFDFFFFFFFFFF
    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB68F8AF4C481E5B27200
    0000000000000000000000000000000000000000D8AE97FEFDFDFFFFFFE2B69B
    FEFEFCFFFFFFFFFFFEFFFFFEFFFFFEFFFEFEFFFEFEFFFEFEFFFFFFFFFFFFB68F
    8ADDB081000000000000000000000000000000000000000000000000DDB299FE
    FDFDFFFFFFE2B69BE3B89EE3B89EE2B89EDFB59DDBB29BD7AE9AD3AB98CFA897
    CBA596CBA596B68F8AB68E880000000000000000000000000000000000000000
    00000000E0B69AFEFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFF
    FFFBFFFBF4B68F8AF3C381F8B85BEEAC51000000000000000000000000000000
    000000000000000000000000E1B69BFEFEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFF
    FFFFFFFFFFFFFFFFFFFFFFFFFFB68F8AF4C481E5B27200000000000000000000
    0000000000000000000000000000000000000000E2B69BFEFEFCFFFFFFFFFFFE
    FFFFFEFFFFFEFFFEFEFFFEFEFFFEFEFFFFFFFFFFFFB68F8AE2BB8D0000000000
    00000000000000000000000000000000000000000000000000000000E2B69BE3
    B89EE3B89EE2B89EDFB59DDBB29BD7AE9AD3AB98CFA897CBA596CBA596B68F8A
    000000000000000000000000000000000000000000000000000000000000F800
    0700F8000700F8000700F8000700C0000700C0000700C0000700C0000700C000
    0700C0000700C0000700C0000700C0000700C0000700C0000700C0000700C000
    0F00C0001F00C0003F00C0003F00C0007F00C000FF00C001FF00C003FF00}
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 73
    Width = 22
    Height = 13
    Caption = #1042#1072#1083':'
  end
  object GroupBox1: TGroupBox
    Left = 3
    Top = 4
    Width = 206
    Height = 57
    Caption = #1042#1074#1077#1076#1080#1090#1077' '#1076#1072#1090#1091
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 0
    object DateTimePicker1: TDateTimePicker
      Left = 27
      Top = 22
      Width = 152
      Height = 23
      Date = 39614.000000000000000000
      Time = 0.138486828713212200
      DateFormat = dfLong
      TabOrder = 0
    end
  end
  object BitBtn1: TBitBtn
    Left = 215
    Top = 11
    Width = 127
    Height = 41
    Caption = #1055#1077#1095#1072#1090#1100
    Default = True
    Glyph.Data = {
      F6060000424DF606000000000000360000002800000018000000180000000100
      180000000000C006000000000000000000000000000000000000C8C8C8C8C8C8
      C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8A0A0A07B7B7B9A9A9A9A9A9A9A9A9A9A9A
      9A5B5B5B4A4A4A393939353535484848454545393939434343515151999999C8
      C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C88B89898A8989858585
      8383838382826F6E6E525151939393B6B6B66767674A494A7978787E7E7E4848
      484141415E5C5C949393C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C0
      C0C0EFEDEDEEEDEDEEEDED898888666666CECECEEBEBEBD3D3D3B0B0B0686868
      A0A0A0C0C0C0CCCCCC9C9B9B535353484848C8C8C8C8C8C8C8C8C8C8C8C8C8C8
      C8C8C8C8C8C8C8C0C0C0EEEEEDEEEDED959494797979FAFAFAFFFFFFE4E4E4CC
      CCCCB5B5B5707070696969787878919191D3D3D3C2C2C2484848C8C8C8C8C8C8
      C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C0C0C0F0EEEEEEEDED9F9F9FFFFFFFFFFF
      FFF1F1F1D7D7D7C9C9C9BDBDBDC6C6C6CACACABABABA84848476AB7685B78434
      3434C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C0C0C0EBEAEAEAE9E9
      9F9F9FFAFAFAE3E3E3DEDEDEDCDCDCDADADAD0D0D0C3C3C3C0C0C0C8C8C8CBCA
      CB9FD6A03DFF3D343635C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C0
      C0C0E9E8E8E9E8E89F9E9EF1F1F1EAEAEAE8E8E8EEEEEEEAEAEADCDCDCD0D0D0
      C4C4C4B4B4B4B1B1B19FD49F96D2993B3C3BC8C8C8C8C8C8C8C8C8C8C8C8C8C8
      C8C8C8C8C8C8C8C1C0C0EFEEEEEAEAEAD9D9D9FFFFFFF7F7F7F6F6F6E8E8E8DD
      DDDDE4E4E4E1E1E1D8D8D8CACACABDBEBDB6B6B7BDBABC484848C8C8C8C8C8C8
      C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C1C0C0F1EFF0F3F2F2E2E2E2DFDEDEE8E8
      E8D9D9DAD1D2D4DCDDDEE9E9EAF5F5F4FDFEFDFCFCFCE9E9E9BEBEBE6363637B
      7B7BC8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C2C1C1F1F0F0F1F0F0
      F4F3F3EAE9E9868787B8B6B4DDD8D3CDCBC9C2C2C3BDBEC2CCCED0E3E3E3C5C4
      C4818080A2A2A28E8E8EC8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C0
      C0C0EFEEEEF0EFEFF2F1F1EEEDEDA9A6A5F7E6CCF8E6C2F4DEB9EBD7B2E3CDAD
      B6A69C99989A929292ECEBEBEDECEC919090C8C8C8C8C8C8C8C8C8C8C8C8C8C8
      C8C8C8C8C8C8C8C1C0C0EFEEEEF0EFEFF1F0F0F4F4F4A7A19FFFF0D6FFF0CEFF
      EAC1FFE6B7FFEDB7AB9281B9B9B9F2F1F1EDECECEBEAEA919090C8C8C8C8C8C8
      C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C0C0C0EEEDEDF0EFEFF1F0F0E9E9E9E1D5
      D0FFF8E3FFF0D6FFEBCCFFE9C4FFECBF9B887FC6C6C6EEEEEEEBEAEAE9E8E890
      9090C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C0BFBFEFEDEEF0EFEF
      F1F0F0A8A6A7F4E9E5FFFCEEFFF2DFFFEFD5FFF2CFC2B196786E6DDEDDDDECEC
      ECE7E7E8E5E4E4908F8FC8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8BC
      BCBCEDECECEEEDEDEEEEEEAEA8A8FFFDFEFFFFFDFFFEF1FFFAE6FFFFE2AA978B
      8F8C8CECEBEBE7E7E7E2E2E2E0DFE08F8F8F6766CA1111831211831110830E0E
      830C0B830B0A830808830706830604830403823C357866567E72638487788CCC
      B9BEAB98959A9191DFE0E0E4E3E3DFDEDDDAD9DABEBDBD8C8C8C8988FD3230FF
      302DFFC6C5FFC6C5FF302DFF231FFFC6C5FFBFBEFF110EFF302DFF0C09F50906
      EC0704E00502D10300C945457AD6D6D7C0C0C0B7B7B7B5B5B5B1B0B1AAAAAAA3
      A3A38E8DFD3834FF2A27FF201DFFECECFEECECFE201CFF0E0BF9EDEDFFEDECFF
      0E0BF90F0CF80C09F10906E70704D90501D0404075BEBEBE8D8D8D8E8E8E9B9B
      9BC1C1C1D1D1D1C8C8C89391FD3F3DFF302EFF2623FF0E0BF9ECECFEECECFF0E
      0BF90E0BF9ECECFEECECFE0E0BF90E0BF60B08ED0906E20704DA3E3E73B0B0B0
      FFFFFFFFFFFFF8F8F8D7D7D7CFCFCFC8C8C89896FC4C4AFF3D3BFF2F2CFFECEC
      FEECECFE0E0BF90E0BF9ECECFEECECFF0E0BF90E0BF90E0BF90B08F30906EB08
      05E63C3C71ACACACFFFFFFF8F8F8D6D6D6CECECEC8C8C8C8C8C89896FC625FFE
      5654FFCDCCFFCAC9FF3430FF2A28FFC4C3FFC3C3FF2824FF2723FF231FFF1D1A
      FF1A17FD1916FA1512FD3A396EB1B0B0FFFFFFD6D6D6CECECEC8C8C8C8C8C8C8
      C8C8A4A3F3AAA8F2A5A4F3A2A1F39E9DF3807FBD7372BA9895F39593F49493F4
      9391F39290F3918FF2918FF3908FF18E8DF08A88BCB9B9B9D9D9D9D3D3D3C8C8
      C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8DD
      DBDBDCDADADCDBDADDDBDBDCDBDCDCDBDBDCDBDBDAD9D9D6D5D5C6C6C6C2C2C2
      D8D8D8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8
      C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8
      C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8}
    TabOrder = 1
    OnClick = BitBtn1Click
  end
  object RadioGroup1: TRadioGroup
    Left = 3
    Top = 98
    Width = 206
    Height = 60
    Caption = #1060#1086#1088#1084#1072' '#1086#1090#1095#1105#1090#1072
    Ctl3D = False
    ItemIndex = 0
    Items.Strings = (
      #1050#1088#1072#1090#1082#1080#1081' '#1086#1090#1095#1105#1090
      #1055#1086#1083#1085#1099#1081' '#1086#1090#1095#1105#1090)
    ParentCtl3D = False
    TabOrder = 2
  end
  object ComboBox1: TComboBox
    Left = 36
    Top = 70
    Width = 53
    Height = 21
    Style = csDropDownList
    ItemIndex = 0
    TabOrder = 3
    Text = '1'
    Items.Strings = (
      '1'
      '2')
  end
  object IBQuery1: TIBQuery
    Database = ARM_Data.IBDB_Main
    Transaction = IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    Left = 216
    Top = 56
  end
  object IBTransaction1: TIBTransaction
    DefaultDatabase = ARM_Data.IBDB_Main
    Left = 248
    Top = 56
  end
  object frxReport1: TfrxReport
    Version = '6.7.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = #1055#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39609.945803333300000000
    ReportOptions.LastChange = 42968.837035914350000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 280
    Top = 56
    Datasets = <
      item
        DataSet = frxDBDataset1
        DataSetName = 'frxDBDataset1'
      end>
    Variables = <
      item
        Name = 'Date_of_rep'
        Value = '40465,1384868287'
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 20.000000000000000000
      BottomMargin = 20.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Frame.Typ = []
        Height = 102.047310000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 11.000000000000000000
          Width = 230.551330000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold, fsItalic]
          Frame.Typ = []
          Memo.UTF8W = (
            #1040#1056#1052' '#1082#1072#1089#1089#1080#1088#1072' '#1073#1091#1092#1077#1090#1072)
          ParentFont = False
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 31.897650000000000000
          Width = 548.031850000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 419.527830000000000000
          Top = 41.456710000000000000
          Width = 260.787570000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            #1057#1075#1077#1085#1077#1088#1080#1088#1086#1074#1072#1085#1086' [Date] [Time]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 491.338900000000000000
          Width = 188.976500000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold, fsItalic]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            #1054#1058#1063#1025#1058' '#1047#1040' '#1057#1052#1045#1053#1059)
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 585.827150000000000000
          Top = 60.354360000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsItalic]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            #1050#1088#1072#1090#1082#1080#1081)
          ParentFont = False
        end
        object Date_of_rep: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811380000000000000
          Top = 22.677180000000000000
          Width = 128.504020000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = 'dd mmmm yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold, fsItalic]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[Date_of_rep]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Frame.Typ = []
        Height = 45.354360000000000000
        Top = 343.937230000000000000
        Width = 680.315400000000000000
        object Line2: TfrxLineView
          AllowVectorExport = True
          Top = 21.000000000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Top = 24.779530000000020000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[Page#] '#1080#1079' [TotalPages#]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 143.622140000000000000
        Width = 680.315400000000000000
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Width = 109.606370000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            #1044#1072#1090#1072' '#1080' '#1074#1088#1077#1084#1103)
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            #1058#1072#1073'.'#8470)
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 321.260050000000000000
          Width = 139.842610000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077)
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102660000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            #1044#1086#1083#1078#1085#1086#1089#1090#1100)
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Width = 139.842610000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            #1060'.'#1048'.'#1054'.')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            #1057#1091#1084#1084#1072)
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 222.992270000000000000
        Width = 680.315400000000000000
        DataSet = frxDBDataset1
        DataSetName = 'frxDBDataset1'
        RowCount = 0
        object frxDBDataset1DATE_: TfrxMemoView
          AllowVectorExport = True
          Width = 109.606370000000000000
          Height = 15.118120000000000000
          DataField = 'DATE_'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Memo.UTF8W = (
            '[frxDBDataset1."DATE_"]')
          ParentFont = False
        end
        object frxDBDataset1F_I_O: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Width = 139.842610000000000000
          Height = 15.118120000000000000
          DataField = 'F_I_O'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDBDataset1."F_I_O"]')
          ParentFont = False
        end
        object frxDBDataset1T_NUMBER: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          DataField = 'T_NUMBER'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Memo.UTF8W = (
            '[frxDBDataset1."T_NUMBER"]')
          ParentFont = False
        end
        object frxDBDataset1DIV_NAME: TfrxMemoView
          AllowVectorExport = True
          Left = 321.260050000000000000
          Width = 139.842610000000000000
          Height = 15.118120000000000000
          DataField = 'DIV_NAME'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDBDataset1."DIV_NAME"]')
          ParentFont = False
        end
        object frxDBDataset1DIV_NAME1: TfrxMemoView
          AllowVectorExport = True
          Left = 461.102660000000000000
          Width = 143.622140000000000000
          Height = 15.118120000000000000
          DataField = 'POS_NAME'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Memo.UTF8W = (
            '[frxDBDataset1."POS_NAME"]')
          ParentFont = False
        end
        object frxDBDataset1SUMM: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataField = 'SUMM'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBDataset1."SUMM"]')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 260.787570000000000000
        Width = 680.315400000000000000
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000022000
          Width = 249.448980000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            #1048#1090#1086#1075#1086':')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 321.260050000000000000
          Top = 3.779530000000022000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold, fsItalic]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDBDataset1."SUMM">,MasterData1)]')
          ParentFont = False
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
    end
  end
  object frxDBDataset1: TfrxDBDataset
    UserName = 'frxDBDataset1'
    CloseDataSource = False
    DataSet = IBQuery1
    BCDToCurrency = False
    Left = 216
    Top = 88
  end
end
