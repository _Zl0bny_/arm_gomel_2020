object ARM_Data: TARM_Data
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 403
  Width = 363
  object IBDB_Main: TIBDatabase
    LoginPrompt = False
    ServerType = 'IBServer'
    Left = 48
    Top = 24
  end
  object IBTrans_Main: TIBTransaction
    AllowAutoStart = False
    DefaultDatabase = IBDB_Main
    Params.Strings = (
      'read_committed'
      'rec_version'
      'nowait')
    Left = 48
    Top = 72
  end
  object IBQuery_Main: TIBQuery
    Database = IBDB_Main
    Transaction = IBTrans_Main
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    Left = 48
    Top = 128
  end
  object IBTransactionShort: TIBTransaction
    DefaultDatabase = IBDB_Main
    Params.Strings = (
      'read_committed'
      'rec_version'
      'nowait')
    Left = 48
    Top = 184
  end
  object IBQueryShort: TIBQuery
    Database = IBDB_Main
    Transaction = IBTransactionShort
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    Left = 48
    Top = 240
  end
  object IBDB: TIBDatabase
    LoginPrompt = False
    DefaultTransaction = IBTrans
    ServerType = 'IBServer'
    Left = 144
    Top = 24
  end
  object IBTrans: TIBTransaction
    DefaultDatabase = IBDB
    Params.Strings = (
      'read_committed'
      'rec_version'
      'nowait')
    Left = 145
    Top = 72
  end
  object IBTable1: TIBTable
    Database = IBDB
    Transaction = IBTrans
    BufferChunks = 1000
    CachedUpdates = False
    TableTypes = [ttView]
    UniDirectional = False
    Left = 145
    Top = 128
  end
end
