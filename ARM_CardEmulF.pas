unit ARM_CardEmulF;

interface

uses
  Windows,
  Messages,
  SysUtils,
  Variants,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  StdCtrls,
  ExtCtrls,
  Buttons,
  ARM_Unit;

type
  TARM_CardEmulForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    GroupBox1: TGroupBox;
    LabeledEdit1: TLabeledEdit;
    LabeledEdit2: TLabeledEdit;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    LabeledEdit3: TLabeledEdit;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
  private
    { Private declarations }
    //CardGroup: integer;
    CardCode: integer;
  public
    { Public declarations }
  end;

var
  ARM_CardEmulForm: TARM_CardEmulForm;

implementation

{$R *.dfm}

//-----------------------------------------------
//
procedure TARM_CardEmulForm.BitBtn1Click(Sender: TObject);
begin
 if RadioButton1.Checked then
  begin
   if IsNumberStr(LabeledEdit1.Text) and IsNumberStr(LabeledEdit2.Text) and
      (Length(LabeledEdit1.Text) = 3) and (Length(LabeledEdit2.Text) = 5) then
    begin
     CardGroup := StrToInt(LabeledEdit1.Text);
     CardCode := StrToInt(LabeledEdit2.Text);
     CardNumber := (CardGroup shl 16) or CardCode;
     Close;
    end
   else
    begin
     CardGroup := 0;
     CardCode := 0;
     CardNumber := 0;
     Close;
    end;
  end
 else
  begin
   if IsNumberStr(LabeledEdit3.Text) then
    begin
     CardNumber := StrToInt(LabeledEdit3.Text);
     CardGroup := 100;
     Close;
    end
   else
    begin
     CardGroup := 0;
     CardCode := 0;
     CardNumber := 0;
     Close;
    end;
  end;
end;

//-----------------------------------------------
//
procedure TARM_CardEmulForm.BitBtn2Click(Sender: TObject);
begin
 CardGroup := 0;
 CardCode := 0;
 CardNumber := 0;
 Close;
end;

//-----------------------------------------------
//
procedure TARM_CardEmulForm.FormCreate(Sender: TObject);
begin
 CardGroup := 0;
 CardCode := 0;
 CardNumber := 0;
end;

//-----------------------------------------------
//
procedure TARM_CardEmulForm.RadioButton1Click(Sender: TObject);
begin
 LabeledEdit1.Enabled := RadioButton1.Checked;
 LabeledEdit2.Enabled := RadioButton1.Checked;
 LabeledEdit3.Enabled := not RadioButton1.Checked;
end;

end.
