unit ARM_AddClientF;

interface

uses
  Windows,
  Messages,
  SysUtils,
  Variants,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  ExtCtrls,
  StdCtrls,
  Buttons,
  ARM_Unit,
  ARM_DataMod,
  ARM_CardReadF,
  ARM_CardEmulF,
  Vcl.AppEvnts,
  Vcl.Mask;

type
  ARM_AddClientFormState=(fsAdd, fsEdit);

  TARM_AddClientForm = class(TForm)
    GroupBox1: TGroupBox;
    LabeledEdit1: TLabeledEdit;
    LabeledEdit2: TLabeledEdit;
    LabeledEdit3: TLabeledEdit;
    ComboBox1: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    ComboBox2: TComboBox;
    GroupBox3: TGroupBox;
    LabeledEdit6: TLabeledEdit;
    BitBtn3: TBitBtn;
    Image1: TImage;
    LabeledEdit7: TLabeledEdit;
    BitBtn6: TBitBtn;
    BitBtn7: TBitBtn;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    Edit1: TEdit;
    Label4: TLabel;
    Label5: TLabel;
    Button1: TButton;
    ApplicationEvents1: TApplicationEvents;
    MaskEdit3: TMaskEdit;
    Label6: TLabel;
    GroupBox4: TGroupBox;
    Edit2: TEdit;
    procedure BitBtn7Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
  private
    { Private declarations }
    OldName, OldSecName, OldLastName, OldT_Num,
    OldDiv, OldPosition: string;
    OldNumber: int64;
    OldLim: Currency;
    function CheckCorrect: boolean;
    function IsChange: boolean;
    function CheckCard: string;
    function CheckUnique: boolean; //True - ���� ������ ���������
    function GetMaxClientsId: integer;
    procedure AddClient;
    procedure SetLimit(Cl_Id: integer);
    procedure EditClient;
    procedure FillComboBoxes;
  public
    { Public declarations }
    Res: boolean;
    Rec_Id: integer;
    FormSt: ARM_AddClientFormState;
    PhotoFile: string;
  end;

var
  ARM_AddClientForm: TARM_AddClientForm;

implementation

{$R *.dfm}

//-----------------------------------------------
//�������� �������                                 +
procedure TARM_AddClientForm.AddClient;
begin
 if CheckCorrect and CheckUnique then
  begin
   with ARM_Data do
    begin
     IBTransactionShort.StartTransaction;
     IBQueryShort.SQL.Clear;
     IBQueryShort.SQL.Add('insert into S_STAFF values (null, :TNUM, :LAST_NAME, :FIRST_NAME, :MIDDLE_NAME, :POS, :DEPARTMENT, :CARD_CODE, :DESCRIPTION_)');
     IBQueryShort.ParamByName('FIRST_NAME').AsString := Trim(LabeledEdit2.Text);
     IBQueryShort.ParamByName('MIDDLE_NAME').AsString := Trim(LabeledEdit3.Text);
     IBQueryShort.ParamByName('LAST_NAME').AsString := Trim(LabeledEdit1.Text);
     IBQueryShort.ParamByName('TNUM').AsString := Trim(LabeledEdit7.Text);
     IBQueryShort.ParamByName('POS').AsString := Trim(ComboBox2.Text);
     IBQueryShort.ParamByName('DEPARTMENT').AsString := Trim(ComboBox1.Text);
     IBQueryShort.ParamByName('CARD_CODE').AsInteger := StrToInt(LabeledEdit6.Text);
     if Trim(Edit2.Text) = '' then
      IBQueryShort.ParamByName('DESCRIPTION_').Clear
     else
      IBQueryShort.ParamByName('DESCRIPTION_').AsString := Trim(Edit2.Text);
     IBQueryShort.ExecSQL;
     IBTransactionShort.Commit;
    end;
   Rec_Id := GetMaxClientsId;
   SetLimit(Rec_Id);
   ARM_Data.ImportPhoto(Rec_Id, LabeledEdit1.Text + ' ' + LabeledEdit2.Text + ' ' + LabeledEdit3.Text);
   Res := True;
   Close;
  end;
end;

//-----------------------------------------------
//�������� ������ � �������                       +
procedure TARM_AddClientForm.EditClient;
begin
 if not IsChange then Close;
 if CheckCorrect then
  with ARM_Data do
   begin
    IBTransactionShort.StartTransaction;
    IBQueryShort.SQL.Clear;
    IBQueryShort.SQL.Add('update S_STAFF set TNUM=:TNUM, LAST_NAME=:LAST_NAME, FIRST_NAME=:FIRST_NAME, MIDDLE_NAME=:MIDDLE_NAME, POS=:POS, DEPARTMENT=:DEPARTMENT, CARD_CODE=:CARD_CODE, DESCRIPTION_=:DESCRIPTION_ where ID=:ID');
    IBQueryShort.Prepare;
    IBQueryShort.ParamByName('ID').AsInteger := Rec_Id;
    IBQueryShort.ParamByName('FIRST_NAME').AsString := Trim(LabeledEdit2.Text);
    IBQueryShort.ParamByName('MIDDLE_NAME').AsString := Trim(LabeledEdit3.Text);
    IBQueryShort.ParamByName('LAST_NAME').AsString := Trim(LabeledEdit1.Text);
    IBQueryShort.ParamByName('TNUM').AsString := Trim(LabeledEdit7.Text);
    IBQueryShort.ParamByName('POS').AsString := Trim(ComboBox2.Text);
    IBQueryShort.ParamByName('DEPARTMENT').AsString := Trim(ComboBox1.Text);
    IBQueryShort.ParamByName('CARD_CODE').AsCurrency := StrToInt(LabeledEdit6.Text);
    if Trim(Edit2.Text) = '' then
      IBQueryShort.ParamByName('DESCRIPTION_').Clear
     else
      IBQueryShort.ParamByName('DESCRIPTION_').AsString := Trim(Edit2.Text);
    IBQueryShort.ExecSQL;
    IBTransactionShort.Commit;
    Res := True;
    SetLimit(Rec_Id);
    ImportPhoto(Rec_Id, LabeledEdit1.Text + ' ' + LabeledEdit2.Text + ' ' + LabeledEdit3.Text);
    Close;
   end;
end;

//-----------------------------------------------
//������ ����� ������                       +
procedure TARM_AddClientForm.ApplicationEvents1Message(var Msg: tagMSG;
  var Handled: Boolean);
begin
 if (Edit1.Focused and (Msg.message = WM_CHAR)) then
  begin
   case Msg.wParam of
    44, 46:
     begin
      if (Pos(',', Edit1.Text) = 0) then
       if Length(Edit1.Text) <> 0 then
        Edit1.Text := Edit1.Text + ','
       else
        Edit1.Text := '0,';
      Handled := True;
      Edit1.SelStart := Length(Edit1.Text);  
     end;
    48..57:
     begin
      if Edit1.Text = '0' then
       Edit1.Text := chr(Msg.wParam)
      else
       Edit1.Text := Edit1.Text + chr(Msg.wParam);
      Handled := True; 
      Edit1.SelStart := Length(Edit1.Text);
     end;
    8: Handled := False;         
   else
    Handled := True;         
   end;
  end;
end;

//-----------------------------------------------
//����������� �� �����������                       +
procedure TARM_AddClientForm.BitBtn3Click(Sender: TObject);
var
 St: string;
begin
 if IsEmul then
  begin
   ARM_CardEmulForm := TARM_CardEmulForm.Create(Self);
   ARM_CardEmulForm.ShowModal;
   if CardGroup <> 0 then
    LabeledEdit6.Text := IntToStr(CardNumber);
   ARM_CardEmulForm.Destroy;
  end
 else
  begin
   ARM_CardReadForm := TARM_CardReadForm.Create(Self);
   ARM_CardReadForm.Show;
   if CardGroup <> 0 then
    LabeledEdit6.Text := IntToStr(CardNumber);
   ARM_CardReadForm.Destroy;
  end;
 St := CheckCard;
 if St <> '' then
  begin
   ShowMessage('������ ����� ���������������� � ����!' + #13 + '��������: ' + St);
   LabeledEdit6.Text := '';
  end;
 BitBtn7.Enabled := LabeledEdit6.Text <> '';
end;

//-----------------------------------------------
//������ "�������"                                 +
procedure TARM_AddClientForm.BitBtn6Click(Sender: TObject);
begin
 Close;
end;

//-----------------------------------------------
//�������� "��"                                     +
procedure TARM_AddClientForm.BitBtn7Click(Sender: TObject);
begin
 case FormSt of
  fsAdd: AddClient;
  fsEdit: EditClient;
 end;
end;

//-----------------------------------------------
//��������� ��� �������� ����                      +
procedure TARM_AddClientForm.Button1Click(Sender: TObject);
begin
 PhotoFile := PhotoPath + LabeledEdit1.Text + ' ' + LabeledEdit2.Text + ' ' + LabeledEdit3.Text + '.jpg';
 if FileExists(PhotoFile) then
  begin
   Label3.Visible := False;
   Image1.Visible := True;
   Image1.Picture.LoadFromFile(PhotoFile);
  end
 else
  begin
   Image1.Visible := False;
   Label3.Visible := True;
  end;
end;

//-----------------------------------------------
//���� ����� ���� � ����, ���������� ��� ���������  +
function TARM_AddClientForm.CheckCard: string;
begin
 if LabeledEdit6.Text = '' then
  begin
   Result := '';
   Exit;
  end;
 with ARM_Data do
  begin
   IBTransactionShort.StartTransaction;
   IBQueryShort.SQL.Clear;
   IBQueryShort.SQL.Add('select LAST_NAME, FIRST_NAME, MIDDLE_NAME from CLIENTS_VIEW where (CARD_CODE=:CARD_CODE)');
   IBQueryShort.ParamByName('CARD_CODE').AsInteger := StrToInt(LabeledEdit6.Text);
   try
    IBQueryShort.Open;
    if not IBQueryShort.Eof then
     Result := IBQueryShort.FieldByName('LAST_NAME').AsString + ' ' + IBQueryShort.FieldByName('FIRST_NAME').AsString + ' ' + IBQueryShort.FieldByName('MIDDLE_NAME').AsString
    else
     Result := '';
   finally
    IBQueryShort.Close;
    IBTransactionShort.Commit;
   end;
  end;
end;

//-----------------------------------------------
//�������� ������������ �����                      +
function TARM_AddClientForm.CheckCorrect: boolean;
begin
 Result := False;
 if Trim(LabeledEdit1.Text) = '' then
  begin
   ShowMessage('�� ��������� ���!');
   LabeledEdit1.SetFocus;
   Exit;
  end;
 if Trim(LabeledEdit2.Text) = '' then
  begin
   ShowMessage('�� ��������� ��������!');
   LabeledEdit2.SetFocus;
   Exit;
  end;
 if Trim(LabeledEdit3.Text) = '' then
  begin
   ShowMessage('�� ��������� �������!');
   LabeledEdit3.SetFocus;
   Exit;
  end;
 if Trim(LabeledEdit7.Text) = '' then
  begin
   ShowMessage('�� �������� ��������� �����!');
   LabeledEdit7.SetFocus;
   Exit;
  end;
 if ComboBox1.Text = '' then
  begin
   ShowMessage('�� ��������� �������������!');
   ComboBox1.SetFocus;
   Exit;
  end;
 if ComboBox2.Text = '' then
  begin
   ShowMessage('�� ��������� ���������!');
   ComboBox2.SetFocus;
   Exit;
  end;
 if MaskEdit3.Text <> '  .  .    ' then
  try
   StrToDate(MaskEdit3.Text);
  except
   ShowMessage('������� �������� ���� �������� �����!');
   MaskEdit3.SetFocus;
   Exit;
  end;
 Result := True;
end;

//-----------------------------------------------
//True - ���� ������ ���������                    +
function TARM_AddClientForm.CheckUnique: boolean;
begin
 with ARM_Data do
  begin
   IBTransactionShort.StartTransaction;
   IBQueryShort.SQL.Clear;
   IBQueryShort.SQL.Add('select count(*) from CLIENTS_VIEW where (TNUM=:TNUM) and (LAST_NAME=:LAST_NAME) and (FIRST_NAME=:FIRST_NAME) and (MIDDLE_NAME=:MIDDLE_NAME)');
   IBQueryShort.ParamByName('TNUM').AsString := Trim(LabeledEdit7.Text);
   IBQueryShort.ParamByName('LAST_NAME').AsString := Trim(LabeledEdit1.Text);
   IBQueryShort.ParamByName('FIRST_NAME').AsString := Trim(LabeledEdit2.Text);
   IBQueryShort.ParamByName('MIDDLE_NAME').AsString := Trim(LabeledEdit3.Text);
   try
    IBQueryShort.Open;
    if IBQueryShort.FieldByName('COUNT').AsInteger <> 0 then
     begin
      Result := False;
      ShowMessage('� ���� ��� ��������������� ' + Trim(LabeledEdit1.Text) + ' ' + Trim(LabeledEdit2.Text) + ' ' + Trim(LabeledEdit3.Text) + ' � ��������� ������� ' + Trim(LabeledEdit7.Text) + '!');
     end
    else
     Result := True;
    IBQueryShort.Close;
   finally
    IBTransactionShort.Commit;
   end;
  end;
end;

//-----------------------------------------------
//��������� ����������                             +
procedure TARM_AddClientForm.FillComboBoxes;
begin
 //���������
 ComboBox2.Items.BeginUpdate;
 ComboBox2.Items.Clear;
 with ARM_Data do
  begin
   IBTransactionShort.StartTransaction;
   IBQueryShort.SQL.Clear;
   IBQueryShort.SQL.Add('select distinct POS from CLIENTS_VIEW order by POS');
   try
    IBQueryShort.Open;
    while not IBQueryShort.Eof do
     begin
      ComboBox2.Items.Add(IBQueryShort.FieldByName('POS').AsString);
      IBQueryShort.Next;
     end;
   finally
    IBQueryShort.Close;
    IBTransactionShort.Commit;
   end;
  end;
 ComboBox2.Items.EndUpdate;
 //�������������
 ComboBox1.Items.BeginUpdate;
 ComboBox1.Items.Clear;
 with ARM_Data do
  begin
   IBTransactionShort.StartTransaction;
   IBQueryShort.SQL.Clear;
   IBQueryShort.SQL.Add('select distinct DEPARTMENT from CLIENTS_VIEW order by DEPARTMENT');
   try
    IBQueryShort.Open;
    while not IBQueryShort.Eof do
     begin
      ComboBox1.Items.Add(IBQueryShort.FieldByName('DEPARTMENT').AsString);
      IBQueryShort.Next;
     end;
   finally
    IBQueryShort.Close;
    IBTransactionShort.Commit;
   end;
  end;
 ComboBox1.Items.EndUpdate;
end;

//-----------------------------------------------
//���������� �����, ���������� �������             +
procedure TARM_AddClientForm.FormCreate(Sender: TObject);
begin
 FillComboBoxes;
end;

//-----------------------------------------------
//���������� ����� � ������                        +
procedure TARM_AddClientForm.FormShow(Sender: TObject);
begin
 Res := False;
 case FormSt of
  fsAdd:
   begin
    Caption := '�������� �������';
    OldName := '';
    OldSecName := '';
    OldLastName := '';
    OldT_Num := '';
    OldDiv := '';
    OldPosition := '';
    OldNumber := 0;
    OldLim := -1;
   end;
  fsEdit:
   begin
    Caption := '�������� �������';
    OldName := LabeledEdit2.Text;
    OldSecName := LabeledEdit3.Text;
    OldLastName := LabeledEdit1.Text;
    OldT_Num := LabeledEdit7.Text;
    OldDiv := ComboBox1.Text;
    OldPosition := ComboBox2.Text;
    OldNumber := StrToInt(LabeledEdit6.Text);
    if Edit1.Text = '' then
     OldLim := -1
    else 
     OldLim := StrToCurr(Edit1.Text);
   end;
 end;
 BitBtn7.Enabled := LabeledEdit6.Text <> '';
end;

//-----------------------------------------------
//���������� ������������ ID �� ������� ��������  +
function TARM_AddClientForm.GetMaxClientsId: integer;
begin
 with ARM_Data do
  begin
   IBTransactionShort.StartTransaction;
   IBQueryShort.SQL.Clear;
   IBQueryShort.SQL.Add('select MAX(ID) from S_STAFF');
   IBQueryShort.Open;
   Result := IBQueryShort.FieldByName('MAX').AsInteger;
   IBQueryShort.Close;
   IBTransactionShort.Commit;
  end;
end;

//-----------------------------------------------
//��������� �������� true, ���� �� ����� ���������
//���������                                         +
function TARM_AddClientForm.IsChange: boolean;
var
 index: integer;
 Lim_: currency;
begin
 index := 0;
 if OldName <> LabeledEdit2.Text then inc(index);
 if OldSecName <> LabeledEdit3.Text then inc(index);
 if OldLastName <> LabeledEdit1.Text then inc(index);
 if OldT_Num <> LabeledEdit7.Text then inc(index);
 if OldDiv <> ComboBox1.Text then inc(index);
 if OldPosition <> ComboBox2.Text then inc(index);
 if OldNumber <> StrToInt(LabeledEdit6.Text) then inc(index);
 if Edit1.Text = '' then
  Lim_ := -1
 else
  Lim_ := StrToCurr(Edit1.Text);
 if OldLim <> Lim_ then inc(index);
 Result := index <> 0;
end;

//-----------------------------------------------
//������������� �����                              +
procedure TARM_AddClientForm.SetLimit(Cl_Id: integer);
begin
 with ARM_Data do
  begin
   IBTransactionShort.StartTransaction;
   IBQueryShort.SQL.Clear;
   IBQueryShort.SQL.Add('update S_LIMITS set LIM=:LIM, VALIDTHRU=:VALIDTHRU where STAFF_ID=:STAFF_ID');
   if Edit1.Text = '' then
    IBQueryShort.ParamByName('LIM').Clear
   else 
    IBQueryShort.ParamByName('LIM').AsCurrency := StrToCurr(Edit1.Text);
   try
    IBQueryShort.ParamByName('VALIDTHRU').AsDate := StrToDate(MaskEdit3.Text);
   except
    IBQueryShort.ParamByName('VALIDTHRU').Clear;
   end;
   IBQueryShort.ParamByName('STAFF_ID').AsInteger := Cl_Id;
   IBQueryShort.ExecSQL;
   IBTransactionShort.Commit;
  end;
end;

end.
