unit ARM_RepDailyF;

interface

uses
  Windows,
  Messages,
  SysUtils,
  Variants,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  ARM_DataMod,
  ARM_Unit,
  frxClass,
  frxDBSet,
  IBX.IBDatabase,
  DB,
  IBX.IBCustomDataSet,
  IBX.IBQuery,
  ExtCtrls,
  StdCtrls,
  Buttons,
  ComCtrls,
  DateUtils,
  frxExportRTF,
  frxExportHTML,
  frxExportPDF,
  AppEvnts;

type
  TARM_RepDailyForm = class(TForm)
    GroupBox1: TGroupBox;
    DateTimePicker1: TDateTimePicker;
    BitBtn1: TBitBtn;
    RadioGroup1: TRadioGroup;
    IBQuery1: TIBQuery;
    IBTransaction1: TIBTransaction;
    frxReport1: TfrxReport;
    frxDBDataset1: TfrxDBDataset;
    Label1: TLabel;
    ComboBox1: TComboBox;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ARM_RepDailyForm: TARM_RepDailyForm;

implementation

{$R *.dfm}

//-----------------------------------------------
//
procedure TARM_RepDailyForm.BitBtn1Click(Sender: TObject);
begin
 IBTransaction1.StartTransaction;
 IBQuery1.SQL.Clear;
 case RadioGroup1.ItemIndex of
  0: IBQuery1.SQL.Add('select * from OPER_VIEW_SHORT_BUF where (DATE_ > :DATE_1) and (DATE_ < :DATE_2) and (KASSA = :KASSA) order by DATE_');
  1: IBQuery1.SQL.Add('select * from OPER_VIEW_BUF where (DATE_ > :DATE_1) and (DATE_ < :DATE_2) and (KASSA = :KASSA) order by DATE_');
 end;
 IBQuery1.ParamByName('DATE_1').AsDateTime := StartOfTheDay(DateTimePicker1.Date);
 IBQuery1.ParamByName('DATE_2').AsDateTime := EndOfTheDay(DateTimePicker1.Date);
 IBQuery1.ParamByName('KASSA').AsInteger := ComboBox1.ItemIndex + 1;
 IBQuery1.Open;
 case RadioGroup1.ItemIndex of
  0: frxReport1.LoadFromFile(ARM_ExePath + 'Reports\DailyShort.fr3');
  1: frxReport1.LoadFromFile(ARM_ExePath + 'Reports\DailyFull.fr3');
 end;
 frxReport1.Variables['Date_of_rep'] := DateTimePicker1.Date;
 frxReport1.ShowReport;
 IBQuery1.Close;
 IBTransaction1.Commit;
end;

//-----------------------------------------------
//
procedure TARM_RepDailyForm.FormCreate(Sender: TObject);
begin
 DateTimePicker1.Date := Date;
end;

end.
