program ARM_Bufet;

uses
  Vcl.Forms,
  ARM_DataMod in 'ARM_DataMod.pas' {ARM_Data: TDataModule},
  ARM_MainBufF in 'ARM_MainBufF.pas' {ARM_MainBufForm},
  ARM_SetupF in 'ARM_SetupF.pas' {ARM_SetupForm},
  ARM_Unit in 'ARM_Unit.pas',
  ARM_ExportF in 'ARM_ExportF.pas' {ARM_ExportForm},
  ARM_CardReadF in 'ARM_CardReadF.pas' {ARM_CardReadForm},
  ARM_HelpF in 'ARM_HelpF.pas' {ARM_HelpForm},
  ARM_AboutF in 'ARM_AboutF.pas' {ARM_AboutForm},
  ARM_CardEmulF in 'ARM_CardEmulF.pas' {ARM_CardEmulForm},
  ARM_RepDailyF in 'ARM_RepDailyF.pas' {ARM_RepDailyForm},
  ARM_RepClientF in 'ARM_RepClientF.pas' {ARM_RepClientForm},
  ARM_BlindRepF in 'ARM_BlindRepF.pas' {ARM_BlindRepForm},
  ARM_SprClientsF in 'ARM_SprClientsF.pas' {ARM_SprClientsForm},
  ARM_AddClientF in 'ARM_AddClientF.pas' {ARM_AddClientForm},
  ARM_StatRepF in 'ARM_StatRepF.pas' {ARM_StatRepForm},
  ARM_RepDepartF in 'ARM_RepDepartF.pas' {ARM_RepDepartForm},
  ARM_ChooseKassaForm in 'ARM_ChooseKassaForm.pas' {ARM_ChooseKassaF};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := '��� "�����"';
  Application.CreateForm(TARM_Data, ARM_Data);
  Application.Run;
end.
