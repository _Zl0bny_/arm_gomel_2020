unit ARM_AboutF;

interface

uses
  Windows,
  Messages,
  SysUtils,
  Variants,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  StdCtrls,
  Buttons,
  ARM_Unit,
  ARM_DataMod;

type
  TARM_AboutForm = class(TForm)
    BitBtn1: TBitBtn;
    Memo1: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ARM_AboutForm: TARM_AboutForm;

implementation

{$R *.dfm}

procedure TARM_AboutForm.BitBtn1Click(Sender: TObject);
begin
 Close;
end;

procedure TARM_AboutForm.FormCreate(Sender: TObject);
begin
 Memo1.Lines.Clear;
 Memo1.Lines.Add('');
 Memo1.Lines.Add('���������');
 Memo1.Lines.Add('"��� ������� ������"');
 Memo1.Lines.Add('������ ' + ARM_Version);
 Memo1.Lines.Add('');
 Memo1.Lines.Add('���������� ������� ������');
 Memo1.Lines.Add('+375 44 533-56-26');
 Memo1.Lines.Add('d_soroka@tut.by');
 Memo1.Lines.Add('');
 Memo1.Lines.Add('2008-2020(�)');
end;

end.
